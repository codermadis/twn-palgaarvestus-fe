var App = angular.module("App", ['ngRoute', 'ngAnimate', 'ngSanitize', 'ui.bootstrap', 'ng-breadcrumbs', 'angularModalService', 'googlechart', 'ngProgress', 'ngCookies', 'mdo-angular-cryptography', 'ngFileUpload', 'ui.tinymce']);

var settings = {
	template: "/dist/templates.html",
	prefix: 'https://palgaarvestus-be-2-madispold.c9users.io/api/v1/',
	resolveAll: {
		authenticate: function($settings) {
			$settings.getLogin()
		}
	},
	resolveValidLocation: {
		validLocation: function($validLocation) {
			$validLocation.get()
		}
	},
	resolveOrderedUsers: {
		orderedList: function(orderedUsers) {
			orderedUsers.get()
		}
	}
};
App.factory('orderedUsers', function($http, $q, $location, $rootScope, $filter, $routeParams) {
    var content = false;
    
	return {
		get: function(){
			var deferred = $q.defer();
			var redirect;
			
			if( !content ){
				
				var promise = $http({
					method: "GET",
					url: settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users'
				});

    			promise.then(function(response){
    				
    				var outSourcerRole = "OUTSOURCER";
    				
    				$rootScope.workerOrderedIds = [];
    				$rootScope.outSourcerOrderedIds = [];
					var orderedUsers = $filter('orderBy')(response.data, 'first_name');
					var workerOrderedUsers = $filter('filter')(orderedUsers, '!' + outSourcerRole, 'role');
					var outSourcerOrderedUsers = $filter('filter')(orderedUsers, outSourcerRole, 'role');
					
					for(var item in workerOrderedUsers) {
					    $rootScope.workerOrderedIds.push(workerOrderedUsers[item].id);
					};
					var currentIdPosition = $rootScope.workerOrderedIds.indexOf(parseInt($routeParams.id));
					var nextIdPosition = currentIdPosition+1;
					var previousIdPosition = currentIdPosition-1;
					$rootScope.workerNextId = $rootScope.workerOrderedIds[nextIdPosition];
					$rootScope.workerPrevId = $rootScope.workerOrderedIds[previousIdPosition];
					
					for(var elem in outSourcerOrderedUsers) {
					    $rootScope.outSourcerOrderedIds.push(outSourcerOrderedUsers[elem].id);
					};
					var curPos = $rootScope.outSourcerOrderedIds.indexOf(parseInt($routeParams.id));
					var nextPos = curPos+1;
					var prevPos = curPos-1;
					$rootScope.outSourcerNextId = $rootScope.outSourcerOrderedIds[nextPos];
					$rootScope.outSourcerPrevId = $rootScope.outSourcerOrderedIds[prevPos];
    			}, function(){
		
    			});
    			
    			return deferred.promise;
			}
			
		},
	};
});
App.factory('$settings', function($http, $q, $location, $rootScope) {
   var content = true; //set false
    
	return {
		getLogin: function() {
			if(!sessionStorage.loggedIn || !sessionStorage.loggedIn) {
				$location.path('/login');
			}
		},
		get: function(param){
			var deferred = $q.defer();
			var redirect;
			
			if( !content ){
				
				var promise = $http({
					method: "GET",
					url: settings.prefix+"settings"
				});

    			promise.then(function(response){
    				
					if( response.data.user ){
    			    	
    			    	content = response.data;
    			    	$rootScope.settings = content;
    			    	
						deferred.resolve(response);
					}else{
						$location.path("/login");
					}
					
    			}, function(){
		
    			});
    			
    			return deferred.promise;
			}
			
		},
		
		delete: function(){
			content = false;
			$rootScope.settings = false;
		}
	};
});
App.factory('$templateCache', function($cacheFactory, $http, $injector) {
  var cache = $cacheFactory('templates');
  var allTplPromise;

  return {
    get: function(url) {
      var fromCache = cache.get(url);

      // already have required template in the cache
      if (fromCache) {
        return fromCache;
      }

      // first template request ever - get the all tpl file
      if (!allTplPromise) {
        allTplPromise = $http.get(settings.template).then(function(response) {
          // compile the response, which will put stuff into the cache
          $injector.get('$compile')(response.data);
          return response;
        });
      }
      
      // return the all-tpl promise to all template requests
      return allTplPromise.then(function(response) {
        return {
          status: response.status,
          data: cache.get(url),
          headers: response.headers
        };
      });
    },

    put: function(key, value) {
      cache.put(key, value);
    }
  };
});
App.factory('$validLocation', function($http, $q, $location, $rootScope) {
   var content = false; //set false
    
	return {
		get: function(param){
			var deferred = $q.defer();
			var redirect;
			
			if( !content ){
				
				var promise = $http({
					method: "GET",
					url: settings.prefix+"ip_check"
				});
    			promise.then(function(response){
    			    $rootScope.locationValiditySet = true;
					$rootScope.locationAllowed = true;
    			}, function(){
    			    $rootScope.locationValiditySet = true;
		            $rootScope.locationAllowed = false;
    			})
    			return deferred.promise;
			}
			
		},
	};
});
App.directive("toalertMessage", ['$timeout', function($timeout) {
    return {
        restrict: "E",
        replace: true,
        scope: {
            'toalert': '='
        },
        template: 
            '<div class="row fade-element-in">' +
                '<div class="col-12">' +
                    '<div class="alert alert-success" role="alert"> {{ toalert }} </div>' +
                '</div>' +
            '</div>',
        link: function (scope, elem, attrs) {
            scope.$watch('toalert', function (newValue, oldValue, scope) {
                $timeout(function () {
                    scope.$parent.messages.alertMessage = false;
                    scope.$parent.messages.generalAlertMessage = false;
                    scope.$parent.updatedWorkdays = false;
                }, 5000);       
            });
        }
    };
}]);
App.directive('convertToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(val) {
        return val != null ? parseInt(val, 10) : null;
      });
      ngModel.$formatters.push(function(val) {
        return val != null ? '' + val : null;
      });
    }
  };
});
App.directive("dynamicCompile", function($templateRequest, $compile, $interpolate){
    return{
        link: function(scope, element){
            //$templateRequest("blank-email-html.html").then(function(html){
                var template = '<div>{{user.name}}</div>'
                var linkFn = $compile(template);
                var content = linkFn(scope);
                var testInterpolate = $interpolate(template)(scope);
                var outerContent = linkFn(scope)[0].outerHTML
                scope.$parent.compiledHtml = content;
                console.log(scope.$parent.compiledHtml);
                console.log(testInterpolate);
                console.log(outerContent);
            //});
        },
        // link: function(scope, element){
        //   $templateRequest("blank-email-html.html").then(function(html){
        //       var template = angular.element(html);
        //       scope.$parent.compiledHtml = template);
        //       $compile(template)(scope);
        //   });
        // }
    };
});
App.directive("errorMessage", ['$timeout', function($timeout){
    return {
        restrict: "E",
        replace: true,
        scope: {
            'error': '=',
            'fadeout': '='
        },
        template: 
            '<div class="row animate-if-error">' +
                '<div class="col-12">' +
                    '<div class="alert alert-danger" role="alert"> {{ error }} </div>' +
                '</div>' +
            '</div>',
        link: function (scope, elem, attrs) {
            if(scope.fadeout) {
                scope.$watch('error', function (newValue, oldValue, scope) {
                    $timeout(function () {
                        scope.$parent.messages.errorMessage = false;
                    }, 5000);       
                });
            };
        }
    };
}]);

App.filter('naturalSort',function(){
    function naturalSort (a, b) {
       var re = /(^-?[0-9]+(\.?[0-9]*)[df]?e?[0-9]?$|^0x[0-9a-f]+$|[0-9]+)/gi,
            sre = /(^[ ]*|[ ]*$)/g,
            dre = /(^([\w ]+,?[\w ]+)?[\w ]+,?[\w ]+\d+:\d+(:\d+)?[\w ]?|^\d{1,4}[\/\-]\d{1,4}[\/\-]\d{1,4}|^\w+, \w+ \d+, \d{4})/,
            hre = /^0x[0-9a-f]+$/i,
            ore = /^0/,
            i = function(s) { return naturalSort.insensitive && (''+s).toLowerCase() || ''+s },
            // convert all to strings strip whitespace
            x = i(a).replace(sre, '') || '',
            y = i(b).replace(sre, '') || '',
            // chunk/tokenize
            xN = x.replace(re, '\0$1\0').replace(/\0$/,'').replace(/^\0/,'').split('\0'),
            yN = y.replace(re, '\0$1\0').replace(/\0$/,'').replace(/^\0/,'').split('\0'),
            // numeric, hex or date detection
            xD = parseInt(x.match(hre)) || (xN.length != 1 && x.match(dre) && Date.parse(x)),
            yD = parseInt(y.match(hre)) || xD && y.match(dre) && Date.parse(y) || null,
            oFxNcL, oFyNcL;
        // first try and sort Hex codes or Dates
        if (yD)
            if ( xD < yD ) return -1;
        else if ( xD > yD ) return 1;
        // natural sorting through split numeric strings and default strings
        for(var cLoc=0, numS=Math.max(xN.length, yN.length); cLoc < numS; cLoc++) {
            // find floats not starting with '0', string or 0 if not defined (Clint Priest)
            oFxNcL = !(xN[cLoc] || '').match(ore) && parseFloat(xN[cLoc]) || xN[cLoc] || 0;
            oFyNcL = !(yN[cLoc] || '').match(ore) && parseFloat(yN[cLoc]) || yN[cLoc] || 0;
            // handle numeric vs string comparison - number < string - (Kyle Adams)
            if (isNaN(oFxNcL) !== isNaN(oFyNcL)) { return (isNaN(oFxNcL)) ? 1 : -1; }
            // rely on string comparison if different types - i.e. '02' < 2 != '02' < '2'
            else if (typeof oFxNcL !== typeof oFyNcL) {
                oFxNcL += '';
                oFyNcL += '';
            }
            if (oFxNcL < oFyNcL) return -1;
            if (oFxNcL > oFyNcL) return 1;
        }
        return 0;
    }
    return function(arrInput) {
        var arr = arrInput.sort(function(a, b) {
            return naturalSort(a.name,b.name);
        });
        return arr;
    }
});
App.directive('showCompiledTemplate',['$compile','$timeout',
    function($compile , $timeout) {
        return {
            restrict: 'E',
            template: '<div class="compiled-template" ng-show="false"></div>',
            scope: {
              data: '=',
              template: '=',
            },
            link: function(scope,elem,attrs) {
            	var updateOutput = function(tpl) {
                    var compiled = $compile(tpl)(scope);
                    $timeout(function(){
                        var theHtml = compiled[0].outerHTML;
                        scope.$parent.$parent.compiledHtml = theHtml;
                    })
                };
                scope.$watch("template",function(tpl){
            		updateOutput(tpl);
                });
                scope.$watch("data",function(){
                    updateOutput(scope.template);
                },true);
            }
        };
    }
]);
App.directive("smallAlertMessage", ['$timeout', function($timeout) {
    return {
        restrict: "E",
        replace: true,
        scope: {
            'workdays': '=',
            'project': '=',
            'myMessage': '='
        },
        template: 
            '<span class="animate-if small-alert float-right">' +
                '<i class="fa fa-check item-update"></i>' +
            '</span>',
        link: function (scope, elem, attrs) {
            if(scope.workdays) {
                scope.$watch('workdays', function (newValue, oldValue, scope) {
                    $timeout(function () {
                        scope.$parent.messages.workdaysAlert = false;
                    }, 1000);       
                });
            } else if(scope.project) {
                scope.$watch('project', function (newValue, oldValue, scope) {
                    $timeout(function () {
                        scope.$parent.messages.updatedProjectId = false;
                    }, 10000);       
                });
            } else {
                scope.$watch('myMessage', function (newValue, oldValue, scope) {
                    $timeout(function () {
                        scope.$parent.messages.myMessage = false;
                    }, 1500);       
                });
            }
        }
    };
}]);
App.directive('smartFloat', function() {
    var FLOAT_REGEXP = /^\-?\d+((\.|\,)\d+)?$/;
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {
            ctrl.$parsers.unshift(function(viewValue) {
                // if (FLOAT_REGEXP.test(viewValue)) {
                    // ctrl.$setValidity('float', true);
                    if(typeof viewValue === "number")
                    return viewValue;
                    else
                    return parseFloat(viewValue.replace(',', '.'));
                // } 
            });
        }
    };
});
App.directive('validNumber', function () {
    return {
        require: '?ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            element.on('keydown', function (event) {
                var key = event.which || event.keyCode;
                
                // MINUS key == 109
                
                if (!event.shiftKey && !event.altKey && !event.ctrlKey &&
                    // numbers   
                    key >= 48 && key <= 57 ||
                    // Numeric keypad
                    key >= 96 && key <= 105 ||
                    // comma, period on keypad
                    key == 190 || key == 188 || key == 110 ||
                    // Backspace and Tab and Enter
                    key == 8 || key == 9 || key == 13 ||
                    // Home and End
                    key == 35 || key == 36 ||
                    // left and right arrows
                    key == 37 || key == 39 ||
                    // Del and Ins
                    key == 46 || key == 45) return true;
                        return false;
            });
        }
    }
});
App.directive('validTime', function () {
    return {
        require: '?ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            element.on('keydown', function (event) {
                var key = event.which || event.keyCode;
                
                // MINUS key == 109
                
                if (!event.shiftKey && !event.altKey && !event.ctrlKey &&
                    // numbers   
                    key >= 48 && key <= 57 ||
                    // Numeric keypad
                    key >= 96 && key <= 105 ||
                    // colon
                    key == 186 ||
                    // Backspace and Tab and Enter
                    key == 8 || key == 9 || key == 13 ||
                    // Home and End
                    key == 35 || key == 36 ||
                    // left and right arrows
                    key == 37 || key == 39 ||
                    // Del and Ins
                    key == 46 || key == 45) return true;
                        return false;
            });
        }
    }
});
App.config(function ($routeProvider, $locationProvider, $httpProvider){
	
	if(location.hostname.indexOf("palgaarvestus.twn.ee") != -1 ) {
      settings.url = "https://palgaarvestus.twn.ee";
		settings.prefix = "https://palgaarvestus.twn.ee/api/v1/";
	};
	
	$locationProvider.html5Mode({
  		enabled: true,
  		requireBase: false
	});
	$routeProvider.otherwise('/');
   $httpProvider.defaults.withCredentials = true;
	
   $httpProvider.interceptors.push(function($q, $location, $rootScope, $interval) {
      return {
         'responseError': function(response) {
            if(response.status === 404) {
               window.scrollTo(0, 0);	
               return $q.reject(response);
            } else if(response.status === 500) {
               window.scrollTo(0, 0);	
               return $q.reject(response);
            } else if(response.status === 412) {
               window.scrollTo(0, 0);	
               return $q.reject(response);
            } else if(response.status === 401) {
               $location.path('/login');
               return $q.reject(response);
            } else if(response.status === 403) {
               $location.path('/login');
               $rootScope.locationAllowed = false;
               return $q.reject(response);
            } else if(response.status === 503) {
               $rootScope.logOut();
               $location.path('/login');
               return $q.reject(response);
            } else if(response.status === 304) {
               return $q.defer(response);
            } 
         }
      };       
   });
   
});
App.config(['$cryptoProvider', function($cryptoProvider){
    $cryptoProvider.setCryptographyKey('myKey');
}]);
App.controller('mainCtrl', function ($scope, $http, $rootScope, $sanitize, $route, $routeParams, $location, breadcrumbs, $filter, $anchorScroll) {   
	
    $scope.breadcrumbs = breadcrumbs;
    $rootScope.changedProjects = [];
	
	$rootScope.hideNavbar = false;
	$rootScope.$on("$routeChangeSuccess", function (event, currentRoute, previousRoute) {

		window.scrollTo(0, 0);		
   		$rootScope.activeRole = sessionStorage.getItem('role');
   		$scope.loginPage = false;
		$rootScope.hideNavbar = false;
		
		$rootScope.currentLocation = $location.path();
		
		if($rootScope.currentLocation == '/login' || $rootScope.currentLocation.indexOf('password') != -1) { $scope.loginPage = true; }
	});
	
	$rootScope.logOut = function () {
		$http({
			method: "PUT",
			url: settings.prefix+"logout",
		}).then(function( response ){
			$rootScope.login = false; 
			sessionStorage.clear();
			$location.path('/login');
		}, function(response){
			console.log(response);
		});
	}
	
	$rootScope.monthLabels = ['Jaanuar', 'Veebruar','Märts', 'Aprill', 'Mai', 'Juuni', 'Juuli', 'August', 'September', 'Oktoober', 'November', 'Detsember']
    //month setter
    $rootScope.monthSetter = function () {
	    var currentDate = new Date();
	    $rootScope.currentMonth = $filter('date')(currentDate,'yyyy-MM');
	    
	    var previousMonth = currentDate.getMonth();
	    
	    var previousBeforeMonth = previousMonth-1;
	    $rootScope.currentMonthLabel = $rootScope.monthLabels[previousMonth];
	    $rootScope.previousMonthLabel = $rootScope.monthLabels[previousMonth-1] || $rootScope.monthLabels[$rootScope.monthLabels.length-1]; // start from the end again
	    $rootScope.previousBeforeMonthLabel = $rootScope.monthLabels[previousBeforeMonth-1] || $rootScope.monthLabels[$rootScope.monthLabels.length-1];; // start from the end again
	    
	    var thisYear = currentDate.getFullYear();
	    var previousBeforeYear = thisYear;
	    
	    if(previousMonth == 0) { previousMonth = 12, thisYear = thisYear - 1 }
	    if(previousBeforeMonth == 0) { previousBeforeMonth = 12; var previousBeforeYear = thisYear - 1}
	    if(previousBeforeMonth == -1) { previousBeforeMonth = 11; var previousBeforeYear = thisYear }
	    
	    $rootScope.prevMonthNum = previousMonth;
	    $rootScope.prevBeforeMonthNum = previousBeforeMonth;
	    $rootScope.thisYearNum = thisYear;
	    
	    thisYear = thisYear.toString(); previousMonth = previousMonth.toString();
	    $rootScope.thisYear = thisYear.toString();
	    if(previousMonth > 9) {
	        $rootScope.finalPreviousMonth = (thisYear + '-' + previousMonth);
	    } else {
	        $rootScope.finalPreviousMonth = (thisYear + '-0' + previousMonth);
	    }
	    if(previousBeforeMonth > 9) {
	        $rootScope.finalPreviousBeforeMonth = (previousBeforeYear + '-' + previousBeforeMonth);
	    } else {
	        $rootScope.finalPreviousBeforeMonth = (previousBeforeYear + '-0' + previousBeforeMonth);
	    }
    };
    $rootScope.monthSetter();
	
});
App.controller('alert', function($scope, $http, $rootScope, $routeParams, $location){
	
})

App.controller('archive', function($scope, $http, $rootScope, $routeParams, $location, ModalService){
    $scope.billableVal = [ "Unbillable", "Billable" ];
    $scope.limit = 20;
    $scope.emailLimit = 20;
    $scope.monthValues = [];
    $scope.yearValues = [];
    $scope.outSourcer = "OUTSOURCER";
    $scope.selectedYear = $rootScope.thisYearNum;
    $scope.api_host = settings.prefix;
    
    var startYear = 2017;
    var startMonth = 1;
    for(var i = startYear; i <= $rootScope.thisYearNum; i++) {
        if(i == $rootScope.thisYearNum) { var endMonth = $rootScope.prevMonthNum; startMonth = 1 } else { endMonth = 12 }
        for(var j = startMonth; j <= endMonth; j++) {
            if(j < 10) { var monthVal = j.toString(); monthVal = '0' + monthVal } else { monthVal = j };
            var monthHash = {};
            monthHash['label'] = $rootScope.monthLabels[j-1] + ' ' + i;
            monthHash['name'] = i + '-' + monthVal;    
            $scope.monthValues.push(monthHash);
        }
    }
    $scope.monthValues = $scope.monthValues.reverse();
	$scope.selectedMonth = $rootScope.finalPreviousMonth;
	
	for (var year = startYear; year <= $rootScope.thisYearNum; year++) {
	    $scope.yearValues.push(year)
	}
	
	$scope.messages = { alertMessage: false, errorMessage: false };
	
	$scope.$watch('tabs._active', function(newValue, oldValue){
	    if(newValue != oldValue) {
            $scope.getData(newValue); 
	    };
    });
	
	$scope.tabs = {
	    _active: 'PROJECTS',
	    setActive: function(tab) {
	        this._active = tab;
	    },
	    isActive: function(tab) {
	        return this._active === tab;
	    },
	    getActive: function() {
	        return this._active;
	    }
	}
	
	$scope.upTheLimit = function() {
	    $scope.limit += 20;
	}
	$scope.upTheEmailLimit = function() {
	    $scope.emailLimit += 20;
	}
	
	$scope.onChangeData = function(selected) {
	    var activeTab = $scope.tabs.getActive()
	    switch (activeTab) {  
            case 'PROJECTS':
            case 'EMPLOYEES':
                $scope.getData(activeTab, {
                    month: selected
                })
                break;
            case 'EMAILS':
                $scope.getData(activeTab, {
                    year: selected
                })
                break;
        }
	}
	
	$scope.getData = function(tab, options) {
	    options = options || {}
	    var selectedMonth = options.month || $scope.selectedMonth
	    var selectedYear = options.year || $scope.selectedYear
        switch (tab) {  
            case 'PROJECTS':
                $scope.projectData = false;
                if(!selectedMonth) {return;}
                var url = settings.prefix+ 'archive/months/' + selectedMonth + '/projects'
                break;
            case 'EMPLOYEES':
                $scope.userData = false;
                if(!selectedMonth) {return;}
                var url = settings.prefix+ 'archive/months/' + selectedMonth + '/users'
                break;
            case 'EMAILS':
                $scope.emailData = false;
                if(!selectedYear) {return;}
                var url = settings.prefix+ 'archive/emails/' + selectedYear
                break;
        }
        $http.get(url).then(function(response) {
            switch (tab) {
                case 'PROJECTS':
                    $scope.projectData = response.data;
                    var projects = response.data;
                    for(var item in projects) {
                        projects[item].minute_remainder = projects[item].minutes%60;
                        projects[item].hour_floored = Math.floor(projects[item].minutes/60);
                        if($rootScope.changedProjects.includes(projects[item].id)) { projects[item].changed = true }
                    }
                    break;
                case 'EMPLOYEES':
                    $scope.userData = response.data;
                    break;
                case 'EMAILS':
                    $scope.emailData = response.data;
                    break;
            }
        }, function(response) {
            if (options.hideErrors) return;
            var message = response.data.message;
		    if(message.length > 0) { 
		        $scope.messages.errorMessage = message; 
		    } else if (message.constructor === Object) { 
		        $scope.messages.errorMessage = response.statusText 
		    }       
        });
    };
    
    $scope.getData('PROJECTS');
    $scope.getData('EMPLOYEES', {hideErrors: true});
    $scope.getData('EMAILS', {hideErrors: true});
    
    $scope.openEmailView = function(id, first, last){
        ModalService.showModal({
            templateUrl: 'wageEmail.html',
            controller: "wageEmail"
        }).then(function(modal) {
            
            $http.get(settings.prefix+ 'months/' + $scope.selectedMonth + '/users/' + id + '/email').then(function(response) {
                modal.scope.hideActions = true;
                modal.scope.user = response.data;
                modal.scope.user.name = first + ' ' + last;
                modal.scope.userId = id;
                
                var monthNumber = parseInt($scope.selectedMonth.slice(-1));
                var monthLabel = $rootScope.monthLabels[monthNumber-1];
                var yearLabel = $rootScope.thisYear;
                modal.scope.wageEmailTitle = monthLabel + ' ' + yearLabel + ' - ' + modal.scope.user.name;
            }, function(response) {
                console.log(response);
            }).then(function(){
                modal.element.show();  
            });
            modal.close.then(function(result) {
                
            });
        });
    };
    
    $scope.openEmail = function(email){
        ModalService.showModal({
            templateUrl: 'archiveEmail.html',
            controller: "archiveEmail"
        }).then(function(modal) {
            modal.element.show();  
            modal.scope.email = email;
            modal.scope.apiUrl = settings.prefix;
            modal.close.then(function(result) {
                
            });
        });
    };
})
App.controller("frontpage", function($scope, $rootScope){
   $scope.text = "Pealeht";
});
App.controller('holidays', function($scope, $http, $route, $rootScope, $location, ModalService, $filter){
    
    var VACATION_TYPES = {
        'vacation': 'Puhkus',
        'unpaid': 'Palgata puhkus',
        'study': 'Õppepuhkus',
        'disability': 'Töövõimetus',
        'child': 'Lapsepuhkus',
        'other': 'Muu'
    };
    
    $scope.messages = { alertMessage: false, myMessage: false };
    $scope.firstActive = true;
	$scope.secondActive = false;
	$scope.regularLimit = 20;
	$scope.activeLimit = 20;
    
    $scope.upTheRegularLimit = function() {
	    $scope.regularLimit += 20;
	}
	$scope.upTheActiveLimit = function() {
	    $scope.activeLimit += 20;
	}
	
	$scope.getVacations = function() {
	    $scope.vacations = false
	    $scope.active_vacations = false
        $http.get(settings.prefix + 'vacations').then(function(response) {
            $scope.vacations = response.data.map(function(elem) {
                elem.type = VACATION_TYPES[elem.type];
                return elem;
            });
            $scope.active_vacations = $scope.vacations.filter(function(elem){return elem.active});
            $scope.previousMonthBeginning = $rootScope.finalPreviousMonth+ '-01';
            $scope.now = $filter('date')(new Date($scope.previousMonthBeginning), 'yyyy-MM-dd');
        }, function(response) {
            $scope.messages.errorMessage = response.data.message;
        });
    };
    $scope.getVacations();
    
    $scope.reloadThis = function() {
        $route.reload()
    }
    
    $scope.openHolidayAdd = function(){
        ModalService.showModal({
            templateUrl: 'addHoliday.html',
            controller: "addHoliday"
        }).then(function(modal) {
            modal.scope.modalHeader = "Eemalviibimise lisamine";
            modal.element.show();  
            modal.close.then(function(result) {
                if(result) { 
                    $scope.firstActive = false;
	                $scope.secondActive = true;
	                $scope.getVacations(); 
                };
            });
        });
    };
    $scope.openHolidayView = function(id, first_name, last_name){
        ModalService.showModal({
            templateUrl: 'viewHoliday.html',
            controller: "addHoliday"
        }).then(function(modal) {
            modal.scope.modalHeader = "Eemalviibimine";
            modal.scope.userName = first_name + ' ' + last_name;
            $http.get(settings.prefix+ 'vacations/' + id).then(function(response){
                modal.scope.vacationData = response.data;
                modal.scope.vacationData.start_date = new Date(modal.scope.vacationData.start_date);
                modal.scope.vacationData.end_date = new Date(modal.scope.vacationData.end_date);
                modal.scope.type = VACATION_TYPES[modal.scope.vacationData.type];
                modal.scope.editView = true;
                modal.scope.editVacationId = id;
                modal.element.show();  
            }, function(response) {
                console.log(response);
            });
            modal.close.then(function(result) {
                if(result) {
                    $scope.firstActive = false;
	                $scope.secondActive = true;
	                $scope.getVacations(); 
                };
            });
        });
    };
})
App.controller('import', function($scope, $route, $http, $rootScope, $routeParams, $location, $filter, ModalService, $timeout, $interval, ngProgressFactory){
    
    $scope.messages = { alertMessage: false, errorMessage: false, workdaysAlert: false, updatedProjectId: false, monthDaysInvalid: false };
    
    $scope.$watch('selectedMonth', function (newValue, oldValue, scope) {
        $scope.importedProjects = false;
        $scope.importValues($scope.selectedMonth, '/projects');
    });
    // $scope.$watch('messages.monthDaysInvalid', function(){
    //     if($scope.messages.monthDaysInvalid) {
    //         $timeout(function () {
    //             $scope.messages.monthDaysInvalid = false;
    //         }, 2500);  
    //     }
    // });
    $scope.billableVal = [ "Unbillable", "Billable" ]
    $scope.selectedMonth = $rootScope.finalPreviousMonth;
    
    var prog;
    $scope.getProgress = function(selection, urlEnding){ 
        prog = $interval(function() {
            if(urlEnding.indexOf('import') != -1 && !$scope.importedProjects) {
                $http.get(settings.prefix+ 'months/' + selection + urlEnding + '/progress').then(function(response) { 
                    if(response.data.progress || response.data.progress == null) {
                        if(response.data.progress == 100) {
                            $scope.stopRequests();
                            $scope.progressbar.complete();
                        }
                        if(!$scope.progressbar) {
                            $scope.progressbar = ngProgressFactory.createInstance();
                            $scope.progressbar.setHeight('4px');
                            $scope.progressbar.setParent(document.getElementById('progressBar'));
                        }
                        if(response.data.progress != null) {
                            $scope.progressbar.set(response.data.progress);  
                        }
                    } else {
                        $scope.stopRequests();
                        $scope.progressbar.complete();
                    }
                });
            } else {
                $scope.stopRequests();
                $scope.progressbar.complete();
            }
        }, 1250);
    };
    $scope.stopRequests = function() {
        $interval.cancel(prog);
    };
    
    $scope.importValues = function(selection, urlEnding) {
        if(selection) {
            $scope.messages.workdaysAlert = false;
            $scope.disableLoader = false;
            $scope.importedProjects = false;
            if(urlEnding.indexOf('import') != -1) { 
                $scope.getProgress(selection, urlEnding);
            };
            
            $http.get(settings.prefix+ 'months/' + selection + urlEnding).then(function(response) {
                if(urlEnding.indexOf('import') != -1) { 
                    $scope.importWorkdays(selection);
                }
                $scope.messages.workdaysAlert = false;
                $scope.importedProjects = response.data;
                var projects = response.data;
                for(var item in projects) {
                    projects[item].minute_remainder = projects[item].minutes%60;
                    projects[item].hour_floored = Math.floor(projects[item].minutes/60);
                    if($rootScope.changedProjects) {
                        if($rootScope.changedProjects.includes(projects[item].id)) { projects[item].changed = true }
                    };
                }
                $rootScope.changedProjects = [];
            }, function(response) {
                $scope.disableLoader = true;
                var message = response.data.message;
		        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
            });
        };
    }
    
    $scope.updateProject = function(selection, project, type) {
        angular.forEach(project, function(value, key){
            if(!value && value != 0) { 
                project[key] = null;
            };
            if(value == "" && key == "comment") { 
                project[key] = null;
            };
        });
        if((type == 1 && (project.rate_salary < 0 || project.rate_salary === null)) || (type == 2 && (project.rate_statistic < 0 || project.rate_statistic === null))) { } else {
            $http({
    			method: "PUT",
    			url: settings.prefix+ 'months/' + selection + '/projects/' + project.id,
    			data: project,
    		}).then(function( response ){
    		    $scope.messages.updatedProjectId = project.id;
    		}, function(response){
    		    var message = response.data.message;
    	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
    		});
        };
    };
    
    $scope.setRateStatistic = function(project) {
        if(project.rate_statistic == null) { project.rate_statistic = project.rate_salary }
    };
    
    $scope.exportValues = function(selection) {
        $http({
			method: "PUT",
			url: settings.prefix+ 'months/' + selection + '/projects',
			data: $scope.importedProjects,
		}).then(function( response ){
			$location.path('/wages');
		}, function(response){
		    var message = response.data.message;
	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
		});
    };
    
    
    $scope.saveWorkdays = function(selection, monthDays, form) {
        if(form.monthDays.$valid) {
            $http({
    			method: "PUT",
    			url: settings.prefix+ 'months/' + selection + '/workdays',
    			data: {
    			    'workdays': monthDays,
    			}
    		}).then(function( response ){
    		  //  $scope.messages.alertMessage = response.data.message;
    		    $scope.messages.workdaysAlert = true;
    		}, function(response){
    		    var message = response.data.message;
    	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
    		});   
        } else {
            $scope.messages.monthDaysInvalid = true;
        }
    }
    
    $scope.importWorkdays = function(selection) {
        // $http.get(settings.prefix+ 'months/' + selection + '/workdays').then(function(response) {
        //     $scope.monthDays = response.data.workdays;
        // }, function(response) {
        //     var message = response.data.message;
	       // if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
        // });
    }
    
    $scope.promptImport = function(){
        ModalService.showModal({
            templateUrl: 'importPrompt.html',
            controller: "importPrompt"
        }).then(function(modal) {
            modal.scope.modalHeader = "Oled sa kindel?"
            modal.scope.selectedMonth = $scope.selectedMonth;
            modal.element.show();  
            modal.close.then(function(result) {
                if(result == 1) { $location.path('/wages'); };
                if(result == 0) { $scope.importValues($scope.selectedMonth, '/projects') };
                if(!result) {};
            });
        });
    };
});

App.controller('login', function($scope, $http, $rootScope, $routeParams, $location, $timeout, $cookies, $crypto) {
	
	var expireDate = new Date();
	expireDate.setDate(expireDate.getDate() + 180);
	var cookieOptions = {
		expires: expireDate
	}
	
	if($cookies.get('rememberMe')) {
		$scope.remembered = true;
		$scope.username = JSON.parse($cookies.get('username'));
		$scope.password = $crypto.decrypt(JSON.parse($cookies.get('password')));
	} else {
		$scope.remembered = false;
	}
	
	$rootScope.hideNavbar = true;
	$scope.newPassword = false;
	$scope.messages = {
		errorMessage: false,
		alertMessage: false
	};
	$scope.$watch('messages.errorMessage', function(newValue, oldValue) {
	    if(newValue != oldValue) {
	        $timeout(function () {
                $scope.messages.errorMessage = false;
            }, 2500);      
	    }
	});
	$scope.$watch('messages.alertMessage', function(newValue, oldValue) {
	    if(newValue != oldValue) {
	        $timeout(function () {
                $scope.messages.alertMessage = false;
            }, 2500);      
	    }
	});
	$scope.forgotPassword = false;
	$scope.openEmailForm = function() {
		$scope.forgotPassword = !$scope.forgotPassword;
	};
	$scope.loginSubmit = function(username, password, remembered){
		$http({
			method: "POST",
			url: settings.prefix+"login",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: {
				email: username,
				password: password
			},
    		transformRequest: function(obj) {
	            var str = [];
	            for(var p in obj)
	               str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
	            return str.join("&");
    		}
		}).then(function( response ){
			if(response.data.success) { 
				if(remembered) {
					$cookies.putObject('rememberMe', true, cookieOptions);
					$cookies.putObject('username', username, cookieOptions);
					$cookies.putObject('password', $crypto.encrypt(password), cookieOptions);
				} else {
					$cookies.remove('rememberMe');
					$cookies.remove('username');
					$cookies.remove('password');
				}
		
				$rootScope.login = true; 
				sessionStorage.setItem('loggedIn', true);
				sessionStorage.setItem('user', username);
				sessionStorage.setItem('role', response.data.role );
				$location.path('/');
			} else {
				$scope.messages.errorMessage = response.data.message;
			}
			
		}, function(response){
			if(response.data.message) {
				$scope.messages.errorMessage = response.data.message;
			} else { $scope.messages.errorMessage = "Logimine ebaõnnestus" }
		});
	}
	$scope.sendRecoveryEmail = function(input){
		$http({
			method: "POST",
			url: settings.prefix+"recovery",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: {
				email: input,
			},
    		transformRequest: function(obj) {
	            var str = [];
	            for(var p in obj)
	               str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
	            return str.join("&");
    		}
		}).then(function( response ){
			if(response.data.success) { 
				$scope.messages.alertMessage = response.data.message;
			} else {
				$scope.messages.errorMessage = response.data.message;
			}
		}, function(response){
			if(response.data.message) {
				$scope.messages.errorMessage = response.data.message;
			} else { $scope.messages.errorMessage = "Logimine ebaõnnestus" }
		});
	};
})
App.controller('wageEmail', function($scope, close, $http, $rootScope, $location){
    $scope.user = {};
    $scope.disableSend = false;
    $scope.userId = false;
    $scope.messages = { errorMessage: false, alertMessage: false };
    $scope.close = function(result) {
        close(result, 1000);
    };
    $scope.goToUser = function() {
        $location.path('wages/' + $scope.userId);
    };
    $scope.sendEmail = function() {
        $scope.disableSend = true;
        $http({
			method: "POST",
			url: settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users/' + $scope.userId + '/email',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).then(function( response ){
		    $scope.messages.alertMessage = response.data.message;
		    $scope.disableSend = false;
		    $scope.close(true);
		}, function(response){
		    $scope.disableSend = false;
		    if(response.data.message) {
		        $scope.messages.errorMessage = response.data.message;    
		    } else {
		        $scope.messages.errorMessage = "Esines viga"
		    }
		});
    };
});
App.controller('archiveEmail', function($scope, close, $http, $rootScope, $location){
    $scope.close = function(result) {
        close(result, 500);
    };
    $scope.downloadDocument = function(id) {
        console.log(id)
        window.open(settings.prefix + 'archive/files/' + id, '_blank');
    }
    $scope.downloadVacationDocument = function(id) {
        console.log(id)
        window.open(settings.prefix + 'vacations/' + id + '/download' , '_blank');
    }
});
App.controller('accountantPrompt', function($scope, close, $http, $rootScope, $location){
    $scope.close = function(result) {
        close(result, 500);
    };
});
App.controller('accountantEmail', function($scope, close, $http, $rootScope, $location, Upload, $timeout, $filter){
    $scope.disableSend = false;
    $scope.messages = { errorMessage: false, alertMessage: false };
    $scope.model = {};
    $scope.tinymceOptions = {
        theme: "modern",
        plugins: [
            "advlist autolink lists charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code",
            "insertdatetime nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor"
        ],
        toolbar1: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | print preview media | forecolor backcolor emoticons",
        height: "300px",
    };	
    $scope.close = function(result) {
        close(result, 1000);
    };
    $scope.downloadDocument = function(id) {
        console.log(id)
        window.open(settings.prefix + 'archive/files/' + id, '_blank');
    }
    $scope.downloadVacationDocument = function(id) {
        console.log(id)
        window.open(settings.prefix + 'vacations/' + id + '/download' , '_blank');
    }
    $scope.deleteDocument = function(id) {
        $http.delete(settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/accounting/files/' + id).then(function(response){
            $scope.messages.alertMessage = response.data.message;
            for(var item in $scope.model.files) {
                if($scope.model.files[item].id == id) {
                    $scope.model.files.splice(item, 1);    
                }
            }
        })
    }
    $scope.uploadFiles = function(files, errFiles) {
        $scope.files = files;
        $scope.errFiles = errFiles;
        angular.forEach(files, function(file) {
            file.upload = Upload.upload({
                url: settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/accounting/files' ,
                data: {file: file}
            });

            file.upload.then(function (response) {
                $timeout(function () {
                    file.result = response.data;
                });
                
                var newfile = {};
                newfile['originalname'] = file.name;
                newfile['id'] = response.data.file_id;
                newfile['removable'] = true;
                $scope.model.files.push(newfile);
            
            }, function (response) {
                if(response.data.message) {
                    $scope.messages.errorMessage = response.data.message;    
                } else {
                    $scope.messages.errorMessage = "Esines viga"
                }
            });
        });
    }
    $scope.sendEmail = function() {
        $scope.disableSend = true;
        $http({
        	method: "POST",
        	url: settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/accounting/email',
        	headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        	data: $scope.model,
        }).then(function( response ){
            $scope.disableSend = false;
            $scope.messages.alertMessage = response.data.message;
            $scope.close(true);
        }, function(response){
            $scope.disableSend = false;
            if(response.data.message) {
                $scope.messages.errorMessage = response.data.message;    
            } else {
                $scope.messages.errorMessage = "Esines viga"
            }
        });
    };
});
App.controller('addHoliday', function($scope, close, $http, $rootScope, $location, $filter, $timeout, $routeParams){

    $scope.replaceFile = false;
    $scope.vacationData = {};
    $scope.userName = {};
    $scope.editView = false;
    $scope.model = {
        type: 'vacation' // form pre-filled
    };
    $scope.dateFields = [{}];
    $scope.autoModel = {};
    $scope.userId = false;
    $scope.messages = { errorMessage: false, alertMessage: false };
    $scope.popup_start = [false];
    $scope.popup_end = [false];
    $scope.openStart = function(index) {
        $scope.popup_start[index] = true;
    };
    $scope.openEnd = function(index) { 
        $scope.popup_end[index] = true;
    };
    
    $scope.addDateField = function() { 
        $scope.dateFields.push({});
    };
    
    $scope.close = function(result) {
        close(result, 1000);
    };
    $scope.setEndDate = function(index) {
        $scope.model.end = [];
        var endDate = new Date($scope.model.start[index].getTime() + 6 * 86400000 );
        $scope.model.end[index] = endDate;
        $scope.openEnd(index);
    }
    $scope.setViewEndDate = function(index) {
        $scope.vacationData.end_date = new Date( $scope.vacationData.start_date.getTime() + 6 * 86400000 );
        $scope.openEnd(index);
    }
    $scope.getUser = function(val) {
        return $http.get(settings.prefix+ 'users/autocomplete', {
            params: {
                search: val,
            }
        }).then(function(response){
            return response.data.map(function(item){
                return item;
            });
        });
    };
    
    $scope.deleteHoliday = function(id) {
        $http.delete(settings.prefix+ 'vacations/' + id).then(function(response){
            $scope.messages.alertMessage = "Puhkus kustutatud!";
            $scope.close(true); 
        }, function(response) {
            $scope.messages.errorMessage = response.data.message;
        });
    };
    
    $scope.saveHoliday = function(form, uploadedFile) {
        if(form.$valid) {
            var submitData = {};
            var startValues = [];
            var endValues = [];
            submitData = angular.copy($scope.model);
            submitData['user_id'] = $scope.autoModel.user.id;
            angular.forEach(submitData.start, function(value, key) {
                var val = $filter('date')(value, 'yyyy-MM-dd');
                startValues.push(val);
            });
            angular.forEach(submitData.end, function(value, key) {
                var val = $filter('date')(value, 'yyyy-MM-dd');
                endValues.push(val);
            });
            submitData['start_date'] = startValues.join(',');
            submitData['end_date'] = endValues.join(',');
            delete submitData['end'];
            delete submitData['start'];
            
            var fd = new FormData();
            fd.append('file', uploadedFile);
            var data ={};
            
            angular.forEach(submitData, function(value, key){
                if(submitData[key] && submitData[key] != null) {
                    data[key] = value;
                    fd.append(key, data[key]);  
                }
            });
            $http.post(settings.prefix+ 'vacations', fd, {
                headers : {
                    'Content-Type' : undefined
                },
                transformRequest : angular.identity
            }).success(function(response) {
                $scope.datepickerFirstError = false;
    		    $scope.datepickerSecondError = false;
    		    if(response.data && response.data.message) {
    		        $scope.messages.alertMessage = response.data.message;    
    		    } else { $scope.messages.alertMessage = response };
    			$scope.close(true); 
            }).error(function(response) {
                console.log(response)
                if(response && response.message) {
    		        $scope.messages.errorMessage = response.message;    
    		    } else { $scope.messages.errorMessage = response };
            });  
            
        } else {
            if(angular.element(".datepicker-element-1").hasClass('ng-invalid-required')) { $scope.datepickerFirstError = true; } else { $scope.datepickerFirstError = false; }
            if(angular.element(".datepicker-element-2").hasClass('ng-invalid-required')) { $scope.datepickerSecondError = true; } else { $scope.datepickerSecondError = false; }
        }
    }
    $scope.updateHoliday = function(form, uploadedFile) {
        if(form.$valid) {
            var fd = new FormData();
            if(uploadedFile) {
                fd.append('file', uploadedFile);
            };
            var data ={};
            
            angular.forEach($scope.vacationData, function(value, key){
                if($scope.vacationData[key] && $scope.vacationData[key] != null) {
                    if(value instanceof Date) {
                        value = $filter('date')(value, 'yyyy-MM-dd');
                    }
                    data[key] = value;
                    fd.append(key, data[key]);  
                }
            });
            $http.put(settings.prefix+ 'vacations/' + $scope.vacationData.id, fd, {
                headers : {
                    'Content-Type' : undefined
                },
                transformRequest : angular.identity
            }).success(function(response) {
                $scope.datepickerFirstError = false;
    		    $scope.datepickerSecondError = false;
    		    if(response.data && response.data.message) {
    		        $scope.messages.alertMessage = response.data.message;    
    		    } else { $scope.messages.alertMessage = response };
    			$scope.close(true); 
            }).error(function(response) {
                if(response.data && response.data.message) {
    		        $scope.messages.errorMessage = response.data.message;    
    		    } else { $scope.messages.errorMessage = response };
            });  
            
        } else {
            if(angular.element(".datepicker-element-1").hasClass('ng-invalid-required')) { $scope.datepickerFirstError = true; } else { $scope.datepickerFirstError = false; }
            if(angular.element(".datepicker-element-2").hasClass('ng-invalid-required')) { $scope.datepickerSecondError = true; } else { $scope.datepickerSecondError = false; }
        }
    }
    $scope.downloadDocument = function(id) {
        window.open(settings.prefix + 'vacations/' + id + '/download', '_blank');
    }
});
App.controller('importPrompt', function($scope, close, $http, $rootScope, $location){
    $scope.messages = { errorMessage: false, alertMessage: false };
    $scope.exportValues = function(selection) {
        $scope.setLoader = true;
        $scope.loading = true;
        $http({
			method: "PUT",
			url: settings.prefix+ 'months/' + selection + '/projects',
			data: $scope.importedProjects,
		}).then(function( response ){
		    $scope.loading = false;
		    var message = response.data.message;
		    $scope.messages.alertMessage = message;
		    $scope.close(1);
		}, function(response){
		    $scope.loading = false;
		    var message = response.data.message;
		    $rootScope.changedProjects = response.data.project_ids;
	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText } 
	        $scope.close(0);
		});
    };
    
    $scope.close = function(result) {
        close(result, 2500);
    };
});
App.controller('helpPrompt', function($scope, close){
    $scope.close = function(result) {
        close(result, 500);
    };
});
App.controller('userConfirmModal', function($scope, close, $http, $rootScope, $location, $filter){
    $scope.messages = { errorMessage: false, alertMessage: false };
    $scope.user = {};
    $scope.add = function() {
        var submitData = {};
        submitData = $scope.user;
        submitData['active_since'] = $filter('date')($scope.user.active_since, 'yyyy-MM-dd');
        $http({
			method: "POST",
			url: settings.prefix+ 'users',
			data: submitData,
		}).then(function( response ){
			$scope.close(true);
		}, function(response){
		    var message = response.data.message;
	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
		});
    }
    
    $scope.close = function(result) {
        close(result, 500);
    };
});
App.controller('wagePrompt', function($scope, close, $http, $rootScope, $location){
    $scope.messages = { errorMessage: false, alertMessage: false };
    $scope.resetUserWage = function() {
        $scope.setLoader = true;
        $scope.loading = true;
        $http({
			method: "PUT",
			url: settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users/' + $scope.userId + '/reset'
		}).then(function( response ){
		    $scope.loading = false;
		    var message = response.data.message;
		    $scope.messages.alertMessage = message;
		    $scope.close(true);
		}, function(response){
		    $scope.loading = false;
		    var message = response.data.message;
	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
		});
    }
    $scope.close = function(result) {
        close(result, 500);
    };
});
App.controller('addProject', function($scope, close, $http, $rootScope, $routeParams){
    $scope.model = {};
    $scope.autoModel = {};
    $scope.projectForm = {};
    $scope.model.billable = 1;
    $scope.billableValues = [
        { name: 'Billable', value: 1 },
        { name: 'Unbillable', value: 0 }
    ]
    $scope.getProject = function(val) {
        return $http.get(settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/projects/search/auto', {
            params: {
                name: val,
            }
        }).then(function(response){
            return response.data.map(function(item){
                return item;
            });
        });
    };
    $scope.setProjectProperties = function(item) {
        angular.forEach(item, function(value, key) {
            $scope.model[key] = value;
        })
    }
    $scope.close = function(result) {
        close(result, 500);
    };
    $scope.add = function(form) {
        if(form.$valid) {
            var submitModel = angular.copy($scope.model);
            if(!submitModel.name && typeof $scope.autoModel.selectedProject != 'object') { submitModel.name = $scope.autoModel.selectedProject; }
            var submitData = [];
            submitModel['minutes'] = submitModel['hours']*60;
            delete submitModel['hours'];
            submitData.push(submitModel);
            $http({
    			method: "POST",
    			url: settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users/' + $routeParams.id + '/projects',
    			data: submitData,
    		}).then(function( response ){
    			$scope.close(true);
    		}, function(response){
    		    var message = response.data.message;
    	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
    		});
        };
    }
});
App.controller('userModal', function($scope, close, $route, $http, $rootScope, $filter, $location, ModalService){
    
    $scope.messages = { alertMessage: false, errorMessage: false };
    
    $scope.addedFile = {};
    $scope.setVatMin = false;
    $scope.userForm = {};
    $scope.user = {};
    $scope.editForm = false;
    $scope.salaryVal = ['Põhipalk', 'Tunnipõhine', 'Valemipõhine']
    $scope.activatedVal = [false, true];
    $scope.popup = { first: false, second: false };
    $scope.open_first = function() { $scope.popup.first = true; };
    $scope.open_second = function() { $scope.popup.second = true; };
    $scope.dateOptions = { startingDay: 1 };
    
    $scope.close = function(result) {
        close(result, 1000);
    };
    
    $scope.reloadThis = function() {
        $route.reload();
    }
    $scope.downloadDocument = function(id, path) {
        window.open(settings.prefix + 'users/files/' + id + '/download', '_blank');
    }
    $scope.deleteDocument = function(id) {
        $http.delete(settings.prefix+ 'users/files/' + id).then(function(response){
            $scope.messages.alertMessage = response.data.message;
            for(var item in $scope.user.files) {
                if($scope.user.files[item].id == id) {
                    $scope.user.files.splice(item, 1);    
                }
            }
        })
    }
    $scope.setAdmin = function(value) {
        var submitData = {
            "admin": value
        };
        $http({
			method: "PUT",
			url: settings.prefix+ 'users/' + $scope.user.id + '/admin',
			data: submitData,
		}).then(function( response ){
            $scope.messages.alertMessage = response.data.message;
		}, function(response){
		    var message = response.data.message;
	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
		});
    };
    $scope.edit = function(user, form, uploadedFile) {
        if(form.$valid) {
            var submitData = {};
            submitData = angular.copy(user);
            submitData['active_since'] = $filter('date')(user.active_since, 'yyyy-MM-dd');
            submitData['vat_min_since'] = $filter('date')(user.vat_min_since, 'yyyy-MM-dd');
            
            var fd = new FormData();
            fd.append('file', uploadedFile); 
            var data ={};    
            
            angular.forEach(submitData, function(value, key){
                if(submitData[key] || submitData[key] == 0) {
                    data[key] = value;
                    fd.append(key, data[key]);  
                }
            });
            $http.put(settings.prefix+ 'users/' + submitData.id, fd, {
                headers : {
                    'Content-Type' : undefined
                },
                transformRequest : angular.identity
            }).success(function(response) {
                $scope.datepickerFirstError = false;
    		    $scope.datepickerSecondError = false;
    		    $scope.roleError = false;
    		    if(response.data && response.data.message) {
    		        $scope.messages.alertMessage = response.data.message;    
    		    } else { $scope.messages.alertMessage = response };
    			$scope.close(true); 
            }).error(function(response) {
                $scope.messages.errorMessage = response.message;
            });  
        } else {
            if(angular.element(".datepicker-element-1").hasClass('ng-invalid-required')) { $scope.datepickerFirstError = true; } else { $scope.datepickerFirstError = false; }
            if(angular.element(".datepicker-element-2").hasClass('ng-invalid-required')) { $scope.datepickerSecondError = true; } else { $scope.datepickerSecondError = false; }
            if(angular.element("#role").hasClass('ng-invalid-required')) { $scope.roleError = true; } else { $scope.roleError = false; }
        }
    }
    
    $scope.add = function(form) {
        var submitData = {};
        submitData = angular.copy($scope.user);
        submitData['active_since'] = $filter('date')($scope.user.active_since, 'yyyy-MM-dd');
        $http({
			method: "POST",
			url: settings.prefix+ 'users',
			data: submitData,
		}).then(function( response ){
		    $scope.messages.alertMessage = response.data.message;
			$scope.close(true);
		}, function(response){
		    var message = response.data.message;
	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
		});
    }
    
    $scope.openUserConfirm = function(form, uploadedFile){
        if(form.$valid) {
            $http.get(settings.prefix + 'users?' + 'first_name=' + $scope.user['first_name'] + '&last_name=' + $scope.user['last_name']).then(function(response) {
                var submitData = {};
                submitData = angular.copy($scope.user);
                submitData['active_since'] = $filter('date')($scope.user.active_since, 'yyyy-MM-dd');
                submitData['vat_min_since'] = $filter('date')($scope.user.vat_min_since, 'yyyy-MM-dd');
                
                var fd = new FormData();
                if(uploadedFile && uploadedFile != null) {
                    fd.append('file', uploadedFile);     
                }
                var data ={};    
                
                angular.forEach(submitData, function(value, key){
                    if(submitData[key] && submitData[key] != null) {
                        data[key] = value;
                        fd.append(key, data[key]);  
                    }
                });
                $http.post(settings.prefix+ 'users', fd, {
                    headers : {
                        'Content-Type' : undefined
                    },
                    transformRequest : angular.identity
                }).success(function(response) {
                    $scope.datepickerFirstError = false;
        		    $scope.datepickerSecondError = false;
        		    $scope.roleError = false;
        		    if(response.data && response.data.message) {
        		        $scope.messages.alertMessage = response.data.message;    
        		    } else { $scope.messages.alertMessage = response };
        			$scope.close(true); 
                }).error(function(response) {
                    $scope.datepickerFirstError = false;
        		    $scope.datepickerSecondError = false;
        		    $scope.roleError = false;
                    if(response.data && response.data.message) {
        		        $scope.messages.errorMessage = response.data.message;    
        		    } else { $scope.messages.errorMessage = response };
                });  
            }, function(response) {
                ModalService.showModal({
                    templateUrl: 'userConfirmModal.html',
                    controller: "userConfirmModal"
                }).then(function(modal) {
                    modal.scope.modalHeader = "Kasutaja juba olemas";
                    modal.scope.user = $scope.user;
                    modal.element.show();  
                    modal.close.then(function(result) {
                        if(result) { $scope.close(true); };
                    });
                });
            });
        } else {
            if(angular.element(".datepicker-element-1").hasClass('ng-invalid-required')) { $scope.datepickerFirstError = true; } else { $scope.datepickerFirstError = false; }
            if(angular.element(".datepicker-element-2").hasClass('ng-invalid-required')) { $scope.datepickerSecondError = true; } else { $scope.datepickerSecondError = false; }
            if(angular.element("#role").hasClass('ng-invalid-required')) { $scope.roleError = true; } else { $scope.roleError = false; }
        }
    };
});
App.controller('navbar', function($scope, ModalService) {
    $scope.activeUser = sessionStorage.user;
    $scope.setCollapsed = true;
    $scope.promptImport = function(){
        ModalService.showModal({
            templateUrl: 'helpPrompt.html',
            controller: "helpPrompt"
        }).then(function(modal) {
            modal.element.show();  
            modal.close.then(function(result) { });
        });
    };
})
App.controller('password', function($scope, $http, $rootScope, $routeParams, $location, $timeout){
	
	$scope.getLinkStatus = function() {
		$http.get(settings.prefix + 'password_reset?email=' + $location.search().email + '&code=' + $location.search().code).then(function(response) {
			$scope.acceptedLink = true;
			$scope.currentEmail = $location.search().email;
			$scope.linkStatusReceived = true;
		}, function(response){
			$scope.currentEmail = $location.search().email;
			$scope.messages.errorMessage = response.data.message;
			$scope.acceptedLink = false;
			$scope.linkStatusReceived = true;
			// $timeout(function () {
			// 	$location.search({});
   //             $location.path('/login');
   //         }, 2000); 
		});
		
	};
	$scope.getLinkStatus();
	
	$rootScope.hideNavbar = true;
	$scope.messages = { errorMessage : false, alertMessage: false }; 
	
	$scope.$watch('messages.errorMessage', function(newValue, oldValue) {
	    if(newValue != oldValue) {
	        $timeout(function () {
                $scope.messages.errorMessage = false;
            }, 2500);      
	    }
	});
	
	$scope.$watch('messages.alertMessage', function(newValue, oldValue) {
	    if(newValue != oldValue) {
	        $timeout(function () {
                $scope.messages.alertMessage = false;
            }, 2500);      
	    }
	});
	
	$scope.openPasswordForm = function() {
	    $location.path('/login');
	};
	$scope.changePassword = function(password, repeated, form){
	    
	    var parseQueryString = function() {
            var str = window.location.search;
            var objURL = {};
        
            str.replace(
                new RegExp( "([^?=&]+)(=([^&]*))?", "g" ),
                function( $0, $1, $2, $3 ){
                    objURL[ $1 ] = $3;
                }
            );
            return objURL;
        };
        
        var params = parseQueryString();
		
		if(form.$valid) {
		    if(angular.equals(password, repeated)) {
				$http({
					method: "PUT",
					url: settings.prefix + 'password_reset?email=' + $location.search().email + '&code=' + $location.search().code,
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					data: {
						password: password,
					},
		    		transformRequest: function(obj) {
			            var str = [];
			            for(var p in obj)
			               str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
			            return str.join("&");
		    		}
				}).then(function( response ){
					$scope.messages.alertMessage = response.data.message;
				}, function(response){
					$scope.messages.errorMessage = response.data.message;
				});
			} else {
				$scope.messages.errorMessage = "Sisestatud paroolid ei ühti, proovi uuesti!";
			}
		} 
	}
})
App.controller('profile', function($scope, $http, $rootScope, $routeParams, $location, $route){
    
    $scope.emailRecipients = [];
    
    $scope.removeRecipient = function(index) {
        $scope.emailRecipients.splice(index, 1);
        $scope.saveEmailRecipients();
    };
    
    $scope.addRecipient = function() {
        $scope.emailRecipients.push({ email: ""});
    };
    
    $scope.getStatus = function() {
    	$http.get(settings.prefix + 'ip_check').then(function( response ){
			if(response.data.status) { $scope.ipCheckStatus = true; } else { $scope.ipCheckStatus = false };
		}, function(response){
		});
    }
    $scope.changeStatus = function(currentStatus) {
    	$http({
    		method: "PUT",
			url: settings.prefix+ 'ip_check_toggle',
		}).then(function( response ){
			$scope.messages.alertMessage = response.data;
		}, function(response){
		});
    } 
    $scope.$watch('actives.thirdActive', function(newValue, oldValue){
    	if(newValue) { 
    		$scope.getAddressList(); 
    		$scope.getStatus();
    	};	
    })
    $scope.$watch('actives.secondActive', function(newValue, oldValue){
    	if(newValue) { 
    		$scope.getOutlookStatus();
    	};	
    })
    $scope.$watch('actives.fourthActive', function(newValue, oldValue){
    	if(newValue) { 
    		$scope.getEmailRecipients();
    	};	
    })
    $scope.$watch('actives.fifthActive', function(newValue, oldValue){
    	if(newValue) { 
    		$scope.getAccountant();
    	};	
    })
    
    $scope.getEmailRecipients = function() {
    	$scope.emailRecipients = [];
        $http.get(settings.prefix + 'config/carbon').then(function(response) {
            if(response.data.value) {
                var recipientArray = (response.data.value).split(',');
                for(var elem in recipientArray) {
                    $scope.emailRecipients.push({ email: recipientArray[elem] });
                }
            } else {
                $scope.emailRecipients.push({ email: "" });
            }
        }, function(response) {
            $scope.messages.errorMessage = response.data.message;
        });
    }
    
    $scope.saveEmailRecipients = function() {
        
        var submitData = {};
        var recipientArray = [];
        for(var elem in $scope.emailRecipients) {
            if($scope.emailRecipients[elem].email != undefined) {
                recipientArray.push($scope.emailRecipients[elem].email);
            };
        }
        submitData['cc'] = recipientArray.join(',');
        if(submitData.cc == "") { submitData.cc = " "; }
        $http({
			method: "PUT",
			url: settings.prefix+ 'config/carbon',
			data: submitData,
		}).then(function( response ){
		    $scope.messages.myMessage = true;
		}, function(response){
		    var message = response.data.message;
	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
		});
		
    }
    
    
    $scope.messages = { errorMessage: false, alertMessage: false };
	$scope.outlookPlaceholder = "";

	$scope.accountant_model = { };
	$scope.pw_model = {};
	$scope.actives = {};
    $scope.actives.firstActive = true;
    $scope.actives.secondActive = false;
    $scope.actives.thirdActive = false;
    $scope.actives.fourthActive = false;
    $scope.actives.fifthActive = false;
	$scope.activeUser = sessionStorage.user;    
    
    $scope.getOutlookStatus = function() {
        $http.get(settings.prefix+"config/outlook").then(function(response) {
            $scope.outlookPasswordStatus = response.data.email_password;
            if($scope.outlookPasswordStatus) {
            	$scope.outlookPlaceholder = "********";
            };
            $scope.statusReceived = true;
        }, function(response){
			$scope.messages.errorMessage = response.data.message;
			$scope.statusReceived = true;
		});
    };
    
    $scope.getAddressList = function() {
    	$scope.additionList = [{ address: ""}];
    	$scope.addressList = false;
    	$http.get(settings.prefix+"config/ip_whitelist").then(function(response) {
            $scope.addressList = response.data.ip_whitelist.split(',');
            $scope.addressesReceived = true;
        }, function(response){
			$scope.messages.errorMessage = response.data.message;
			$scope.addressesReceived = true;
		});
    }
    
    $scope.addAddressBar = function() {
    	$scope.additionList.push({ address: "" });
    }
    $scope.removeAddressListItem = function(index) {
    	$scope.addressList.splice(index, 1);
    }
    
    $scope.saveAddresses = function() {
    	
    	var submitData = {};
        var addressArray = [];
        for(var elem in $scope.additionList) {
        	if($scope.additionList[elem].address && $scope.additionList[elem].address != "") {
            	addressArray.push($scope.additionList[elem].address);
        	}
        }
        for(var item in $scope.addressList) {
        	if($scope.addressList[item] && $scope.addressList[item] != "") {
        		addressArray.push($scope.addressList[item]);	
        	}
        }
        var val = null;
        if(!addressArray.length) { submitValue = val } else { var submitValue = addressArray.join(','); }
    	
        $http({
			method: "PUT",
			url: settings.prefix+"config/ip_whitelist",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: {
				ip_whitelist: submitValue
			},
    		transformRequest: function(obj) {
	            var str = [];
	            for(var p in obj)
	            	if(obj[p] != "null" && obj[p] != null) {
	            		str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p])); 
	            	} else { 
	            		console.log(obj[p]); 
	            		str.push(encodeURIComponent(p) + "=" + obj[p]) 
	            	}
	            return str.join("&");
    		}
		}).then(function( response ){
			$scope.messages.alertMessage = response.data.message;
			$scope.getAddressList();
		}, function(response){
			$scope.messages.errorMessage = response.data.message;
		});
    }
    $scope.setOutlookPassword = function(password) {
        $http({
			method: "PUT",
			url: settings.prefix+"config/outlook",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: {
				email_password: password
			},
    		transformRequest: function(obj) {
	            var str = [];
	            for(var p in obj)
	               str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
	            return str.join("&");
    		}
		}).then(function( response ){
			$scope.messages.alertMessage = response.data.message;
			$scope.getOutlookStatus();
		}, function(response){
			$scope.messages.errorMessage = response.data.message;
		});
    }
    $scope.setAccountant = function(accountant) {
    	$http({
			method: "POST",
			url: settings.prefix+"config/accountant",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: {
				value: accountant
			},
    		transformRequest: function(obj) {
	            var str = [];
	            for(var p in obj)
	               str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
	            return str.join("&");
    		}
		}).then(function( response ){
			$scope.messages.alertMessage = response.data.message;
		}, function(response){
			$scope.messages.errorMessage = response.data.message;
		});
    }
    $scope.getAccountant = function() {
    	$http.get(settings.prefix + 'config/accountant').then(function( response ){
    		$scope.accountant_model['email'] = response.data.value;
		}, function(response){
		});
    }
    
    $scope.changePassword = function(password, repeated, form){
    	if(form.$valid) {
			if( password && repeated && angular.equals(password, repeated) ) {
				$http({
					method: "PUT",
					url: settings.prefix+"config/password",
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					data: {
						password: password
					},
		    		transformRequest: function(obj) {
			            var str = [];
			            for(var p in obj)
			               str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
			            return str.join("&");
		    		}
				}).then(function( response ){
					$scope.messages.alertMessage = response.data.message;
				}, function(response){
					$scope.messages.errorMessage = response.data.message;
				});
			} else {
				$scope.messages.errorMessage = "Sisestatud paroolid ei ühti, proovi uuesti!"
			}
    	}
	}
})
App.controller('statistics', function($scope, $http, $rootScope, $routeParams, $location, $filter){
    $scope.messages = { alertMessage: false, errorMessage: false };
    $scope.enableMonthRevenueTable = false;
    $scope.enableMonthHourTable = false;
    $scope.enableYearRevenueTable = false;
    $scope.enableYearHourTable = false;
    $scope.yearLoader = true;
    $scope.monthLoader = true;
    $scope.monthValues = [];
    $scope.selectedGeneralItem = 1;
    if(!$scope.statisticSelectedMonth) { $scope.statisticSelectedMonth = $rootScope.finalPreviousMonth; }   
    if(!$scope.yearStatisticSelectedMonth) { $scope.yearStatisticSelectedMonth = $rootScope.finalPreviousMonth; }   
    
    $scope.sortBySum = function(arr) {
	    var resultArr = arr.sort(function(first, second) {
	        return (parseInt(first.minutes_client + first.minutes_internal, 10) - parseInt(second.minutes_client + second.minutes_internal, 10));
	    });
	    return resultArr.reverse();
	};
    
    $scope.setTab = function(modifier) {
        $scope.enableMonthRevenueTable = false;
        $scope.enableMonthHourTable = false;
        $scope.enableYearRevenueTable = false;
        $scope.enableYearHourTable = false;
        if(modifier) { $scope.firstActive = true; $scope.secondActive = false; } else { $scope.firstActive = false; $scope.secondActive = true; }
        if(modifier) { $scope.getMonthStatistics($scope.statisticSelectedMonth); } else { $scope.getYearStatistics($scope.yearStatisticSelectedMonth); }
    }

    $scope.setMonthRevenueTableVisible = function() {
        $scope.enableMonthRevenueTable = !$scope.enableMonthRevenueTable;
    }
    $scope.setMonthHourTableVisible = function() {
        $scope.enableMonthHourTable = !$scope.enableMonthHourTable;
    }
    $scope.setYearRevenueTableVisible = function() {
        $scope.enableYearRevenueTable = !$scope.enableYearRevenueTable;
    }
    $scope.setYearHourTableVisible = function() {
        $scope.enableYearHourTable = !$scope.enableYearHourTable;
    }
    $scope.setGeneralItem = function(nr) {
        $scope.selectedGeneralItem = nr;
        $scope.yearGeneralRows = [];
        
        var monthList = $filter('orderBy')($scope.yearStatistics.months, 'month_of_year');
        if($scope.selectedGeneralItem == 1) { var activeGeneralItem = 'avg_hour_rate'; $scope.activeGeneralItemLabel = "Tegelik billitava töö tunnihind" }
        if($scope.selectedGeneralItem == 2) { activeGeneralItem = 'gross_margin'; $scope.activeGeneralItemLabel = "Brutomarginaal"  }
        if($scope.selectedGeneralItem == 3) { activeGeneralItem = 'total_billing_rate'; $scope.activeGeneralItemLabel = "Billing rate (sh siseprojektid)"  }
        if($scope.selectedGeneralItem == 4) { activeGeneralItem = 'client_billing_rate'; $scope.activeGeneralItemLabel = "Billing rate (siseprojektideta)"  }
        for(var month in monthList) {
            $scope.yearGeneralRows.push({ 'c': [
                { 'v': $rootScope.monthLabels[monthList[month].month_of_year - 1] },
                { 'v': monthList[month][activeGeneralItem] },
            ]});
        }
        $scope.drawYearGeneralChart();
    }
    
    var startYear = 2017;
    var startMonth = 1;
    for(var i = startYear; i <= $rootScope.thisYearNum; i++) {
        if(i == $rootScope.thisYearNum) { var endMonth = $rootScope.prevMonthNum; startMonth = 1 } else { endMonth = 12 }
        for(var j = startMonth; j <= endMonth; j++) {
            if(j < 10) { var monthVal = j.toString(); monthVal = '0' + monthVal } else { monthVal = j };
            var monthHash = {};
            monthHash['label'] = $rootScope.monthLabels[j-1] + ' ' + i;
            monthHash['name'] = i + '-' + monthVal;    
            $scope.monthValues.push(monthHash);
        }
    }
    $scope.monthValues = $scope.monthValues.reverse();
    
	$scope.firstActive = true;
	$scope.secondActive = false;
	
	$scope.getMonthStatistics = function(month) {
        if(month) {
            $scope.enableMonthRevenueTable = false;
            $scope.enableMonthHourTable = false;
            $scope.enableYearRevenueTable = false;
            $scope.enableYearHourTable = false;
            $scope.messages.errorMessage = false;
            $scope.monthRevenueChart = false;
            $scope.monthHourChart = false;
            $scope.monthStatistics = false;
            $scope.monthLoader = true;
            $http.get(settings.prefix+ 'months/' + month + '/statistics').then(function(response) {
                $scope.monthStatistics = response.data;
                var userListByRevenue = $filter('orderBy')($scope.monthStatistics.users, '-revenue');
                var userListByClientProjects = $scope.sortBySum($scope.monthStatistics.users);
                $scope.monthStatistics.minutes_client_project_floored = Math.floor($scope.monthStatistics.minutes_client_project/60);
                $scope.monthStatistics.minutes_client_project_remainder = $scope.monthStatistics.minutes_client_project%60;
                $scope.monthStatistics.minutes_internal_project_floored = Math.floor($scope.monthStatistics.minutes_internal_project/60);
                $scope.monthStatistics.minutes_internal_project_remainder = $scope.monthStatistics.minutes_internal_project%60;
                $scope.monthRevenueRows = [];
                $scope.monthHourRows = [];
                for(var user in userListByRevenue) {
                    $scope.monthRevenueRows.push({ 'c': [
                        { 'v': userListByRevenue[user].first_name + ' ' + userListByRevenue[user].last_name },
                        { 'v': parseInt(userListByRevenue[user].revenue) },
                        { 'v': parseInt(userListByRevenue[user].expense) },
                    ]});
                }
                for(var user in userListByClientProjects) {
                    $scope.monthHourRows.push({ 'c': [
                        { 'v': userListByClientProjects[user].first_name + ' ' + userListByClientProjects[user].last_name },
                        { 'v': userListByClientProjects[user].minutes_client/60, f: Math.floor(userListByClientProjects[user].minutes_client/60) + 'h ' + (userListByClientProjects[user].minutes_client%60) + 'min'},
                        { 'v': userListByClientProjects[user].minutes_internal/60, f: Math.floor(userListByClientProjects[user].minutes_internal/60) + 'h ' + (userListByClientProjects[user].minutes_internal%60) + 'min'},
                    ]});
                }
                $scope.drawMonthRevenueChart();
                $scope.drawMonthHourChart();
                $scope.monthLoader = false;
            }, function(response) {
                var message = response.data.message;
		        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }               
		        $scope.monthLoader = false;
            });
            for(var item in $scope.monthValues) {
                if($scope.monthValues[item].name == month) {
                    $scope.activeMonthLabel = $scope.monthValues[item].label;
                }
            }
        }
    };
    $scope.getYearStatistics = function(year) {
        if(year) {
            $scope.yearStatistics = false;
            $scope.yearLoader = true;
            $http.get(settings.prefix+ 'years/' + year + '/statistics').then(function(response){ 
                $scope.yearStatistics = response.data;
                $scope.yearStatistics.minutes_client_project_floored = Math.floor($scope.yearStatistics.minutes_client_project/60);
                $scope.yearStatistics.minutes_client_project_remainder = $scope.yearStatistics.minutes_client_project%60;
                $scope.yearStatistics.minutes_internal_project_floored = Math.floor($scope.yearStatistics.minutes_internal_project/60);
                $scope.yearStatistics.minutes_internal_project_remainder = $scope.yearStatistics.minutes_internal_project%60;
                var lastMonth = response.data.last_month;
                $scope.yearStatisticsStartMonth = $rootScope.monthLabels[0];
                $scope.yearStatisticsEndMonth = $rootScope.monthLabels[lastMonth-1];
                
                var userListByRevenue = $filter('orderBy')($scope.yearStatistics.users, '-revenue');
                var userListByClientProjects = $scope.sortBySum($scope.yearStatistics.users);
                var monthList = $filter('orderBy')($scope.yearStatistics.months, 'month_of_year');
                $scope.yearRevenueRows = [];
                $scope.yearHourRows = [];
                $scope.yearGeneralRows = [];
                
                for(var user in userListByRevenue) {
                    $scope.yearRevenueRows.push({ 'c': [
                        { 'v': userListByRevenue[user].first_name + ' ' + userListByRevenue[user].last_name },
                        { 'v': parseInt(userListByRevenue[user].revenue) },
                        { 'v': parseInt(userListByRevenue[user].expense) },
                    ]});
                }
                for(var user in userListByClientProjects) {
                    $scope.yearHourRows.push({ 'c': [
                        { 'v': userListByClientProjects[user].first_name + ' ' + userListByClientProjects[user].last_name },
                        { 'v': userListByClientProjects[user].minutes_client/60, f: Math.floor(userListByClientProjects[user].minutes_client/60) + 'h ' + (userListByClientProjects[user].minutes_client%60) + 'min' },
                        { 'v': userListByClientProjects[user].minutes_internal/60, f: Math.floor(userListByClientProjects[user].minutes_internal/60) + 'h ' + (userListByClientProjects[user].minutes_internal%60) + 'min' },
                    ]});
                }
                
                if($scope.selectedGeneralItem == 1) { var activeGeneralItem = 'avg_hour_rate'; $scope.activeGeneralItemLabel = "Tegelik billitava töö tunnihind" }
                if($scope.selectedGeneralItem == 2) { activeGeneralItem = 'gross_margin'; $scope.activeGeneralItemLabel = "Brutomarginaal"  }
                if($scope.selectedGeneralItem == 3) { activeGeneralItem = 'total_billing_rate'; $scope.activeGeneralItemLabel = "Billing rate (sh siseprojektid)"  }
                if($scope.selectedGeneralItem == 4) { activeGeneralItem = 'client_billing_rate'; $scope.activeGeneralItemLabel = "Billing rate (siseprojektideta)"  }
                for(var month in monthList) {
                    $scope.yearGeneralRows.push({ 'c': [
                        { 'v': $rootScope.monthLabels[monthList[month].month_of_year - 1] },
                        { 'v': monthList[month][activeGeneralItem] },
                    ]});
                }
                $scope.drawYearRevenueChart();
                $scope.drawYearHourChart();
                $scope.drawYearGeneralChart();
                $scope.yearLoader = false;
            }, function(response) {
                var message = response.data.message;
		        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }
		        $scope.yearLoader = false;
            });  
        }
    };
    
    $scope.getMonthStatistics($scope.statisticSelectedMonth);
    
    //month charts
    $scope.drawMonthRevenueChart = function() {
        $scope.monthRevenueChart = {};
        $scope.monthRevenueChart.type = "ColumnChart";
        $scope.monthRevenueChart.data = {
            "cols": [
                {id: 1, label: "Töötaja", type: "string"},
                {id: 2, label: "Tulu", type: "number"},
                {id: 2, label: "Kulu", type: "number"}
            ], 
            "rows": $scope.monthRevenueRows
        };
    
        $scope.monthRevenueChart.options = {
            'legend': { position: 'top', alignment: 'end' },
            'title': 'Raha',
            "colors": ['#3366cc', '#dc3912'],
            'chartArea': {'width': '85%'},
            'hAxis' : { 
                showTextEvery: 1,
                slantedText: true,
                slantedTextAngle: 45,
                textStyle : {
                    fontSize: 9
                }
            }
        };    
    }
    $scope.drawMonthHourChart = function() {
        $scope.monthHourChart = {};
        $scope.monthHourChart.type = "ColumnChart";
        $scope.monthHourChart.data = {
            "cols": [
                {id: 1, label: "Töötaja", type: "string"},
                {id: 2, label: "Tunnid kliendiprojektides", type: "number"},
                {id: 3, label: "Tunnid siseprojektides", type: "number"},
            ], 
            "rows": $scope.monthHourRows
        };
    
        $scope.monthHourChart.options = {
            'legend': { position: 'top', alignment: 'end' },
            'title': 'Töötunnid',
            "colors": ['#FF9900', '#990099'],
            'chartArea': {'width': '85%'},
            'isStacked': true,
            'hAxis' : {
                showTextEvery: 1,
                slantedText: true,
                slantedTextAngle: 45,
                textStyle : {
                    fontSize: 9
                }
            }
        };    
    }
    
    //year-charts
    $scope.drawYearRevenueChart = function() {
        $scope.yearRevenueChart = {};
        $scope.yearRevenueChart.type = "ColumnChart";
        $scope.yearRevenueChart.data = {
            "cols": [
                {id: 1, label: "Töötaja", type: "string"},
                {id: 2, label: "Tulu", type: "number"},
                {id: 2, label: "Kulu", type: "number"}
            ], 
            "rows": $scope.yearRevenueRows
        };
    
        $scope.yearRevenueChart.options = {
            'legend': { position: 'top', alignment: 'end' },
            'title': 'Raha',
            "colors": ['#3366cc', '#dc3912'],
            'chartArea': {'width': '85%'},
            'hAxis' : { 
                showTextEvery: 1,
                slantedText: true,
                slantedTextAngle: 45,
                textStyle : {
                    fontSize: 9
                }
            }
        };    
    }
    $scope.drawYearHourChart = function() {
        $scope.yearHourChart = {};
        $scope.yearHourChart.type = "ColumnChart";
        $scope.yearHourChart.data = {
            "cols": [
                {id: 1, label: "Töötaja", type: "string"},
                {id: 2, label: "Tunnid kliendiprojektides", type: "number"},
                {id: 3, label: "Tunnid siseprojektides", type: "number"},
            ], 
            "rows": $scope.yearHourRows
        };
    
        $scope.yearHourChart.options = {
            'legend': { position: 'top', alignment: 'end' },
            'title': 'Töötunnid',
            "colors": ['#FF9900', '#990099'],
            'chartArea': {'width': '85%'},
            'isStacked': true,
            'hAxis' : { 
                showTextEvery: 1,
                slantedText: true,
                slantedTextAngle: 45,
                textStyle : {
                    fontSize: 9
                }
            }
        };    
    }
    $scope.drawYearGeneralChart = function() {
        $scope.yearGeneralChart = {};
        $scope.yearGeneralChart.type = "LineChart";
        $scope.yearGeneralChart.data = {
            "cols": [
                {id: 1, label: "Kuud", type: "string"},
                {id: 2, label: $scope.activeGeneralItemLabel, type: "number"},
            ], 
            "rows": $scope.yearGeneralRows
        };
    
        $scope.yearGeneralChart.options = {
            'legend': { position: 'top', alignment: 'end' },
            'title': $scope.activeGeneralItemLabel,
            "colors": ['#990099'],
            'chartArea': {'width': '85%'}
        };    
    }
})
App.controller('user-wage', function($scope, $http, $rootScope, $routeParams, $location, ModalService, $route, $anchorScroll){
    
    $scope.messages = { alertMessage: false, errorMessage: false, generalAlertMessage: false };
    $scope.salaryVal = ['Põhipalk', 'Tunnipõhine', 'Valemipõhine'];
    
    $scope.addCustomProject = function() {
        ModalService.showModal({
            templateUrl: 'addProject.html',
            controller: "addProject"
        }).then(function(modal) {
            modal.scope.modalHeader = "Lisa projekt";
            modal.element.show();  
            modal.close.then(function(result) {
                if(result) { $scope.user = false; $scope.getUserInfo(); }
            });
        });
    };
    $scope.deleteProject = function(id) {
        $http.delete(settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users/' + $routeParams.id + '/projects/' + id).then(function(response) {
            $route.reload();
        }, function(response) {
            $scope.messages.errorMessage = response.data.message;
        });
    }
    
    
    $scope.setProjectParameters = function(id, hours, rate, statistic) {
        var minutes = hours*60;
        $http({
			method: "PUT",
			url: settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users/' + $routeParams.id + '/projects/' + id,
			data: {
			    minutes: minutes,
			    rate_salary: rate,
			    rate_statistic: statistic
			},
		}).then(function( response ){
		    var message = response.data.message;
		  //  $scope.messages.alertMessage = message;
		  
		  $scope.getUserInfo();
		  
		}, function(response){
		    var message = response.data.message;
	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
		});
        
        $scope.total_minutes = 0;
        $scope.total_calculated_income = 0;
        $scope.total_calculated_statistic_income = 0;
        
        var allProjects = $scope.user.projects;
        for(var project in allProjects) {
            $scope.total_minutes += allProjects[project].hours*60;
            allProjects[project].calculated_income = allProjects[project].rate_salary*(allProjects[project].hours);
            allProjects[project].calculated_statistic_income = allProjects[project].rate_statistic*(allProjects[project].hours);
            $scope.total_calculated_income += parseInt(allProjects[project].calculated_income);
            $scope.total_calculated_statistic_income += parseInt(allProjects[project].calculated_statistic_income);
        }
        $scope.total_calculated_income = $scope.total_calculated_income.toFixed(2);
        $scope.total_calculated_statistic_income = $scope.total_calculated_statistic_income.toFixed(2);
        $scope.total_minutes_floored = Math.floor($scope.total_minutes/60);
        if(!$scope.total_minutes_floored) { $scope.total_minutes_floored = "00" };
        $scope.total_minutes_remainder = Math.floor($scope.total_minutes%60);
        if($scope.total_minutes_remainder == 0) { $scope.total_minutes_remainder = "00" };
        if(!$scope.total_minutes_remainder) { $scope.total_minutes_remainder = "" };
        // $scope.updateSalaryParameters($scope.user.one_time_model); // removed at 15.09, Silver
    };
    $scope.getUserInfo = function() {
        $http.get(settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users/' + $routeParams.id).then(function(response) {
            $scope.user = response.data;
            $scope.workdays = response.data.workdays;
            $scope.worked_days = response.data.worked_days;
            if (!$scope.user.confirmed) {
                $scope.user.workdays = response.data.workdays_dynamic;
                $scope.user.worked_days = response.data.worked_days_dynamic;
            }
            $scope.total_minutes = 0;
            $scope.total_calculated_income = 0;
            $scope.total_calculated_statistic_income = 0;
            
            var allProjects = $scope.user.projects;
            // console.log(allProjects)
            for(var project in allProjects) {
                allProjects[project].hours = (allProjects[project].minutes/60).toFixed(2);
                $scope.total_minutes += allProjects[project].minutes;
                var calculated_income = allProjects[project].rate_salary*(allProjects[project].minutes/60);
                var calculated_statistic_income = allProjects[project].rate_statistic*(allProjects[project].minutes/60);
                allProjects[project].calculated_income = calculated_income.toFixed(2);
                allProjects[project].calculated_statistic_income = calculated_statistic_income.toFixed(2);
                $scope.total_calculated_income += calculated_income;
                $scope.total_calculated_statistic_income += calculated_statistic_income;
            }
            $scope.total_calculated_income = $scope.total_calculated_income.toFixed(0);
            $scope.total_calculated_statistic_income = $scope.total_calculated_statistic_income.toFixed(0);
            $scope.total_minutes_floored = Math.floor($scope.total_minutes/60);
            if(!$scope.total_minutes_floored) { $scope.total_minutes_floored = "00" };
            $scope.total_minutes_remainder = Math.floor($scope.total_minutes%60);
            if($scope.total_minutes_remainder == 0) { $scope.total_minutes_remainder = "00" };
            // $scope.updateSalaryParameters($scope.user.one_time_model); // removed at 15.09, Silver
			if($scope.user.salary) {
			 //   $scope.user.salary = Math.round($scope.user.salary);
			}
            $scope.user.front_calc = {};
            $scope.user.front_calc.rate_month = ($scope.user.salary_calculated.rate_month * ($scope.worked_days/$scope.workdays)).toFixed(0);
            $scope.user.front_calc.bonus_threshold = ($scope.user.salary_calculated.bonus_threshold * ($scope.worked_days/$scope.workdays)).toFixed(0);
            $scope.user.front_calc.bonus = ($scope.user.salary - ($scope.user.salary_calculated.rate_month * ($scope.worked_days/$scope.workdays))).toFixed(0);
            
            if(!$scope.last_calculation_salary_model) { $scope.last_calculation_salary_model = $scope.user.salary_calculated.salary_model; };
        })
    };
    $scope.getUserInfo();
    
    $scope.goToNextWorker = function() {
        $location.path('wages/' + $rootScope.workerNextId);
    };
    $scope.goToPreviousWorker = function() {
        $location.path('wages/' + $rootScope.workerPrevId);
    };
    $scope.goToNextSourcer = function() {
        $location.path('wages/' + $rootScope.outSourcerNextId);
    };
    $scope.goToPreviousSourcer = function() {
        $location.path('wages/' + $rootScope.outSourcerPrevId);
    };
    
    $scope.setWorkedDays = function() {
        var user = angular.copy($scope.user);
        var workedDays = user.worked_days;
        var submitData = {};
        if(user.worked_days <= user.workdays) { 
            submitData['worked_days'] = user.worked_days;
            $http({
    			method: "PUT",
    			url: settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users/' + $routeParams.id + '/salary',
    			data: submitData,
    		}).then(function( response ){
    		  //  var message = response.data.message;
    		  //  $scope.messages.alertMessage = message;
    		  $scope.workedDaysFalse = false;
    		}, function(response){
    		    var message = response.data.message;
    	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
    		});
        } else if(user.worked_days > 0 && workedDays.toString().indexOf('.') == -1) {
            $scope.workedDaysFalse = true;
            var message = "Töötatud päevade arv ei saa olla suurem kui tööpäevade arv kuus";
	        if(message.length > 0) { $scope.messages.errorMessage = message; };  
	        window.scrollTo(0, 0);	
        }
    }
    $scope.setOverallWorkDays = function() {
        var user = angular.copy($scope.user);
        var submitData = {};
        if(user.worked_days <= user.workdays) { 
            submitData['workdays'] = user.workdays;
            $http({
    			method: "PUT",
    			url: settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users/' + $routeParams.id + '/salary',
    			data: submitData,
    		}).then(function( response ){
    		  $scope.overallWorkDaysFalse = false;
    		}, function(response){
    		    var message = response.data.message;
    	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
    		});
        } else {
            $scope.overallWorkDaysFalse = true;
            var message = "Tööpäevade arv ei saa olla väiksem kui töötatud tööpäevade arv kuus";
	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }  
	        window.scrollTo(0, 0);	
        }
    }
    
    $scope.updateExpenseParams = function() {
        var user = angular.copy($scope.user);
        var submitData = {};
        submitData = {
            'salary_model' : 1,
            'rate_month' : user.salary_calculated.rate_month,
            // 'comment' : user.comment,
            'one_time_model' : 1,
        };
        $http({
			method: "PUT",
			url: settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users/' + $routeParams.id + '/salary',
			data: submitData,
		}).then(function( response ){
		  //  var message = response.data.message;
		  //  $scope.messages.alertMessage = message;
		}, function(response){
		    var message = response.data.message;
	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
		});
    }
    
    $scope.updateSalaryParameters = function(modifier) {
        //Accepted keys: 'salary_model', 'rate_hour', 'rate_month', 'coefficient', 'bonus_threshold', 'worked_days', 'comment'
        var user = angular.copy($scope.user);
        var submitData = {};
        if(modifier == 1) { 
            submitData = {
                'salary_model' : user.salary_calculated.salary_model,
                'rate_hour' : user.salary_calculated.rate_hour,
                'rate_month' : user.salary_calculated.rate_month,
                'coefficient' : user.salary_calculated.coefficient,
                'bonus_threshold' : user.salary_calculated.bonus_threshold,
                'comment' : user.comment,
                'one_time_model' : user.one_time_model,
            };
        } else {
            if(user.salary_default.salary_model) {
                submitData = {
                    'salary_model' : user.salary_default.salary_model,
                    'rate_hour' : user.salary_default.rate_hour,
                    'rate_month' : user.salary_default.rate_month,
                    'coefficient' : user.salary_default.coefficient,
                    'bonus_threshold' : user.salary_default.bonus_threshold,
                    'comment' : user.comment,
                    'one_time_model' : user.one_time_model,
                };
            } else {
                submitData = {
                    'salary_model' : null
                }
            }
        }
        $http({
			method: "PUT",
			url: settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users/' + $routeParams.id + '/salary',
			data: submitData,
		}).then(function( response ){
		  //  var message = response.data.message;
		  //  $scope.messages.alertMessage = message;
		}, function(response){
		    var message = response.data.message;
	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
		});
    }
    
    $scope.calculateSalary = function(currentSalary) {
        // THIS UPDATES SALARY MODEL NOW
        var user = angular.copy($scope.user);
        var submitData = { 
            worked_days: user.worked_days,
            workdays: user.workdays,
            one_time_model: user.one_time_model ? user.salary_calculated : null,
        };
        $http({
			method: "PUT",
			url: settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users/' + $routeParams.id + '/calculate',
			data: submitData,
		}).then(function( response ){
		    $scope.user.salary = response.data.salary;
		    $scope.user.salary_calculated = response.data;
		    $scope.last_calculation_salary_model = $scope.user.salary_calculated.salary_model;
		    $scope.user = false;
		    $scope.getUserInfo();
		}, function(response){
		    var message = response.data.message;
	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
		});
    }
    $scope.isNumber = function(num) {
        if (num == null) {
            return false;
        } else {
            return true;
        };
    };
    
    $scope.calculateExpense = function(currentSalary) {
        var user = angular.copy($scope.user);
        var submitData = {
            worked_days: user.worked_days,
            workdays: user.workdays,
            one_time_model: user.salary_calculated
        };
        
        $http({
			method: "PUT",
			url: settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users/' + $routeParams.id + '/calculate',
			data: submitData,
		}).then(function( response ){
		  //  $scope.user.salary = response.data.salary;
		    $scope.user = false;
		    $scope.getUserInfo();
		}, function(response){
		    var message = response.data.message;
	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
		});    
    }
    $scope.confirmSalary = function() {
        
        var user = angular.copy($scope.user);
        var submitData = {
            comment: user.comment,
            salary_base: user.salary_base,
            salary_bonus: user.salary_bonus,
            salary: user.employee === 1 ? user.salary : user.salary_calculated.rate_month,
            minutes: user.minutes
        };
        // return console.log(submitData)
        $http({
			method: "PUT",
			url: settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users/' + $routeParams.id + '/confirm',
			data: submitData,
		}).then(function( response ){
		  //  $scope.scrollBottom = true;
		    $scope.user = false;
		    $scope.getUserInfo();
		}, function(response){
		    var message = response.data.message;
	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
		});
    }
    $scope.promptWage = function(id, role){
        ModalService.showModal({
            templateUrl: 'wagePrompt.html',
            controller: "wagePrompt"
        }).then(function(modal) {
            modal.scope.modalHeader = "Oled sa kindel?";
            if(role == "OUTSOURCER") {
                modal.scope.modalText = "Kulud juba lisatud! Kas soovid lähtestada?";
            } else { modal.scope.modalText = "Palk on juba arvutatud. Edasi liikudes kasutaja palgaarvestus lähtestatakse, kas soovid jätkata?"; }
            modal.scope.userId = $routeParams.id;
            modal.element.show();  
            modal.close.then(function(result) {
                $route.reload();
            });
        });
    };
});
App.controller('users', function($scope, $http, $route, $rootScope, $routeParams, $location, ModalService){
    
    $scope.roles = { "ADMIN": 'Administraator', "OUTSOURCER": "Allhankija", "USER": "Projektijuht", "EMPLOYEE": "Töötaja" };
    
    $scope.messages = { alertMessage: false, myMessage: false };
    $scope.activatedVal = [ "Deaktiveeritud", "Aktiveeritud" ];
    $scope.emailRecipients = [];
    
    $scope.outSourcerLast = function(item){
        return item.role !== 'OUTSOURCER';
    }
    
    $scope.removeRecipient = function(index) {
        $scope.emailRecipients.splice(index, 1);
        $scope.saveEmailRecipients();
    };
    
    $scope.addRecipient = function() {
        $scope.emailRecipients.push({ email: ""});
    };
    
	$scope.getUsers = function() {
        $http.get(settings.prefix + 'users').then(function(response) {
            $scope.users = response.data;
        }, function(response) {
            $scope.messages.errorMessage = response.data.message;
        });
    };
    $scope.getUsers();
    
    $scope.reloadThis = function() {
        $route.reload();
    }
    
    $scope.openUserEdit = function(id){
        ModalService.showModal({
            templateUrl: 'userModal.html',
            controller: "userModal"
        }).then(function(modal) {
            
            $http.get(settings.prefix+ 'users/' + id).then(function(response) {
                modal.scope.modalHeader = "Töötaja andmed";
                modal.scope.editForm = true;
                modal.scope.user = response.data;
                if(modal.scope.user && modal.scope.user.active_since) {
                    modal.scope.user.active_since = new Date(modal.scope.user.active_since);
                };
                if(modal.scope.user && modal.scope.user.vat_min_since) {
                    modal.scope.user.vat_min_since = new Date(modal.scope.user.vat_min_since);
                };
            }, function(response) {
                console.log(response);
            }).then(function(){
                modal.element.show();  
            });
            modal.close.then(function(result) {
                if(result) { $scope.reloadThis(); };
            });
        });
    };
    $scope.openUserAdd = function(){
        ModalService.showModal({
            templateUrl: 'userModal.html',
            controller: "userModal"
        }).then(function(modal) {
            modal.scope.modalHeader = "Uue töötaja lisamine"
            modal.element.show();  
            modal.close.then(function(result) {
                if(result) { $scope.reloadThis(); };
            });
        });
    };
    $scope.getEmailRecipients = function() {
        $http.get(settings.prefix + 'config/carbon').then(function(response) {
            if(response.data.value) {
                var recipientArray = (response.data.value).split(',');
                for(var elem in recipientArray) {
                    $scope.emailRecipients.push({ email: recipientArray[elem] });
                }
            } else {
                $scope.emailRecipients.push({ email: "" });
            }
        }, function(response) {
            $scope.messages.errorMessage = response.data.message;
        });
    }
    $scope.getEmailRecipients();
    
    $scope.saveEmailRecipients = function() {
        
        var submitData = {};
        var recipientArray = [];
        for(var elem in $scope.emailRecipients) {
            if($scope.emailRecipients[elem].email != undefined) {
                recipientArray.push($scope.emailRecipients[elem].email);
            };
        }
        submitData['cc'] = recipientArray.join(',');
        if(submitData.cc == "") { submitData.cc = " "; }
        $http({
			method: "PUT",
			url: settings.prefix+ 'config/carbon',
			data: submitData,
		}).then(function( response ){
		    $scope.messages.myMessage = true;
		}, function(response){
		    var message = response.data.message;
	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
		});
		
    }
})
App.controller('wages', function($scope, $http, $rootScope, $q,$routeParams, $location, $uibModal, $document, ModalService, $filter, $window, $route){
    
    var VACATION_TYPES = {
        'vacation': 'Puhkus',
        'unpaid': 'Palgata puhkus',
        'study': 'Õppepuhkus',
        'disability': 'Töövõimetus',
        'child': 'Lapsepuhkus',
        'other': 'Eemalviibimine'
    };
    
    $scope.loading = false;
    $scope.messages = { alertMessage: false, errorMessage: false };
    $scope.accountingEmailSent = false;
    $scope.statusReceived = false;
    $scope.outSourcer = "OUTSOURCER";
    
    $scope.getUsers = function() {
        $http.get(settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users').then(function(response) {
            $scope.users = response.data;
            $scope.wagesNotFound = false;
        }, function(response) {
            $scope.messages.errorMessage = response.data.message;
            $scope.wagesNotFound = true;
        });
    };
    $scope.getUsers();
    
    $scope.generateEmail = function() {
        $http.get(settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/accounting/email').then(function(response) {
            $scope.generatedEmail = response.data;
            
            var mailbody = '';
            var lb = '%0D%0A'; var sp = ' '; var tab = '    '; var eur = '€'; var semi = ':';
            mailbody += 'Brutopalgad'+lb;
            angular.forEach($scope.generatedEmail.users, function(value, key){
                mailbody += tab + key + semi + sp + value + sp + eur + lb;
            });
            $scope.sendMail("ksild@neti.ee", "Palgaarvestuse kokkuvõte " + $scope.previousMonthLabel + ' ' + $rootScope.thisYear, mailbody);
            $scope.accountingEmailSent = true;
            
        }, function(response) {
            $scope.messages.errorMessage = response.data.message;
        });
    }
    $scope.getAccountingStatus = function() {
        $http.get(settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/accounting/status').then(function(response) { 
            if(response.data.email_sent == 1) {
                $scope.accountingEmailSent = true;
            };
            $scope.statusReceived = true;
        }, function(response) {
            $scope.messages.errorMessage = response.data.message;
            $scope.statusReceived = true;
        });    
    };
    $scope.getAccountingStatus();
    
    $scope.openEmailView = function(id, first, last){
        ModalService.showModal({
            templateUrl: 'wageEmail.html',
            controller: "wageEmail"
        }).then(function(modal) {
            
            $http.get(settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users/' + id + '/email').then(function(response) {
                modal.scope.user = response.data;
                modal.scope.user.name = first + ' ' + last;
                modal.scope.userId = id;
                var monthLabel = $rootScope.previousMonthLabel;
                var yearLabel = $rootScope.thisYear;
                modal.scope.wageEmailTitle = monthLabel.charAt(0).toUpperCase() + monthLabel.substr(1) + ' ' + yearLabel + ' - ' + modal.scope.user.name;
            }, function(response) {
            }).then(function(){
                modal.element.show();  
            });
            modal.close.then(function(result) {
                if(result) { $route.reload() };
            });
        });
    };
    $scope.openAccountantPrompt = function() {
        ModalService.showModal({
            templateUrl: 'accountantPrompt.html',
            controller: "accountantPrompt"
        }).then(function(modal) {
            modal.scope.modalHeader = "Raamatupidaja e-mail"
            modal.scope.modalText = "E-mail juba saadetud, kas soovid uuesti saata?";
            modal.element.show();
            modal.close.then(function(result) {
                if(result) { $scope.openAccountantEmail() };
            });
        });
    }
    $scope.openAccountantEmail = function(){
        ModalService.showModal({
            templateUrl: 'accountantEmail.html',
            controller: "accountantEmail"
        }).then(function(modal) {
            $http.get(settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/accounting/email').then(function(response) {
                if(response.data.message) {
                    $scope.messages.errorMessage = response.data.message
                } else {
                    $scope.generatedEmail = response.data;
                    var userList = $filter('orderBy')($scope.generatedEmail.users, 'name');
                    var fileList = $scope.generatedEmail.files;
                    
                    var mailbody = '';
                    var lb = '%0D%0A'; var comma = ','; var sp = ' '; var tab = '    '; var eur = '€'; var semi = ':';
                    mailbody = '<p>Tere,</p>' +
                        '<p>Saadan eelmise kuu palgad.</p>' +
                        '<div>';
                    for(var user in userList) { 
                        var username = userList[user].name;
                        var vacationList = userList[user].vacations;
                        var salary_base = userList[user].salary_base.toFixed(0); 
                        var salary_bonus = userList[user].salary_bonus.toFixed(0); 
                        var salary_total = userList[user].salary_total.toFixed(0); 
                        var salary_model = userList[user].salary_model;

                        mailbody += '<span>' + username + semi + sp;
                        
                        // numbers
                        if (salary_model === 2) {
                            var rate_hour = userList[user].rate_hour.toFixed(2);
                            var hours = (userList[user].minutes / 60).toFixed(2);
                            mailbody += 'Tunnihind ' + rate_hour + sp + eur + ', töötatud tunde ' + hours; 
                        }
                        else {
                            // "Põhipalk x" on default, "Kogu kuu põhipalk x" if missing some workdays
                            var rate_month = userList[user].rate_month.toFixed(0); 
                            var salary_base_label = vacationList.length ? 'Kogu kuu põhipalk ' : 'Põhipalk ';
                            var about_bonus = (salary_model === 3) ? (comma + ' lisatasu ' + salary_bonus + sp + eur) : '';
                            mailbody += salary_base_label + rate_month + sp + eur + about_bonus; 
                        }
                        
                        var userFile = fileList.find(function(file){ return file.user_id === userList[user].id });
                        if(userList[user].vat_min_since && userFile) { mailbody += comma + sp + 'tulumaksuvaba miinimumi arvestatakse alates ' + $filter('date')(userList[user].vat_min_since, "dd.MM.yyyy") + comma + sp + "avaldus manuses"; }
                        if(userList[user].vacations.length) {
                            for(var vacation in vacationList) {
                                var vacation_type = VACATION_TYPES[vacationList[vacation].type];
                                var vacationFileExists = !!vacationList[vacation].file_path;
                                mailbody += comma + sp + vacation_type.toLowerCase() + sp + $filter('date')(vacationList[vacation].start_date, "dd.MM.yyyy") + '-' + $filter('date')(vacationList[vacation].end_date, "dd.MM.yyyy");     
                                if (vacationFileExists) mailbody += comma + sp + "puhkuseavaldus manuses"
                            }
                        }
                        
                        mailbody += '.</span><br />';
                    }
                    mailbody += '</div>' +
                        '<p>Tervitades,</p>' +
                        '<p>Ander</p>';
                    
                    modal.scope.model.files = fileList;
                    modal.scope.model.email_body = mailbody;
                    modal.element.show();
                }
            }, function(response) {
                $scope.messages.errorMessage = response.data.message;
            });
            
            modal.close.then(function(result) {
                if(result) { $route.reload() };
            });
        });
    };
    
    $scope.sendToAll = function() {
        $scope.loading = true;
        return $http({
            method: "POST",
        	url: settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users/email',
        	headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response){
            $scope.loading = false;
            $route.reload();
        }).catch(function(response){
            $scope.loading = false;
            $scope.messages.errorMessage = response.data.message;
        })
    }
    
    $scope.canSendToAll = function() {
        return !!$scope.getUsersConfirmedCount()
    }
    
    $scope.getUsersConfirmedCount = function() {
        return $scope.users && $scope.users.filter(function(user) {
            return user.confirmed && user.role != "OUTSOURCER" && user.email_sent != 1;
        }).length;
    }
    $scope.getUsersWithoutConfirmedCount = function() {
        return $scope.users && $scope.users.filter(function(user) {
            return !user.confirmed && user.role != "OUTSOURCER";
        }).length;
    }
});
App.controller('workdays', function($scope, $http, $route, $filter){
    
    $scope.model = {};
    $scope.day = ['Ei', 'Jah'];
    $scope.datepickerFirstError = false;
    $scope.datepickerSecondError = false;
    $scope.messages = { alertMessage: false, myMessage: false, errorMessage: false };
    $scope.popup_open1 = false;
    $scope.popup_open2 = false;
    $scope.open_popup1 = function() { $scope.popup_open1 = !$scope.popup_open1 };
    $scope.open_popup2 = function() { $scope.popup_open2 = !$scope.popup_open2 };
    
	$scope.getDates = function() {
	    $scope.model = {};
	    $scope.dates = false;
        $http.get(settings.prefix+'holidays').then(function(response) {
            $scope.dates = response.data;
            $scope.model['short_day'] = 0;
        }, function(response) {
            $scope.messages.errorMessage = response.data.message;
        });
    };
    $scope.compareDates = function(start, end) {
        var startAsObj = new Date(start);
        var endAsObj = new Date(end);
        if(startAsObj.getTime() === endAsObj.getTime()) { return true; }
        return false;
    };
    $scope.setEndDate = function() {
        if($scope.model.end_date == null) { $scope.model.end_date = new Date($scope.model.start_date) }
    };
    $scope.getDates();
    $scope.removeHoliday = function(id) {
        $http.delete(settings.prefix + 'holidays/' + id).then(function(response){
            $scope.messages.alertMessage = response.data.message;
            $scope.getDates();
        }, function(response) {
            $scope.messages.errorMessage = response.data.message;
        });
    }
    $scope.addHoliday = function(form) {
        if(form.$valid) {
            var submitData = {};
            submitData = angular.copy($scope.model);
            submitData.start_date = $filter('date')(submitData.start_date, 'yyyy-MM-dd');
            submitData.end_date = $filter('date')(submitData.end_date, 'yyyy-MM-dd');

            console.log(submitData);
            $http({
    			method: "POST",
    			url: settings.prefix+ 'holidays',
    			data: submitData,
    		}).then(function( response ){
                $scope.datepickerFirstError = false;
    		    $scope.datepickerSecondError = false;
		        $scope.messages.alertMessage = response.data.message; 
    		    $scope.getDates();
            }, function(response) {
                if(response.data && response.data.message) {
    		        $scope.messages.errorMessage = response.data.message;    
    		    } else { $scope.messages.errorMessage = response };
            });  
            
        } else {
            if(angular.element(".datepicker-element-1").hasClass('ng-invalid-required')) { $scope.datepickerFirstError = true; } else { $scope.datepickerFirstError = false; }
            if(angular.element(".datepicker-element-2").hasClass('ng-invalid-required')) { $scope.datepickerSecondError = true; } else { $scope.datepickerSecondError = false; }
            $scope.messages.errorMessage = "Sisesta kuupäevad";
        }
    }
    
    $scope.reloadThis = function() {
        $route.reload();
    }
})
App.config(function ($routeProvider){
   var conf = {
      labels: {
         "title": "Teade"
      }
   }
   
   $routeProvider
      .when("/alert",{
         templateUrl: "alert.tpl",
			controller: "alert",
			conf: conf,
			label: conf.labels.title,
			resolve: settings.resolveAll
      });
});
App.config(function ($routeProvider){
   var conf = {
      labels: {
         "title": "Arhiiv"
      }
   }
   
   $routeProvider
      .when("/archive",{
         templateUrl: "archive.tpl",
			controller: "archive",
			conf: conf,
			label: conf.labels.title,
			resolve: settings.resolveAll
      });
});
App.config(function ($routeProvider){
   var conf = {
      labels: {
         "title": "Pealeht"
      }
   }
   
   $routeProvider
      .when("/blank",{
         templateUrl: "frontpage.tpl",
			controller: "frontpage",
			conf: conf,
			label: conf.labels.title,
			resolve: settings.resolveAll
      });
});
App.config(function ($routeProvider){
   var conf = {
      labels: {
         "title": "Eemalviibimised"
      }
   }
   
   $routeProvider
      .when("/holidays",{
         templateUrl: "holidays.tpl",
         controller: "holidays",
			conf: conf,
			label: conf.labels.title,
			resolve: settings.resolveAll
      });
});
App.config(function ($routeProvider){
   var conf = {
      labels: {
         "title": "Projektid"
      }
   }
   
   $routeProvider
      .when("/",{
         templateUrl: "import.tpl",
         controller: "import",
			conf: conf,
			label: conf.labels.title,
			resolve: settings.resolveAll
      });
});
App.config(function ($routeProvider){
   var conf = {
		loginView: true,
      labels: {
         "title": "Login"
      }
   }
   
   $routeProvider
      .when("/login",{
         templateUrl: "login.tpl",
         controller: "login",
			conf: conf,
			label: conf.labels.title,
			resolve: settings.resolveValidLocation
      });
});
App.config(function ($routeProvider){
    var conf = {
		loginView: true,
        labels: {
            "title": "Modal"
        }
    }
   
    $routeProvider
        .when("/modal",{
            templateUrl: "modal.tpl",
    		conf: conf,
    		label: conf.labels.title,
        });
    });
App.config(function ($routeProvider){
    var conf = {
		loginView: true,
      labels: {
         "title": "Uue parooli määramine"
      }
    };
   
    $routeProvider
        .when("/password_change",{
            templateUrl: "password.tpl",
            controller: "password",
            conf: conf,
            label: conf.labels.title,
        });
});
App.config(function ($routeProvider){
   var conf = {
      labels: {
         "title": "Seaded"
      }
   }
   
   $routeProvider
      .when("/profile",{
         templateUrl: "profile.tpl",
         controller: "profile",
			conf: conf,
			label: conf.labels.title,
			resolve: settings.resolveAll
      });
});
App.config(function ($routeProvider){
   var conf = {
      labels: {
         "title": "Statistika"
      }
   }
   
   $routeProvider
      .when("/statistics",{
         templateUrl: "statistics.tpl",
         controller: "statistics",
			label: conf.labels.title,
			conf: conf,
			resolve: settings.resolveAll
      });
});
App.config(function ($routeProvider){
   var conf = {
      labels: {
         "title": "Töötajad"
      }
   }
   
   $routeProvider
      .when("/users",{
         templateUrl: "users.tpl",
         controller: "users",
			conf: conf,
			label: conf.labels.title,
			resolve: settings.resolveAll
      });
});
App.config(function ($routeProvider){
   var conf = {
      labels: {
         "title": "Palgaarvestus",
         "user": "Töötaja"
      }
   }
   
   $routeProvider
      .when("/wages",{
         templateUrl: "wages.tpl",
         controller: "wages",
			label: conf.labels.title,
			resolve: settings.resolveAll
      }).when("/wages/:id",{
         templateUrl: "user-wage.tpl",
         controller: "user-wage",
			label: conf.labels.user,
			resolve: settings.resolveAll,
			resolve: settings.resolveOrderedUsers
      })
});
App.config(function ($routeProvider){
   var conf = {
      labels: {
         "title": "Riigipühad"
      }
   }
   
   $routeProvider
      .when("/workdays",{
         templateUrl: "workdays.tpl",
         controller: "workdays",
			conf: conf,
			label: conf.labels.title,
			resolve: settings.resolveAll
      });
});
