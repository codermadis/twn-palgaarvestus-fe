App.directive("toalertMessage", ['$timeout', function($timeout) {
    return {
        restrict: "E",
        replace: true,
        scope: {
            'toalert': '='
        },
        template: 
            '<div class="row fade-element-in">' +
                '<div class="col-12">' +
                    '<div class="alert alert-success" role="alert"> {{ toalert }} </div>' +
                '</div>' +
            '</div>',
        link: function (scope, elem, attrs) {
            scope.$watch('toalert', function (newValue, oldValue, scope) {
                $timeout(function () {
                    scope.$parent.messages.alertMessage = false;
                    scope.$parent.messages.generalAlertMessage = false;
                    scope.$parent.updatedWorkdays = false;
                }, 5000);       
            });
        }
    };
}]);