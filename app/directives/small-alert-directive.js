App.directive("smallAlertMessage", ['$timeout', function($timeout) {
    return {
        restrict: "E",
        replace: true,
        scope: {
            'workdays': '=',
            'project': '=',
            'myMessage': '='
        },
        template: 
            '<span class="animate-if small-alert float-right">' +
                '<i class="fa fa-check item-update"></i>' +
            '</span>',
        link: function (scope, elem, attrs) {
            if(scope.workdays) {
                scope.$watch('workdays', function (newValue, oldValue, scope) {
                    $timeout(function () {
                        scope.$parent.messages.workdaysAlert = false;
                    }, 1000);       
                });
            } else if(scope.project) {
                scope.$watch('project', function (newValue, oldValue, scope) {
                    $timeout(function () {
                        scope.$parent.messages.updatedProjectId = false;
                    }, 10000);       
                });
            } else {
                scope.$watch('myMessage', function (newValue, oldValue, scope) {
                    $timeout(function () {
                        scope.$parent.messages.myMessage = false;
                    }, 1500);       
                });
            }
        }
    };
}]);