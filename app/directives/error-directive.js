App.directive("errorMessage", ['$timeout', function($timeout){
    return {
        restrict: "E",
        replace: true,
        scope: {
            'error': '=',
            'fadeout': '='
        },
        template: 
            '<div class="row animate-if-error">' +
                '<div class="col-12">' +
                    '<div class="alert alert-danger" role="alert"> {{ error }} </div>' +
                '</div>' +
            '</div>',
        link: function (scope, elem, attrs) {
            if(scope.fadeout) {
                scope.$watch('error', function (newValue, oldValue, scope) {
                    $timeout(function () {
                        scope.$parent.messages.errorMessage = false;
                    }, 5000);       
                });
            };
        }
    };
}]);
