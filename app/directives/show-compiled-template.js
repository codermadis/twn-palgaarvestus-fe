App.directive('showCompiledTemplate',['$compile','$timeout',
    function($compile , $timeout) {
        return {
            restrict: 'E',
            template: '<div class="compiled-template" ng-show="false"></div>',
            scope: {
              data: '=',
              template: '=',
            },
            link: function(scope,elem,attrs) {
            	var updateOutput = function(tpl) {
                    var compiled = $compile(tpl)(scope);
                    $timeout(function(){
                        var theHtml = compiled[0].outerHTML;
                        scope.$parent.$parent.compiledHtml = theHtml;
                    })
                };
                scope.$watch("template",function(tpl){
            		updateOutput(tpl);
                });
                scope.$watch("data",function(){
                    updateOutput(scope.template);
                },true);
            }
        };
    }
]);