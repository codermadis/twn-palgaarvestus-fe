App.directive("dynamicCompile", function($templateRequest, $compile, $interpolate){
    return{
        link: function(scope, element){
            //$templateRequest("blank-email-html.html").then(function(html){
                var template = '<div>{{user.name}}</div>'
                var linkFn = $compile(template);
                var content = linkFn(scope);
                var testInterpolate = $interpolate(template)(scope);
                var outerContent = linkFn(scope)[0].outerHTML
                scope.$parent.compiledHtml = content;
                console.log(scope.$parent.compiledHtml);
                console.log(testInterpolate);
                console.log(outerContent);
            //});
        },
        // link: function(scope, element){
        //   $templateRequest("blank-email-html.html").then(function(html){
        //       var template = angular.element(html);
        //       scope.$parent.compiledHtml = template);
        //       $compile(template)(scope);
        //   });
        // }
    };
});