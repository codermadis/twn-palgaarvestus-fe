App.config(function ($routeProvider){
   var conf = {
      labels: {
         "title": "Arhiiv"
      }
   }
   
   $routeProvider
      .when("/archive",{
         templateUrl: "archive.tpl",
			controller: "archive",
			conf: conf,
			label: conf.labels.title,
			resolve: settings.resolveAll
      });
});