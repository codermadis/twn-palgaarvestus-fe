App.config(function ($routeProvider){
   var conf = {
      labels: {
         "title": "Teade"
      }
   }
   
   $routeProvider
      .when("/alert",{
         templateUrl: "alert.tpl",
			controller: "alert",
			conf: conf,
			label: conf.labels.title,
			resolve: settings.resolveAll
      });
});