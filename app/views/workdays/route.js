App.config(function ($routeProvider){
   var conf = {
      labels: {
         "title": "Riigipühad"
      }
   }
   
   $routeProvider
      .when("/workdays",{
         templateUrl: "workdays.tpl",
         controller: "workdays",
			conf: conf,
			label: conf.labels.title,
			resolve: settings.resolveAll
      });
});