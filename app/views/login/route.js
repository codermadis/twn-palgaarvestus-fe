App.config(function ($routeProvider){
   var conf = {
		loginView: true,
      labels: {
         "title": "Login"
      }
   }
   
   $routeProvider
      .when("/login",{
         templateUrl: "login.tpl",
         controller: "login",
			conf: conf,
			label: conf.labels.title,
			resolve: settings.resolveValidLocation
      });
});