App.config(function ($routeProvider){
   var conf = {
      labels: {
         "title": "Seaded"
      }
   }
   
   $routeProvider
      .when("/profile",{
         templateUrl: "profile.tpl",
         controller: "profile",
			conf: conf,
			label: conf.labels.title,
			resolve: settings.resolveAll
      });
});