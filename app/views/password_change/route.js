App.config(function ($routeProvider){
    var conf = {
		loginView: true,
      labels: {
         "title": "Uue parooli määramine"
      }
    };
   
    $routeProvider
        .when("/password_change",{
            templateUrl: "password.tpl",
            controller: "password",
            conf: conf,
            label: conf.labels.title,
        });
});