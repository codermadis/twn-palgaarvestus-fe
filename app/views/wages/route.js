App.config(function ($routeProvider){
   var conf = {
      labels: {
         "title": "Palgaarvestus",
         "user": "Töötaja"
      }
   }
   
   $routeProvider
      .when("/wages",{
         templateUrl: "wages.tpl",
         controller: "wages",
			label: conf.labels.title,
			resolve: settings.resolveAll
      }).when("/wages/:id",{
         templateUrl: "user-wage.tpl",
         controller: "user-wage",
			label: conf.labels.user,
			resolve: settings.resolveAll,
			resolve: settings.resolveOrderedUsers
      })
});