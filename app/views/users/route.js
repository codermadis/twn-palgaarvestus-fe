App.config(function ($routeProvider){
   var conf = {
      labels: {
         "title": "Töötajad"
      }
   }
   
   $routeProvider
      .when("/users",{
         templateUrl: "users.tpl",
         controller: "users",
			conf: conf,
			label: conf.labels.title,
			resolve: settings.resolveAll
      });
});