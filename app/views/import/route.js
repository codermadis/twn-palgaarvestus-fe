App.config(function ($routeProvider){
   var conf = {
      labels: {
         "title": "Projektid"
      }
   }
   
   $routeProvider
      .when("/",{
         templateUrl: "import.tpl",
         controller: "import",
			conf: conf,
			label: conf.labels.title,
			resolve: settings.resolveAll
      });
});