App.config(function ($routeProvider){
    var conf = {
		loginView: true,
        labels: {
            "title": "Modal"
        }
    }
   
    $routeProvider
        .when("/modal",{
            templateUrl: "modal.tpl",
    		conf: conf,
    		label: conf.labels.title,
        });
    });