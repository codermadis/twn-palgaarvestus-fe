App.config(function ($routeProvider){
   var conf = {
      labels: {
         "title": "Pealeht"
      }
   }
   
   $routeProvider
      .when("/blank",{
         templateUrl: "frontpage.tpl",
			controller: "frontpage",
			conf: conf,
			label: conf.labels.title,
			resolve: settings.resolveAll
      });
});