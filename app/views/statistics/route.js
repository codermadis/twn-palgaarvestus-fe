App.config(function ($routeProvider){
   var conf = {
      labels: {
         "title": "Statistika"
      }
   }
   
   $routeProvider
      .when("/statistics",{
         templateUrl: "statistics.tpl",
         controller: "statistics",
			label: conf.labels.title,
			conf: conf,
			resolve: settings.resolveAll
      });
});