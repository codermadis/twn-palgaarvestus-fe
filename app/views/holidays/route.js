App.config(function ($routeProvider){
   var conf = {
      labels: {
         "title": "Eemalviibimised"
      }
   }
   
   $routeProvider
      .when("/holidays",{
         templateUrl: "holidays.tpl",
         controller: "holidays",
			conf: conf,
			label: conf.labels.title,
			resolve: settings.resolveAll
      });
});