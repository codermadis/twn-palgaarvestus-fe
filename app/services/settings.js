App.factory('$settings', function($http, $q, $location, $rootScope) {
   var content = true; //set false
    
	return {
		getLogin: function() {
			if(!sessionStorage.loggedIn || !sessionStorage.loggedIn) {
				$location.path('/login');
			}
		},
		get: function(param){
			var deferred = $q.defer();
			var redirect;
			
			if( !content ){
				
				var promise = $http({
					method: "GET",
					url: settings.prefix+"settings"
				});

    			promise.then(function(response){
    				
					if( response.data.user ){
    			    	
    			    	content = response.data;
    			    	$rootScope.settings = content;
    			    	
						deferred.resolve(response);
					}else{
						$location.path("/login");
					}
					
    			}, function(){
		
    			});
    			
    			return deferred.promise;
			}
			
		},
		
		delete: function(){
			content = false;
			$rootScope.settings = false;
		}
	};
});