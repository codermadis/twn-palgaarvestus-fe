App.factory('$validLocation', function($http, $q, $location, $rootScope) {
   var content = false; //set false
    
	return {
		get: function(param){
			var deferred = $q.defer();
			var redirect;
			
			if( !content ){
				
				var promise = $http({
					method: "GET",
					url: settings.prefix+"ip_check"
				});
    			promise.then(function(response){
    			    $rootScope.locationValiditySet = true;
					$rootScope.locationAllowed = true;
    			}, function(){
    			    $rootScope.locationValiditySet = true;
		            $rootScope.locationAllowed = false;
    			})
    			return deferred.promise;
			}
			
		},
	};
});