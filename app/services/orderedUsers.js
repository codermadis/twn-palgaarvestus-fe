App.factory('orderedUsers', function($http, $q, $location, $rootScope, $filter, $routeParams) {
    var content = false;
    
	return {
		get: function(){
			var deferred = $q.defer();
			var redirect;
			
			if( !content ){
				
				var promise = $http({
					method: "GET",
					url: settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users'
				});

    			promise.then(function(response){
    				
    				var outSourcerRole = "OUTSOURCER";
    				
    				$rootScope.workerOrderedIds = [];
    				$rootScope.outSourcerOrderedIds = [];
					var orderedUsers = $filter('orderBy')(response.data, 'first_name');
					var workerOrderedUsers = $filter('filter')(orderedUsers, '!' + outSourcerRole, 'role');
					var outSourcerOrderedUsers = $filter('filter')(orderedUsers, outSourcerRole, 'role');
					
					for(var item in workerOrderedUsers) {
					    $rootScope.workerOrderedIds.push(workerOrderedUsers[item].id);
					};
					var currentIdPosition = $rootScope.workerOrderedIds.indexOf(parseInt($routeParams.id));
					var nextIdPosition = currentIdPosition+1;
					var previousIdPosition = currentIdPosition-1;
					$rootScope.workerNextId = $rootScope.workerOrderedIds[nextIdPosition];
					$rootScope.workerPrevId = $rootScope.workerOrderedIds[previousIdPosition];
					
					for(var elem in outSourcerOrderedUsers) {
					    $rootScope.outSourcerOrderedIds.push(outSourcerOrderedUsers[elem].id);
					};
					var curPos = $rootScope.outSourcerOrderedIds.indexOf(parseInt($routeParams.id));
					var nextPos = curPos+1;
					var prevPos = curPos-1;
					$rootScope.outSourcerNextId = $rootScope.outSourcerOrderedIds[nextPos];
					$rootScope.outSourcerPrevId = $rootScope.outSourcerOrderedIds[prevPos];
    			}, function(){
		
    			});
    			
    			return deferred.promise;
			}
			
		},
	};
});