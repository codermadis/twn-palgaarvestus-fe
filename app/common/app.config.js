App.config(function ($routeProvider, $locationProvider, $httpProvider){
	
	if(location.hostname.indexOf("palgaarvestus.twn.ee") != -1 ) {
      settings.url = "https://palgaarvestus.twn.ee";
		settings.prefix = "https://palgaarvestus.twn.ee/api/v1/";
	};
	
	$locationProvider.html5Mode({
  		enabled: true,
  		requireBase: false
	});
	$routeProvider.otherwise('/');
   $httpProvider.defaults.withCredentials = true;
	
   $httpProvider.interceptors.push(function($q, $location, $rootScope, $interval) {
      return {
         'responseError': function(response) {
            if(response.status === 404) {
               window.scrollTo(0, 0);	
               return $q.reject(response);
            } else if(response.status === 500) {
               window.scrollTo(0, 0);	
               return $q.reject(response);
            } else if(response.status === 412) {
               window.scrollTo(0, 0);	
               return $q.reject(response);
            } else if(response.status === 401) {
               $location.path('/login');
               return $q.reject(response);
            } else if(response.status === 403) {
               $location.path('/login');
               $rootScope.locationAllowed = false;
               return $q.reject(response);
            } else if(response.status === 503) {
               $rootScope.logOut();
               $location.path('/login');
               return $q.reject(response);
            } else if(response.status === 304) {
               return $q.defer(response);
            } 
         }
      };       
   });
   
});
App.config(['$cryptoProvider', function($cryptoProvider){
    $cryptoProvider.setCryptographyKey('myKey');
}]);