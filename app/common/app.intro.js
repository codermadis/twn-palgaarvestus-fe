var App = angular.module("App", ['ngRoute', 'ngAnimate', 'ngSanitize', 'ui.bootstrap', 'ng-breadcrumbs', 'angularModalService', 'googlechart', 'ngProgress', 'ngCookies', 'mdo-angular-cryptography', 'ngFileUpload', 'ui.tinymce']);

var settings = {
	template: "/dist/templates.html",
	prefix: 'https://palgaarvestus-be-2-madispold.c9users.io/api/v1/',
	resolveAll: {
		authenticate: function($settings) {
			$settings.getLogin()
		}
	},
	resolveValidLocation: {
		validLocation: function($validLocation) {
			$validLocation.get()
		}
	},
	resolveOrderedUsers: {
		orderedList: function(orderedUsers) {
			orderedUsers.get()
		}
	}
};