App.controller('mainCtrl', function ($scope, $http, $rootScope, $sanitize, $route, $routeParams, $location, breadcrumbs, $filter, $anchorScroll) {   
	
    $scope.breadcrumbs = breadcrumbs;
    $rootScope.changedProjects = [];
	
	$rootScope.hideNavbar = false;
	$rootScope.$on("$routeChangeSuccess", function (event, currentRoute, previousRoute) {

		window.scrollTo(0, 0);		
   		$rootScope.activeRole = sessionStorage.getItem('role');
   		$scope.loginPage = false;
		$rootScope.hideNavbar = false;
		
		$rootScope.currentLocation = $location.path();
		
		if($rootScope.currentLocation == '/login' || $rootScope.currentLocation.indexOf('password') != -1) { $scope.loginPage = true; }
	});
	
	$rootScope.logOut = function () {
		$http({
			method: "PUT",
			url: settings.prefix+"logout",
		}).then(function( response ){
			$rootScope.login = false; 
			sessionStorage.clear();
			$location.path('/login');
		}, function(response){
			console.log(response);
		});
	}
	
	$rootScope.monthLabels = ['Jaanuar', 'Veebruar','Märts', 'Aprill', 'Mai', 'Juuni', 'Juuli', 'August', 'September', 'Oktoober', 'November', 'Detsember']
    //month setter
    $rootScope.monthSetter = function () {
	    var currentDate = new Date();
	    $rootScope.currentMonth = $filter('date')(currentDate,'yyyy-MM');
	    
	    var previousMonth = currentDate.getMonth();
	    
	    var previousBeforeMonth = previousMonth-1;
	    $rootScope.currentMonthLabel = $rootScope.monthLabels[previousMonth];
	    $rootScope.previousMonthLabel = $rootScope.monthLabels[previousMonth-1] || $rootScope.monthLabels[$rootScope.monthLabels.length-1]; // start from the end again
	    $rootScope.previousBeforeMonthLabel = $rootScope.monthLabels[previousBeforeMonth-1] || $rootScope.monthLabels[$rootScope.monthLabels.length-1];; // start from the end again
	    
	    var thisYear = currentDate.getFullYear();
	    var previousBeforeYear = thisYear;
	    
	    if(previousMonth == 0) { previousMonth = 12, thisYear = thisYear - 1 }
	    if(previousBeforeMonth == 0) { previousBeforeMonth = 12; var previousBeforeYear = thisYear - 1}
	    if(previousBeforeMonth == -1) { previousBeforeMonth = 11; var previousBeforeYear = thisYear }
	    
	    $rootScope.prevMonthNum = previousMonth;
	    $rootScope.prevBeforeMonthNum = previousBeforeMonth;
	    $rootScope.thisYearNum = thisYear;
	    
	    thisYear = thisYear.toString(); previousMonth = previousMonth.toString();
	    $rootScope.thisYear = thisYear.toString();
	    if(previousMonth > 9) {
	        $rootScope.finalPreviousMonth = (thisYear + '-' + previousMonth);
	    } else {
	        $rootScope.finalPreviousMonth = (thisYear + '-0' + previousMonth);
	    }
	    if(previousBeforeMonth > 9) {
	        $rootScope.finalPreviousBeforeMonth = (previousBeforeYear + '-' + previousBeforeMonth);
	    } else {
	        $rootScope.finalPreviousBeforeMonth = (previousBeforeYear + '-0' + previousBeforeMonth);
	    }
    };
    $rootScope.monthSetter();
	
});