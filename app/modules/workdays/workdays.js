App.controller('workdays', function($scope, $http, $route, $filter){
    
    $scope.model = {};
    $scope.day = ['Ei', 'Jah'];
    $scope.datepickerFirstError = false;
    $scope.datepickerSecondError = false;
    $scope.messages = { alertMessage: false, myMessage: false, errorMessage: false };
    $scope.popup_open1 = false;
    $scope.popup_open2 = false;
    $scope.open_popup1 = function() { $scope.popup_open1 = !$scope.popup_open1 };
    $scope.open_popup2 = function() { $scope.popup_open2 = !$scope.popup_open2 };
    
	$scope.getDates = function() {
	    $scope.model = {};
	    $scope.dates = false;
        $http.get(settings.prefix+'holidays').then(function(response) {
            $scope.dates = response.data;
            $scope.model['short_day'] = 0;
        }, function(response) {
            $scope.messages.errorMessage = response.data.message;
        });
    };
    $scope.compareDates = function(start, end) {
        var startAsObj = new Date(start);
        var endAsObj = new Date(end);
        if(startAsObj.getTime() === endAsObj.getTime()) { return true; }
        return false;
    };
    $scope.setEndDate = function() {
        if($scope.model.end_date == null) { $scope.model.end_date = new Date($scope.model.start_date) }
    };
    $scope.getDates();
    $scope.removeHoliday = function(id) {
        $http.delete(settings.prefix + 'holidays/' + id).then(function(response){
            $scope.messages.alertMessage = response.data.message;
            $scope.getDates();
        }, function(response) {
            $scope.messages.errorMessage = response.data.message;
        });
    }
    $scope.addHoliday = function(form) {
        if(form.$valid) {
            var submitData = {};
            submitData = angular.copy($scope.model);
            submitData.start_date = $filter('date')(submitData.start_date, 'yyyy-MM-dd');
            submitData.end_date = $filter('date')(submitData.end_date, 'yyyy-MM-dd');

            console.log(submitData);
            $http({
    			method: "POST",
    			url: settings.prefix+ 'holidays',
    			data: submitData,
    		}).then(function( response ){
                $scope.datepickerFirstError = false;
    		    $scope.datepickerSecondError = false;
		        $scope.messages.alertMessage = response.data.message; 
    		    $scope.getDates();
            }, function(response) {
                if(response.data && response.data.message) {
    		        $scope.messages.errorMessage = response.data.message;    
    		    } else { $scope.messages.errorMessage = response };
            });  
            
        } else {
            if(angular.element(".datepicker-element-1").hasClass('ng-invalid-required')) { $scope.datepickerFirstError = true; } else { $scope.datepickerFirstError = false; }
            if(angular.element(".datepicker-element-2").hasClass('ng-invalid-required')) { $scope.datepickerSecondError = true; } else { $scope.datepickerSecondError = false; }
            $scope.messages.errorMessage = "Sisesta kuupäevad";
        }
    }
    
    $scope.reloadThis = function() {
        $route.reload();
    }
})