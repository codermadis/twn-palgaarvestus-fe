<script type="text/ng-template" id="workdays.tpl">
    <div class="container animate-if">
        <div class="row">
            <main class="col-sm-12 pt-3 mt-2">
				<ng-include src="'breadcrumbs.tpl'"></ng-include>
				<error-message class="mt-4" ng-if="messages.errorMessage" error="messages.errorMessage" fadeout=true></error-message>
				<toalert-message class="mt-4" ng-if="messages.alertMessage" toalert="messages.alertMessage"></toalert-message>
				<div class="row mt-4 fade-element-in" ng-if="users">
                    <div class="col-12">
                        <div class="bordered-elem p-3">
                            <h5 class="form-control-label mb-3">Lisa uue töötaja andmed ja palgamudel</h5>
                            <div class="mt-3">
                                <a href="" ng-click="openUserAdd()" class="btn btn-primary">Lisa töötaja</a>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="row mt-4">
                   	<div class="col-lg-12 col-md-12">
                   	    <div class="bordered-elem p-3">
                   	        <h5 class="form-control-label mb-3">Lisa riigipühad</h5>
                   	        <form name="workdayForm" id="workdayForm">
                                <div class="row">
                                    <div class="form-group col-lg-3 col-md-6 col-12 mb-0">
                                        <label for="year">Vali alguskuupäev</label>
                                        <p class="input-group date-group" ng-init="people=1">
                                            <input type="text" ng-class="{ 'border-error' : datepickerFirstError }" ng-change="setEndDate()" class="form-control datepicker-element-1" readonly="true" ng-model="model.start_date" uib-datepicker-popup="dd-MM-yyyy" is-open="popup_open1" datepicker-options="dateOptions" close-text="Sulge" current-text="Täna" clear-text="Kustuta" required />
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default" ng-click="open_popup1()"><i class="fa fa-calendar"></i></button>
                                            </span>
                                        </p>
                                    </div>
                                    <div class="form-group col-lg-3 col-md-6 col-12 mb-0">
                                        <label for="year">Vali lõppkuupäev</label>
                                        <p class="input-group date-group">
                                            <input type="text" ng-class="{ 'border-error' : datepickerSecondError }" class="form-control datepicker-element-2" readonly="true" ng-model="model.end_date" uib-datepicker-popup="dd-MM-yyyy" is-open="popup_open2" datepicker-options="dateOptions" close-text="Sulge" current-text="Täna" clear-text="Kustuta" required />
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default" ng-click="open_popup2()"><i class="fa fa-calendar"></i></button>
                                            </span>
                                        </p>
                                    </div>
                                    <div class="form-group col-lg-3 col-md-6 col-12 mb-0">
                                        <label>Tüüp</label>
                                        <p>
                                            <input class="user-inline-radio" type="radio" name="short_day" value="0" ng-model="model.short_day">
        					                <label for="short_day" class="workdays-inline-label">Vaba</label>
        					                <input class="user-inline-radio" type="radio" name="short_day" value="1" ng-model="model.short_day">
        					                <label for="short_day" class="workdays-inline-label">Lühendatud</label>
        					            </p>
                                    </div>
                                    <div class="form-group col-lg-3 col-md-6 col-12 centered-flex mb-0 mt-3">
                                        <button type="submit" href="" class="btn btn-primary" ng-click="addHoliday(workdayForm)">
                                            <span>Lisa püha</span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="row fade-element-in mt-4 mb-4" ng-if="dates">
                   	<div class="col-lg-12 col-md-12">
                   	    <h5>Riigipühad ja lühendatud tööpäevad</h5>
                   	    <div class="row table-holder">
                            <div class="col-md-12 col-sm-12">
        						<table class="table table-bordered table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th style="width:33%">Kuupäevad</th>
                                            <th style="width:33%">Tüüp</th>
                                            <th style="width:35%"></th>
                                        </tr>
                                    </thead>
                                    <tbody class="fade-element-in">
                                        <tr ng-repeat="date in dates">
                                            <td>
                                                <span ng-if="compareDates(date.start_date, date.end_date)">{{date.end_date | date: 'd MMM, y'}}</span>
                                                <span ng-if="!compareDates(date.start_date, date.end_date)">{{date.start_date | date: 'd MMM'}} - {{date.end_date | date: 'd MMM, y'}}</span>
                                            </td>
                                            <td>
                                                <span>{{date.short_day ? "Lühendatud" : "Vaba"}}</span>
                                            </td>
                                            <td>
                                                <span class="ip-action float-right"><a href="" ng-click="removeHoliday(date.id)"><i class="fa fa-close address-close user-alert-text"></i></a></span>
                                            </td>
                                        </tr>
                                        <tr ng-if="!dates.length">
                                            <td colspan=100>Kirjed puuduvad</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div ng-if="false" class="spinner mb-4 mt-4">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </main><!--col12-->
        </div><!--row-->
    </div><!--/container-->
</script>