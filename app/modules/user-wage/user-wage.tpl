<script type="text/ng-template" id="user-wage.tpl">
    <div class="container user-wage">
        <div class="row">
            <main class="col-sm-12 pt-3 mt-2">
                <ng-include src="'breadcrumbs.tpl'"></ng-include>
                <error-message class="mt-4" ng-if="messages.errorMessage" fadeout=true error="messages.errorMessage"></error-message>
                <toalert-message class="mt-4" ng-if="messages.alertMessage" toalert="messages.alertMessage"></toalert-message>
                <div class="row animate-if mt-4" ng-if="user">
                    <div class="col-md-12">
                        <h3 class="header-text">PALGAARVESTUS</h3>
                        <div class="header-label mb-4">{{previousMonthLabel}} {{thisYear}}</div>
                        
                        <section class="wage-section wage-bg">
                            <div class="wage-section__header">
                                <h4>{{user.first_name}} {{user.last_name}}</h4>
                            </div>
                            <div class="wage-section__item">
                                <div><span>E-mail:</span> <span>{{user.email}}</span></div>
                            </div>
                        </section>
                        
                        <section class="wage-section">
                            <div class="wage-section__header">
                                <h5>Osalemine projektides</h5>
                            </div>
                            <div class="wage-section__item">
                                <div class="row" ng-if="user.role != 'OUTSOURCER'">
                					<div class="col-md-3 xs-none bold wage-row">Projekt</div>
                					<div class="col-md-3 xs-none bold wage-row">Töötunnid</div>
                					<div class="col-md-3 xs-none bold wage-row">Palgaarvestuse €/h</div>
                					<div class="col-md-3 xs-none bold wage-row">Palgaarvestuse sissetulek</div>
                				</div> 
                				<div class="row" ng-if="user.role == 'OUTSOURCER'">
                					<div class="col-md-3 xs-none bold wage-row">Projekt</div>
                					<div class="col-md-3 xs-none bold wage-row">Töötunnid</div>
                					<div class="col-md-3 xs-none bold wage-row">Statistika €/h</div>
                					<div class="col-md-3 xs-none bold wage-row">Statistika sissetulek</div>
                				</div> 
                				<hr>
                				<div class="row wage-row" ng-repeat="project in user.projects" ng-if="user.role != 'OUTSOURCER'">
                					<div class="col-6 xs-show bold wage-row">Projekt</div>
                					<div class="col-md-3 col-6 wage-row">
                					    <span>{{project.name}}</span>
                					</div>
                					<div class="col-6 xs-show bold wage-row">Töötunnid</div>
                					<div class="col-md-3 col-6 wage-row">
                					    <span ng-if="user.confirmed == 1">{{project.hours}}</span>
                						<input valid-number class="form-control form-control-sm d-inline auto-width" ng-if="user.confirmed != 1" ng-model-options='{ debounce: 500 }' ng-change="setProjectParameters(project.id, project.hours, project.rate_salary, project.rate_statistic)" type="text" name="hours" ng-model="project.hours" smart-float required >
                					</div>
                					<div class="col-6 xs-show bold wage-row">Palgaarvestuse €/h</div>
                					<div class="col-md-3 col-6 wage-row">
                					    <span ng-if="user.confirmed == 1">{{project.rate_salary}}</span>
                					    <input valid-number class="form-control form-control-sm d-inline auto-width" ng-if="user.confirmed != 1" ng-model-options='{ debounce: 500 }' ng-change="setProjectParameters(project.id, project.hours, project.rate_salary, project.rate_statistic)" type="text" name="rate_salary" ng-model="project.rate_salary" smart-float required >
                					</div>
                					<div class="col-6 xs-show bold wage-row">Palgaarvestuse sissetulek</div>
                					<div class="col-md-3 col-6 wage-row">
                					    <span>{{project.calculated_income | number:0 }} €</span>
                					    <a href="" ng-click="deleteProject(project.id)"><span ng-if="project.deletable" class="fa fa-close project-close user-alert-text float-right"></span></a>
                					</div>
                				</div>
                				<div class="row wage-row" ng-repeat="project in user.projects" ng-if="user.role == 'OUTSOURCER'">
                					<div class="col-6 xs-show bold wage-row">Projekt</div>
                					<div class="col-md-3 col-6 wage-row">
                					    <span>{{project.name}}</span>
                					</div>
                					<div class="col-6 xs-show bold wage-row">Töötunnid</div>
                					<div class="col-md-3 col-6 wage-row">
                					    <span ng-if="user.confirmed == 1">{{project.hours}}</span>
                						<input valid-number class="form-control form-control-sm d-inline auto-width" ng-if="user.confirmed != 1" ng-model-options='{ debounce: 500 }' ng-change="setProjectParameters(project.id, project.hours, project.rate_salary, project.rate_statistic)" type="text" name="hours" ng-model="project.hours" smart-float required >
                					</div>
                					<div class="col-6 xs-show bold wage-row">Statistika €/h</div>
                					<div class="col-md-3 col-6 wage-row">
                					    <span ng-if="user.confirmed == 1">{{project.rate_statistic}}</span>
                					    <input valid-number class="form-control form-control-sm d-inline auto-width" ng-if="user.confirmed != 1" ng-model-options='{ debounce: 500 }' ng-change="setProjectParameters(project.id, project.hours, project.rate_salary, project.rate_statistic)" type="text" name="rate_statistic" ng-model="project.rate_statistic" smart-float required >
                					</div>
                					<div class="col-6 xs-show bold wage-row">Statistika sissetulek</div>
                					<div class="col-md-3 col-6 wage-row">
                					    <span>{{project.calculated_statistic_income | number:0 }} €</span>
                					    <a href="" ng-click="deleteProject(project.id)"><span ng-if="project.deletable" class="fa fa-close project-close user-alert-text float-right"></span></a>
                					</div>
                				</div>
                				<div ng-if="user.confirmed == 0" class="row wage-row">
                				    <div class="col-md-12">
                    					<a href="" ng-click="addCustomProject()">
                    					    <i class="fa fa-plus"></i>
                    					    <span>Lisa projekt</span>
                    					</a>
                    				</div>
                				</div>
                				<hr>
                				<div class="row" ng-if="user.role != 'OUTSOURCER'">
                					<div class="col-md-2 col-6 pull-right bold">Töötunnid kokku</div>
                					<div class="col-md-2 col-6">{{total_minutes_floored}}<span ng-if="total_minutes_remainder">:{{total_minutes_remainder}}</span></div>
                					<div class="col-md-2 col-6 bold">
                					    <span class="xs-none">Palgaarvestuse sissetulek kokku</span>
                					    <span class="xs-show">Palgaarvestuse €</span>
                					</div>
                					<div class="col-md-2 col-6">{{total_calculated_income | number }} €</div>
                					<div class="col-md-2 col-6 bold">
                					    <span class="xs-none">Statistika sissetulek kokku</span>
                					    <span class="xs-show">Statistika €</span>
                					</div>
                					<div class="col-md-2 col-6">{{total_calculated_statistic_income | number }} €</div>
                				</div>
                				<div class="row" ng-if="user.role == 'OUTSOURCER'">
                					<div class="col-md-3 col-6 pull-right bold">Töötunnid kokku</div>
                					<div class="col-md-3 col-6">{{total_minutes_floored}}<span ng-if="total_minutes_remainder">:{{total_minutes_remainder}}</span></div>
                					<div class="col-md-3 col-6 bold">
                					    <span class="xs-none">Statistika sissetulek kokku</span>
                					    <span class="xs-show">Statistika €</span>
                					</div>
                					<div class="col-md-3 col-6">{{total_calculated_statistic_income | number }} €</div>
                				</div>
                			</div>
                        </section>
                        
                        <section class="wage-section" ng-if="user.role == 'OUTSOURCER' && user.confirmed !== 1">
                            <div class="wage-section__header">
                                <h5>Lisa kulud</h5>
                            </div>
                            <div class="wage-section__item">
                                <div class="row wage-row">
                					<div class="col-md-6 col-6">
                					    <div class="row wage-row">
                    						<span class="xs-none col-md-6">Lisa töötaja palgaarvestuse kuu kulud: </span>
                    						<span class="xs-show col-md-6"></span>
                    						<span class="col-md-6">
                    						    <input valid-number smart-float type="text" ng-model-options='{ debounce: 500 }' class="form-control form-control-sm d-inline auto-width" name="salary" ng-model="user.salary_calculated.rate_month" required >
                    					    </span>
                    					</div>
                					</div>
                					<div class="col-md-6 col-6" ng-if="false">
	                                    <div class="wage-section__item pull-right">
                                            <button ng-if="user.confirmed == 1" type="submit" ng-click="promptWage()" class="btn btn-primary">Lähtesta kulud</button>
                                        </div>
                					</div>
                				</div>
                            </div>
                        </section>
                    
                        <section class="wage-section animate-if" ng-if="user.role == 'OUTSOURCER'  && user.confirmed === 1">
                            <div class="wage-section__header">
                                <h5>Selle kuu kulu</h5>
                            </div>
                            <div class="wage-section__item">
                                <div class="row">
                					<div class="col-md-12 col-sm-12">
                						<div class="bold wage-row xs-none">
                						    <div class="row">
                						        <div class="col-md-3">
						                            <span>Kulu</span>
                						        </div>
                						        <div class="col-md-3">
						                            <span>{{user.salary}}</span>
                						        </div>
                						    </div>
                						</div>
                					</div>
            					</div>
        					</div>
                        </section>
                        
                        <section class="wage-section wage-bg" ng-if="user.role == 'OUTSOURCER'">
                            <div class="wage-section__item wage-section__confirm">
                                <div ng-if="user.confirmed != 1" class="text-warning float-left">Kulud kinnitamata</div>
                                <div ng-if="user.confirmed == 1" class="item-update float-left">Kulud kinnitatud</div>
                                
                                <button ng-if="user.confirmed != 1" ng-class="{ 'btn-warning' : user.confirmed != 1, 'btn-primary' : user.confirmed == 1 }" type="submit" ng-click="confirmSalary()" class="btn float-right">
                                    <span ng-if="user.confirmed != 1">Kinnita</span>
                                    <span ng-if="user.confirmed == 1">Kulud kinnitatud</span>
                                </button>
                                <button ng-if="user.confirmed == 1" type="submit" ng-click="promptWage()" class="btn btn-primary float-right">Lähtesta kulud</button>
                            </div>
                        </section>
                        
                        <section class="wage-section" ng-if="user.role != 'OUTSOURCER' && user.confirmed != 1">
                            <div class="wage-section__header">
                                <h5>Tööpäevade arv</h5>
                            </div>
                            <div class="wage-section__item">
                                <div class="row wage-row">
                					<div class="col-md-6 col-6">
                					    <div class="wage-row">
                    						<span>Tööpäevi kuus kokku: </span>
                    						<!-- ng-class="{ 'border-error' : overallWorkDaysFalse, 'border-regular' : !overallWorkDaysFalse }" valid-number smart-float ng-change="setOverallWorkDays()" class="form-control form-control-sm d-inline auto-width" ng-model-options='{ debounce: 500 }' type="text" name="workdays" -->
                    						<span class="d-inline">{{workdays | number }}</span>
                    					</div>
                					</div>
                					<div class="col-md-6 col-6">
                					    <div class="wage-row">
                    						<span>Töötatud tööpäevi kuus: </span>
                    						<span class="d-inline">{{worked_days | number }}</span>
                    					</div>
                					</div>
                				</div>
                            </div>
                        </section>
                        
                        <section class="wage-section" ng-if="user.role != 'OUTSOURCER' && !(user.confirmed == 1 && user.one_time_model == 1)">
                            <div class="wage-section__header">
                                <h5>Kehtiv palgamudel</h5>
                            </div>
                            <div class="wage-section__item">
                                <div class="row">
                					<div class="col-md-6 col-6">
                						<div class="bold wage-row">
                						    <span class="xs-show">Formaat: </span>
                						    <span class="xs-none">Palgamudeli formaat: </span>
                						 </div>
                						<div class="wage-row salary-element" ng-if="user.salary_default.salary_model == 1 || user.salary_default.salary_model == 3">Põhipalk</div>
                						<div class="wage-row salary-element" ng-if="user.salary_default.salary_model == 2">Tunnihind</div>
                						<div class="wage-row salary-element" ng-if="user.salary_default.salary_model == 3">Lisatasupiir</div>
                						<div class="wage-row salary-element" ng-if="user.salary_default.salary_model == 3">Koefitsient</div>
                						<div class="wage-row mt-4" ng-if="user.confirmed != 1">Kas arvutada kehtiva palgamudeli järgi?</div>
                					</div>
                					<div class="col-md-6 col-6">
                						<div class="bold wage-row">{{salaryVal[user.salary_default.salary_model-1]}}</div>
                						<div class="bold wage-row" ng-if="!user.salary_default.salary_model">Puudub</div>
                						<div class="wage-row salary-element" ng-if="user.salary_default.salary_model == 1 || user.salary_default.salary_model == 3">{{user.salary_default.rate_month | number }} €</div>
                						<div class="wage-row salary-element" ng-if="user.salary_default.salary_model == 2">{{user.salary_default.rate_hour | number }} €</div>
                						<div class="wage-row salary-element" ng-if="user.salary_default.salary_model == 3">{{user.salary_default.bonus_threshold | number }} €</div>
                						<div class="wage-row salary-element" ng-if="user.salary_default.salary_model == 3">{{user.salary_default.coefficient | number }}</div>
                						<div class="wage-row mt-4" ng-if="user.confirmed != 1">
                    						<input name="one-time-model" ng-model="user.one_time_model" ng-click="calculateSalary(0)" type="radio" ng-value="0" />
                    						<label class="wage-label" for="yes">Jah</label>
                    						
                    						<input name="one-time-model" ng-model="user.one_time_model" type="radio" ng-value="1" />
                    						<label for="no">Ei</label>
                						</div>
                					</div>
                				</div>
                            </div>
                        </section>
                        
                        <section class="wage-section animate-if" ng-if="user.one_time_model == 1 && user.role != 'OUTSOURCER'">
                            <div class="wage-section__header">
                                <h5>Ühekordne palgamudeli muutmine</h5>
                            </div>
                            <div class="wage-section__item">
                                <div class="row">
                                    <div class="form-group col-md-4">
                				        <label class="primary-label" for="last_name">Palgamudeli formaat</label>
                				        <div>
                    					    <input ng-disabled="user.confirmed == 1 && user.one_time_model == 1" class="form-control user-inline-radio" type="radio" name="main" value=1 ng-model="user.salary_calculated.salary_model">
                    					    <label for="main" class="user-inline-label">põhipalk</label>
                					    </div>
                					    <div>
                    					    <input ng-disabled="user.confirmed == 1 && user.one_time_model == 1" class="form-control user-inline-radio" type="radio" name="hour" value=2 ng-model="user.salary_calculated.salary_model">
                                            <label for="hour" class="user-inline-label">tunnipõhine</label>    	
                                        </div>
                                        <div>
                    					    <input ng-disabled="user.confirmed == 1 && user.one_time_model == 1" class="form-control user-inline-radio" type="radio" name="calc" value=3 ng-model="user.salary_calculated.salary_model">
                    					    <label for="calc" class="user-inline-label">valemipõhine</label>
                					    </div>
                					</div>
                					<div class="form-group col-md-8">
                					    <div class="heavy-spacing"></div>
                					    <div class="row" ng-if="user.salary_calculated.salary_model == 1 || user.salary_calculated.salary_model == 3">
                					        <div class="col-md-2">
                        				        <label for="last_name regular-weight">Põhipalk</label>
                        				    </div>
                        				    <div class="col-md-4">
                        					    <input ng-if="user.confirmed != 1" valid-number smart-float class="form-control form-control-sm" type="text" name="rate_month" id="rate_month" ng-model-options='{ debounce: 500 }' ng-model="user.salary_calculated.rate_month">
                        					    <span ng-if="user.confirmed == 1">{{user.salary_calculated.rate_month}}</span>
                        					</div>
                        				</div>
                        				<div class="row" ng-if="user.salary_calculated.salary_model == 2">
                					        <div class="col-md-2">
                					            <label for="last_name regular-weight">Tunnihind</label>
                        				    </div>
                        				    <div class="col-md-4">
                        				        <input ng-if="user.confirmed != 1" valid-number smart-float class="form-control form-control-sm" type="text" name="rate_hour" id="rate_hour" ng-model-options='{ debounce: 500 }' ng-model="user.salary_calculated.rate_hour">
                        				        <span ng-if="user.confirmed == 1">{{user.salary_calculated.rate_hour}}</span>
                        					</div>
                        				</div>
                        				<div class="row" ng-if="user.salary_calculated.salary_model == 3">
                					        <div class="col-md-2">
                					            <label for="last_name regular-weight">Lisatasupiir</label>
                        				    </div>
                        				    <div class="col-md-4">
                        				        <input ng-if="user.confirmed != 1" valid-number smart-float class="form-control form-control-sm" type="text" name="bonus_threshold" id="bonus_threshold" ng-model-options='{ debounce: 500 }' ng-model="user.salary_calculated.bonus_threshold">
                        				        <span ng-if="user.confirmed == 1">{{user.salary_calculated.bonus_threshold}}</span>
                        					</div>
                        				</div>
                        				<div class="row" ng-if="user.salary_calculated.salary_model == 3">
                					        <div class="col-md-2">
                					            <label for="last_name regular-weight">Koefitsient</label>
                        				    </div>
                        				    <div class="col-md-4">
                        				        <input ng-if="user.confirmed != 1" valid-number smart-float class="form-control form-control-sm" type="text" name="coefficient" id="coefficient" ng-model-options='{ debounce: 500 }' ng-model="user.salary_calculated.coefficient">
                        				        <span ng-if="user.confirmed == 1">{{user.salary_calculated.coefficient}}</span>
                        					</div>
                        				</div>
                					</div>
                				</div>
                            </div>
                        </section>
                        
                        <section class="wage-section wage-bg" id="salary-section" ng-if="(user.one_time_model == 1 || user.confirmed == 1) && user.role != 'OUTSOURCER'">
                            <div class="wage-section__item">
                                <button ng-if="user.confirmed != 1" type="submit" ng-click="calculateSalary(user.one_time_model)" class="btn btn-primary" style="width:auto">Arvuta</button>
                                
                            </div>
                        </section>
                        
                        <section class="wage-section animate-if" ng-if="isNumber(user.salary) && user.role != 'OUTSOURCER'">
                            <div class="wage-section__header">
                                <h5>Selle kuu brutopalk</h5>
                            </div>
                            <div class="wage-section__item">
                                <div class="row">
                					<div class="col-md-10 col-sm-10" ng-if="user.one_time_model == 0">
                						<div class="bold wage-row xs-none" ng-if="last_calculation_salary_model == 1">
                						    <div class="row">
                						        <div class="col-md-4">
						                            <span>Põhipalk</span>
                						        </div>
                						        <div class="col-md-4">
						                            <span>Tööpäevi kuus</span>
                						        </div>
                						        <div class="col-md-4">
						                            <span>Töötatud tööpäevi</span>
                						        </div>
                						    </div>
                						</div>
                						<div class="bold wage-row xs-none" ng-if="last_calculation_salary_model == 2">
                						    <div class="row">
                						        <div class="col-md-6">
						                            <span>Tunnihind</span>
                						        </div>
                						        <div class="col-md-6">
						                            <span>Töötatud tunde</span>
                						        </div>
                						    </div>
                						</div>
                						<div class="bold wage-row xs-none" ng-if="last_calculation_salary_model == 3">
                						    <div class="row">
                						        <div class="col-md-2">
						                            <span>Põhipalk</span>
                						        </div>
                						        <div class="col-md-3">
						                            <span>Tööpäevi kuus</span>
                						        </div>
                						        <div class="col-md-3">
						                            <span>Töötatud tööpäevi</span>
                						        </div>
                						        <div class="col-md-2">
						                            <span>Lisatasupiir</span>
                						        </div>
                						        <div class="col-md-2">
						                            <span>Koefitsient</span>
                						        </div>
                						    </div>
                						</div>
                						
                						<div class="wage-row" ng-if="last_calculation_salary_model == 1">
                						    <div class="row">
                						        <div class="col-6 xs-show bold">
						                            <span>Põhipalk</span>
                						        </div>
                						        <div class="col-md-4 col-6">
						                            <span>{{user.front_calc.rate_month | number }} €</span>
                						        </div>
                						        <div class="col-6 xs-show bold">
						                            <span>Tööpäevi kuus</span>
                						        </div>
                						        <div class="col-md-4 col-6">
						                            <span>{{workdays | number }}</span>
                						        </div>
                						        <div class="col-6 xs-show bold">
						                            <span>Töötatud tööpäevi</span>
                						        </div>
                						        <div class="col-md-4 col-6">
						                            <span>{{worked_days | number }}</span>
                						        </div>
                						    </div>
                						</div>
                						<div class="wage-row" ng-if="last_calculation_salary_model == 2">
                						    <div class="row">
                						        <div class="col-6 xs-show bold">
						                            <span>Tunnihind</span>
                						        </div>
                						        <div class="col-md-6 col-6">
						                            <span>{{user.salary_calculated.rate_hour | number }} €</span>
                						        </div>
                						        <div class="col-6 xs-show bold">
						                            <span>Tunnid</span>
                						        </div>
                						        <div class="col-md-6 col-6">
						                            <span>{{(total_minutes/60).toFixed(2) | number }} h</span>
                						        </div>
                						    </div>
                						</div>
                						<div class="wage-row" ng-if="last_calculation_salary_model == 3">
                						    <div class="row">
                						        <div class="col-6 xs-show bold">
						                            <span>Põhipalk</span>
                						        </div>
                						        <div class="col-md-2 col-6">
						                            <span>{{user.front_calc.rate_month | number }} €</span>
                						        </div>
                						        <div class="col-6 xs-show bold">
						                            <span>Tööpäevad</span>
                						        </div>
                						        <div class="col-md-3 col-6">
						                            <span>{{workdays | number }}</span>
                						        </div>
                						        <div class="col-6 xs-show bold">
						                            <span>Töötatud päevad</span>
                						        </div>
                						        <div class="col-md-3 col-6">
						                            <span>{{worked_days | number }}</span>
                						        </div>
                						        <div class="col-6 xs-show bold">
						                            <span>Lisatasupiir</span>
                						        </div>
                						        <div class="col-md-2 col-6">
						                            <span>{{user.front_calc.bonus_threshold | number }} €</span>
                						        </div>
                						        <div class="col-6 xs-show bold">
						                            <span>Koefitsient</span>
                						        </div>
                						        <div class="col-md-2 col-6">
						                            <span>{{user.salary_calculated.coefficient | number }}</span>
                						        </div>
                						    </div>
                						</div>
                						<div class="wage-row separated bold">Märkus</div>
                						<div class="wage-row">
                    						<textarea ng-if="user.confirmed != 1" cols="32" rows="3" name="comment" ng-model="user.comment"></textarea>
                    						<p ng-if="user.confirmed == 1">{{user.comment || "-"}}</p>
                						</div>
                					</div>
                					<div class="col-md-10 col-sm-10" ng-if="user.one_time_model == 1">
                						<div class="bold wage-row xs-none" ng-if="last_calculation_salary_model == 1">
                						    <div class="row">
                						        <div class="col-md-4">
						                            <span>Põhipalk</span>
                						        </div>
                						        <div class="col-md-4">
						                            <span>Tööpäevi kuus</span>
                						        </div>
                						        <div class="col-md-4">
						                            <span>Töötatud tööpäevi</span>
                						        </div>
                						    </div>
                						</div>
                						<div class="bold wage-row xs-none" ng-if="last_calculation_salary_model == 2">
                						    <div class="row">
                						        <div class="col-md-6">
						                            <span>Tunnihind</span>
                						        </div>
                						        <div class="col-md-6">
						                            <span>Töötatud tunde</span>
                						        </div>
                						    </div>
                						</div>
                						<div class="bold wage-row xs-none" ng-if="last_calculation_salary_model == 3">
                						    <div class="row">
                						        <div class="col-md-2">
						                            <span>Põhipalk</span>
                						        </div>
                						        <div class="col-md-3">
						                            <span>Tööpäevi kuus</span>
                						        </div>
                						        <div class="col-md-3">
						                            <span>Töötatud tööpäevi</span>
                						        </div>
                						        <div class="col-md-2">
						                            <span>Lisatasupiir</span>
                						        </div>
                						        <div class="col-md-2">
						                            <span>Koefitsient</span>
                						        </div>
                						    </div>
                						</div>
                						
                						<div class="wage-row" ng-if="last_calculation_salary_model == 1">
                						    <div class="row">
                						        <div class="col-6 xs-show bold">
						                            <span>Põhipalk</span>
                						        </div>
                						        <div class="col-md-4 col-6">
						                            <span>{{user.front_calc.rate_month | number }} €</span>
                						        </div>
                						        <div class="col-6 xs-show bold">
						                            <span>Tööpäevi kuus</span>
                						        </div>
                						        <div class="col-md-4 col-6">
						                            <span>{{workdays | number }}</span>
                						        </div>
                						        <div class="col-6 xs-show bold">
						                            <span>Töötatud tööpäevi</span>
                						        </div>
                						        <div class="col-md-4 col-6">
						                            <span>{{worked_days | number }}</span>
                						        </div>
                						    </div>
                						</div>
                						<div class="wage-row" ng-if="last_calculation_salary_model == 2">
                						    <div class="row">
                						        <div class="col-6 xs-show bold">
						                            <span>Tunnihind</span>
                						        </div>
                						        <div class="col-md-6 col-6">
						                            <span>{{user.salary_calculated.rate_hour | number }} €</span>
                						        </div>
                						        <div class="col-6 xs-show bold">
						                            <span>Tunnid</span>
                						        </div>
                						        <div class="col-md-6 col-6">
						                            <span>{{total_minutes/60 | number }} h</span>
                						        </div>
                						    </div>
                						</div>
                						<div class="wage-row" ng-if="last_calculation_salary_model == 3">
                						    <div class="row">
                						        <div class="col-6 xs-show bold">
						                            <span>Põhipalk</span>
                						        </div>
                						        <div class="col-md-2 col-6">
						                            <span>{{user.front_calc.rate_month | number }} €</span>
                						        </div>
                						        <div class="col-6 xs-show bold">
						                            <span>Tööpäevad</span>
                						        </div>
                						        <div class="col-md-3 col-6">
						                            <span>{{workdays | number }}</span>
                						        </div>
                						        <div class="col-6 xs-show bold">
						                            <span>Töötatud päevad</span>
                						        </div>
                						        <div class="col-md-3 col-6">
						                            <span>{{worked_days | number }}</span>
                						        </div>
                						        <div class="col-6 xs-show bold">
						                            <span>Lisatasupiir</span>
                						        </div>
                						        <div class="col-md-2 col-6">
						                            <span>{{user.front_calc.bonus_threshold | number }} €</span>
                						        </div>
                						        <div class="col-6 xs-show bold">
						                            <span>Koefitsient</span>
                						        </div>
                						        <div class="col-md-2 col-6">
						                            <span>{{user.salary_calculated.coefficient | number }}</span>
                						        </div>
                						    </div>
                						</div>
                						<div class="wage-row separated bold">Märkus</div>
                						<div class="wage-row">
                    						<textarea ng-if="user.confirmed != 1" cols="32" rows="3" name="comment" ng-model-options='{ debounce: 500 }' ng-change="updateSalaryParameters(1)" ng-model="user.comment"></textarea>
                    						<p ng-if="user.confirmed == 1">{{user.comment}}</p>
                						</div>
                					</div>
                					<div class="col-md-2 col-12 text-right">
                						<div class="bold wage-row user-approve-text">BRUTOPALK</div>
                						<div class="wage-row bold user-approve-text">{{user.salary | number: 0 }} €</div>
                						<div ng-if="user.salary_calculated.salary_model == 3" class="bold wage-row user-approve-text">Sh lisatasu</div>
                						<div ng-if="user.salary_calculated.salary_model == 3" class="wage-row bold user-approve-text">{{user.front_calc.bonus | number: 0 }} €</div>
                					</div>
                				</div>
                            </div>
                        </section>
                        <section class="wage-section wage-bg" ng-if="isNumber(user.salary) && user.role != 'OUTSOURCER'">
                            <div class="wage-section__item wage-section__confirm">
                                <div ng-if="user.confirmed != 1" class="text-warning float-left">Palk ei ole kinnitatud</div>
                                <div ng-if="user.confirmed == 1" class="item-update float-left">Palk kinnitatud</div>
                                <button ng-if="user.confirmed != 1" ng-class="{ 'btn-warning' : user.confirmed != 1, 'btn-primary' : user.confirmed == 1 }" type="submit" ng-click="confirmSalary()" class="btn float-right">
                                    <span ng-if="user.confirmed != 1">Kinnita</span>
                                    <span ng-if="user.confirmed == 1">Palk kinnitatud</span>
                                </button>
                                <button ng-if="user.confirmed == 1" type="submit" ng-click="promptWage()" class="btn btn-primary float-right">Lähtesta palk</button>
                            </div>
                        </section>
                        <toalert-message class="mt-4 mb-4" ng-if="messages.generalAlertMessage" toalert="messages.generalAlertMessage"></toalert-message>
                    </div>
                </div><!--inside row-->
                <div class="row mt-3" ng-if="user && user.role != 'OUTSOURCER'">
                    <div class="col-md-6 col-6 orientation-items">
                        <div class="left-align">
                            <a href="" ng-show="workerPrevId" ng-click="goToPreviousWorker()">
                                <i class="fa fa-arrow-left fa-2x"></i>
                                <span>Eelmine</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6 col-6 orientation-items">
                        <div class="right-align">
                            <a href="" ng-show="workerNextId" ng-click="goToNextWorker()">
                                <span>Järgmine</span>
                                <i class="fa fa-arrow-right fa-2x"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row mt-3" ng-if="user && user.role == 'OUTSOURCER'">
                    <div class="col-md-6 col-6 orientation-items">
                        <div class="left-align">
                            <a href="" ng-show="outSourcerPrevId" ng-click="goToPreviousSourcer()">
                                <i class="fa fa-arrow-left fa-2x"></i>
                                <span>Eelmine</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6 col-6 orientation-items">
                        <div class="right-align">
                            <a href="" ng-show="outSourcerNextId" ng-click="goToNextSourcer()">
                                <span>Järgmine</span>
                                <i class="fa fa-arrow-right fa-2x"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div ng-if="!user" class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </main><!--col9-->
        </div><!--row-->
        
    </div><!--/container-->
</script>