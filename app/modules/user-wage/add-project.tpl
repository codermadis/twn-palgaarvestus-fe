<script type="text/ng-template" id="add-project.tpl">
    <div class="modal-dialog modal-project">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{modalHeader}}</h4>
                <button type="button" class="close" ng-click="close(false)" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form name="projectForm" id="projectForm">
                <div class="modal-body">
                    <div class="row">
    				    <div class="form-group col-md-2">
    				        <label for="first_name">Projekt</label>
    					    <div class="auto-complete">
            				    <input type="text" ng-model="autoModel.selectedProject" placeholder="" typeahead-on-select="setProjectProperties($item)" uib-typeahead="item as item.name for item in getProject($viewValue)" typeahead-min-length="2" class="form-control">
                            </div>
    					</div>
    				    <div class="form-group col-md-2">
    				        <label for="last_name">Tunnid</label>
    					    <input class="form-control" type="text" name="hours" id="hours" valid-number smart-float ng-model="model.hours" required>
    					</div>
    					<div class="form-group col-md-2">
    				        <label for="last_name">Staatus</label>
    					    <select class="form-control" name="status" id="status" ng-model="model.billable" ng-options="item.value as item.name for item in billableValues" required></select>
    					</div>
    					<div class="form-group col-md-3">
    				        <label for="last_name">Palgaarvestuse €/h</label>
    					    <input class="form-control" type="text" name="rate_salary" valid-number smart-float id="rate_salary" ng-model="model.rate_salary" required>
    					</div>
    					<div class="form-group col-md-3">
    				        <label for="last_name">Statistika €/h</label>
    					    <input class="form-control" type="text" name="rate_statistic" valid-number smart-float id="rate_statistic" ng-model="model.rate_statistic" required>
    					</div>
    				</div>
    			</div>
                <div class="modal-footer">
                    <div class="col-md-6 col-sm-12 right-align">
                        <button type="button" ng-click="close(false)" class="btn btn-warning" data-dismiss="modal">Katkesta</button>
                        <button type="submit" ng-click="add(projectForm)" class="btn btn-primary" data-dismiss="modal">Salvesta</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</script>