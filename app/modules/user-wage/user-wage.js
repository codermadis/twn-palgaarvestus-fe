App.controller('user-wage', function($scope, $http, $rootScope, $routeParams, $location, ModalService, $route, $anchorScroll){
    
    $scope.messages = { alertMessage: false, errorMessage: false, generalAlertMessage: false };
    $scope.salaryVal = ['Põhipalk', 'Tunnipõhine', 'Valemipõhine'];
    
    $scope.addCustomProject = function() {
        ModalService.showModal({
            templateUrl: 'addProject.html',
            controller: "addProject"
        }).then(function(modal) {
            modal.scope.modalHeader = "Lisa projekt";
            modal.element.show();  
            modal.close.then(function(result) {
                if(result) { $scope.user = false; $scope.getUserInfo(); }
            });
        });
    };
    $scope.deleteProject = function(id) {
        $http.delete(settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users/' + $routeParams.id + '/projects/' + id).then(function(response) {
            $route.reload();
        }, function(response) {
            $scope.messages.errorMessage = response.data.message;
        });
    }
    
    
    $scope.setProjectParameters = function(id, hours, rate, statistic) {
        var minutes = hours*60;
        $http({
			method: "PUT",
			url: settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users/' + $routeParams.id + '/projects/' + id,
			data: {
			    minutes: minutes,
			    rate_salary: rate,
			    rate_statistic: statistic
			},
		}).then(function( response ){
		    var message = response.data.message;
		  //  $scope.messages.alertMessage = message;
		  
		  $scope.getUserInfo();
		  
		}, function(response){
		    var message = response.data.message;
	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
		});
        
        $scope.total_minutes = 0;
        $scope.total_calculated_income = 0;
        $scope.total_calculated_statistic_income = 0;
        
        var allProjects = $scope.user.projects;
        for(var project in allProjects) {
            $scope.total_minutes += allProjects[project].hours*60;
            allProjects[project].calculated_income = allProjects[project].rate_salary*(allProjects[project].hours);
            allProjects[project].calculated_statistic_income = allProjects[project].rate_statistic*(allProjects[project].hours);
            $scope.total_calculated_income += parseInt(allProjects[project].calculated_income);
            $scope.total_calculated_statistic_income += parseInt(allProjects[project].calculated_statistic_income);
        }
        $scope.total_calculated_income = $scope.total_calculated_income.toFixed(2);
        $scope.total_calculated_statistic_income = $scope.total_calculated_statistic_income.toFixed(2);
        $scope.total_minutes_floored = Math.floor($scope.total_minutes/60);
        if(!$scope.total_minutes_floored) { $scope.total_minutes_floored = "00" };
        $scope.total_minutes_remainder = Math.floor($scope.total_minutes%60);
        if($scope.total_minutes_remainder == 0) { $scope.total_minutes_remainder = "00" };
        if(!$scope.total_minutes_remainder) { $scope.total_minutes_remainder = "" };
        // $scope.updateSalaryParameters($scope.user.one_time_model); // removed at 15.09, Silver
    };
    $scope.getUserInfo = function() {
        $http.get(settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users/' + $routeParams.id).then(function(response) {
            $scope.user = response.data;
            $scope.workdays = response.data.workdays;
            $scope.worked_days = response.data.worked_days;
            if (!$scope.user.confirmed) {
                $scope.user.workdays = response.data.workdays_dynamic;
                $scope.user.worked_days = response.data.worked_days_dynamic;
            }
            $scope.total_minutes = 0;
            $scope.total_calculated_income = 0;
            $scope.total_calculated_statistic_income = 0;
            
            var allProjects = $scope.user.projects;
            // console.log(allProjects)
            for(var project in allProjects) {
                allProjects[project].hours = (allProjects[project].minutes/60).toFixed(2);
                $scope.total_minutes += allProjects[project].minutes;
                var calculated_income = allProjects[project].rate_salary*(allProjects[project].minutes/60);
                var calculated_statistic_income = allProjects[project].rate_statistic*(allProjects[project].minutes/60);
                allProjects[project].calculated_income = calculated_income.toFixed(2);
                allProjects[project].calculated_statistic_income = calculated_statistic_income.toFixed(2);
                $scope.total_calculated_income += calculated_income;
                $scope.total_calculated_statistic_income += calculated_statistic_income;
            }
            $scope.total_calculated_income = $scope.total_calculated_income.toFixed(0);
            $scope.total_calculated_statistic_income = $scope.total_calculated_statistic_income.toFixed(0);
            $scope.total_minutes_floored = Math.floor($scope.total_minutes/60);
            if(!$scope.total_minutes_floored) { $scope.total_minutes_floored = "00" };
            $scope.total_minutes_remainder = Math.floor($scope.total_minutes%60);
            if($scope.total_minutes_remainder == 0) { $scope.total_minutes_remainder = "00" };
            // $scope.updateSalaryParameters($scope.user.one_time_model); // removed at 15.09, Silver
			if($scope.user.salary) {
			 //   $scope.user.salary = Math.round($scope.user.salary);
			}
            $scope.user.front_calc = {};
            $scope.user.front_calc.rate_month = ($scope.user.salary_calculated.rate_month * ($scope.worked_days/$scope.workdays)).toFixed(0);
            $scope.user.front_calc.bonus_threshold = ($scope.user.salary_calculated.bonus_threshold * ($scope.worked_days/$scope.workdays)).toFixed(0);
            $scope.user.front_calc.bonus = ($scope.user.salary - ($scope.user.salary_calculated.rate_month * ($scope.worked_days/$scope.workdays))).toFixed(0);
            
            if(!$scope.last_calculation_salary_model) { $scope.last_calculation_salary_model = $scope.user.salary_calculated.salary_model; };
        })
    };
    $scope.getUserInfo();
    
    $scope.goToNextWorker = function() {
        $location.path('wages/' + $rootScope.workerNextId);
    };
    $scope.goToPreviousWorker = function() {
        $location.path('wages/' + $rootScope.workerPrevId);
    };
    $scope.goToNextSourcer = function() {
        $location.path('wages/' + $rootScope.outSourcerNextId);
    };
    $scope.goToPreviousSourcer = function() {
        $location.path('wages/' + $rootScope.outSourcerPrevId);
    };
    
    $scope.setWorkedDays = function() {
        var user = angular.copy($scope.user);
        var workedDays = user.worked_days;
        var submitData = {};
        if(user.worked_days <= user.workdays) { 
            submitData['worked_days'] = user.worked_days;
            $http({
    			method: "PUT",
    			url: settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users/' + $routeParams.id + '/salary',
    			data: submitData,
    		}).then(function( response ){
    		  //  var message = response.data.message;
    		  //  $scope.messages.alertMessage = message;
    		  $scope.workedDaysFalse = false;
    		}, function(response){
    		    var message = response.data.message;
    	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
    		});
        } else if(user.worked_days > 0 && workedDays.toString().indexOf('.') == -1) {
            $scope.workedDaysFalse = true;
            var message = "Töötatud päevade arv ei saa olla suurem kui tööpäevade arv kuus";
	        if(message.length > 0) { $scope.messages.errorMessage = message; };  
	        window.scrollTo(0, 0);	
        }
    }
    $scope.setOverallWorkDays = function() {
        var user = angular.copy($scope.user);
        var submitData = {};
        if(user.worked_days <= user.workdays) { 
            submitData['workdays'] = user.workdays;
            $http({
    			method: "PUT",
    			url: settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users/' + $routeParams.id + '/salary',
    			data: submitData,
    		}).then(function( response ){
    		  $scope.overallWorkDaysFalse = false;
    		}, function(response){
    		    var message = response.data.message;
    	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
    		});
        } else {
            $scope.overallWorkDaysFalse = true;
            var message = "Tööpäevade arv ei saa olla väiksem kui töötatud tööpäevade arv kuus";
	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }  
	        window.scrollTo(0, 0);	
        }
    }
    
    $scope.updateExpenseParams = function() {
        var user = angular.copy($scope.user);
        var submitData = {};
        submitData = {
            'salary_model' : 1,
            'rate_month' : user.salary_calculated.rate_month,
            // 'comment' : user.comment,
            'one_time_model' : 1,
        };
        $http({
			method: "PUT",
			url: settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users/' + $routeParams.id + '/salary',
			data: submitData,
		}).then(function( response ){
		  //  var message = response.data.message;
		  //  $scope.messages.alertMessage = message;
		}, function(response){
		    var message = response.data.message;
	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
		});
    }
    
    $scope.updateSalaryParameters = function(modifier) {
        //Accepted keys: 'salary_model', 'rate_hour', 'rate_month', 'coefficient', 'bonus_threshold', 'worked_days', 'comment'
        var user = angular.copy($scope.user);
        var submitData = {};
        if(modifier == 1) { 
            submitData = {
                'salary_model' : user.salary_calculated.salary_model,
                'rate_hour' : user.salary_calculated.rate_hour,
                'rate_month' : user.salary_calculated.rate_month,
                'coefficient' : user.salary_calculated.coefficient,
                'bonus_threshold' : user.salary_calculated.bonus_threshold,
                'comment' : user.comment,
                'one_time_model' : user.one_time_model,
            };
        } else {
            if(user.salary_default.salary_model) {
                submitData = {
                    'salary_model' : user.salary_default.salary_model,
                    'rate_hour' : user.salary_default.rate_hour,
                    'rate_month' : user.salary_default.rate_month,
                    'coefficient' : user.salary_default.coefficient,
                    'bonus_threshold' : user.salary_default.bonus_threshold,
                    'comment' : user.comment,
                    'one_time_model' : user.one_time_model,
                };
            } else {
                submitData = {
                    'salary_model' : null
                }
            }
        }
        $http({
			method: "PUT",
			url: settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users/' + $routeParams.id + '/salary',
			data: submitData,
		}).then(function( response ){
		  //  var message = response.data.message;
		  //  $scope.messages.alertMessage = message;
		}, function(response){
		    var message = response.data.message;
	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
		});
    }
    
    $scope.calculateSalary = function(currentSalary) {
        // THIS UPDATES SALARY MODEL NOW
        var user = angular.copy($scope.user);
        var submitData = { 
            worked_days: user.worked_days,
            workdays: user.workdays,
            one_time_model: user.one_time_model ? user.salary_calculated : null,
        };
        $http({
			method: "PUT",
			url: settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users/' + $routeParams.id + '/calculate',
			data: submitData,
		}).then(function( response ){
		    $scope.user.salary = response.data.salary;
		    $scope.user.salary_calculated = response.data;
		    $scope.last_calculation_salary_model = $scope.user.salary_calculated.salary_model;
		    $scope.user = false;
		    $scope.getUserInfo();
		}, function(response){
		    var message = response.data.message;
	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
		});
    }
    $scope.isNumber = function(num) {
        if (num == null) {
            return false;
        } else {
            return true;
        };
    };
    
    $scope.calculateExpense = function(currentSalary) {
        var user = angular.copy($scope.user);
        var submitData = {
            worked_days: user.worked_days,
            workdays: user.workdays,
            one_time_model: user.salary_calculated
        };
        
        $http({
			method: "PUT",
			url: settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users/' + $routeParams.id + '/calculate',
			data: submitData,
		}).then(function( response ){
		  //  $scope.user.salary = response.data.salary;
		    $scope.user = false;
		    $scope.getUserInfo();
		}, function(response){
		    var message = response.data.message;
	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
		});    
    }
    $scope.confirmSalary = function() {
        
        var user = angular.copy($scope.user);
        var submitData = {
            comment: user.comment,
            salary_base: user.salary_base,
            salary_bonus: user.salary_bonus,
            salary: user.employee === 1 ? user.salary : user.salary_calculated.rate_month,
            minutes: user.minutes
        };
        // return console.log(submitData)
        $http({
			method: "PUT",
			url: settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users/' + $routeParams.id + '/confirm',
			data: submitData,
		}).then(function( response ){
		  //  $scope.scrollBottom = true;
		    $scope.user = false;
		    $scope.getUserInfo();
		}, function(response){
		    var message = response.data.message;
	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
		});
    }
    $scope.promptWage = function(id, role){
        ModalService.showModal({
            templateUrl: 'wagePrompt.html',
            controller: "wagePrompt"
        }).then(function(modal) {
            modal.scope.modalHeader = "Oled sa kindel?";
            if(role == "OUTSOURCER") {
                modal.scope.modalText = "Kulud juba lisatud! Kas soovid lähtestada?";
            } else { modal.scope.modalText = "Palk on juba arvutatud. Edasi liikudes kasutaja palgaarvestus lähtestatakse, kas soovid jätkata?"; }
            modal.scope.userId = $routeParams.id;
            modal.element.show();  
            modal.close.then(function(result) {
                $route.reload();
            });
        });
    };
});