<script type="text/ng-template" id="archive-email.tpl">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{email.subject}}</h4>
                <button type="button" class="close" ng-click="close(false)" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
            	<p><strong>Saatja:</strong> {{email.from}}</p>
            	<p><strong>Saaja:</strong> {{email.to}}</p>
            	<p><strong>cc:</strong> {{email.cc}}</p>
            	<p ng-bind-html="email.content"></p>
            	
            	<div class="row">
		    	<div class="form-group col-md-12">
				    <span class="bold">Manused</span>
				</div>
				<div class="form-group col-md-12">
				    <div class="bordered-elem">
				        <div ng-if="!email.attachments.length"class="p-3">Puuduvad</div>
				        <div ng-if="email.attachments.length" class="p-3">
				            
                            <div class="d-inline-block text-center pr-2 pl-2 fade-element-in fade-element-out" ng-repeat="file in email.attachments" ng-if="file && email.attachments.length">
                                <a ng-if="file.type === 'vacation'" ng-click="downloadVacationDocument(file.id)">
                                    <i class="fa fa-download fa-2x"></i>
                                </a>
                                <a ng-if="file.type !== 'vacation'" ng-click="downloadDocument(file.id)">
                                    <i class="fa fa-download fa-2x"></i>
                                </a>
                                <div class="files-name">{{file.originalname}}</div>
                            </div>
                        </div>
				    </div>
				</div>
			</div>
            </div>
            
        </div>
    </div>
</script>