<script type="text/ng-template" id="archive.tpl">
    <div class="container">
        <div class="row">
            <main class="col-sm-12 pt-3 mt-2">
				<ng-include src="'breadcrumbs.tpl'"></ng-include>
				<error-message class="mt-4" ng-if="messages.errorMessage" fadeout=true error="messages.errorMessage"></error-message>
				<ul class="nav nav-tabs mt-4">
				  <li class="nav-item">
				    <a ng-class="{ 'active' : tabs.isActive('PROJECTS') }" ng-click="tabs.setActive('PROJECTS')" class="nav-link" href="">Projektid</a>
				  </li>
				  <li class="nav-item" ng-hide="activeRole == 'USER'">
				    <a ng-class="{ 'active' : tabs.isActive('EMPLOYEES') }" ng-click="tabs.setActive('EMPLOYEES')" class="nav-link" href="">Töötajad</a>
				  </li>
				  <li class="nav-item" ng-hide="activeRole == 'USER'">
				    <a ng-class="{ 'active' : tabs.isActive('EMAILS') }" ng-click="tabs.setActive('EMAILS')" class="nav-link" href="">Emailid</a>
				  </li>
				</ul>
				<form>
					<div class="row">
						<div class="col-12 col-md-12">
							<div class="bordered-elem p-3">
								<div class="form-group mb-0" ng-if="!tabs.isActive('EMAILS')">
									<h5 class="form-control-label d-inline">Kuu</h5>
									<select ng-model="selectedMonth" 
											class="form-control custom-select" 
											ng-options="item.name as item.label for item in monthValues" 
											ng-change="onChangeData(selectedMonth)" 
											style="width:200px; margin-left: 11px;">
									</select>
								</div><!--/form-group-->
								<div class="form-group mb-0" ng-if="tabs.isActive('EMAILS')">
									<h5 class="form-control-label d-inline">Aasta</h5>
									<select ng-model="selectedYear" 
											class="form-control custom-select" 
											ng-options="item for item in yearValues" 
											ng-change="onChangeData(selectedYear)" 
											style="width:200px; margin-left: 11px;">
									</select>
								</div><!--/form-group-->
	                        </div>
						</div><!--/col-3 col-md-3-->
					</div><!--/row-->
				</form>
			
				<section class="projects table-holder mt-4 fade-element-in" ng-if="tabs.isActive('PROJECTS')">
                    <table class="table table-bordered table-striped table-hover"> <!--Table-->
                        <thead>
                            <tr><!--Headers-->
                                <th>Projekt</th> 
                                <th>Tunnid</th>
                                <th>Staatus</th>
                                <th>Palgaarvestuse tunnihind</th>
                                <th>Statistika tunnihind</th>
                                <th>Kommentaar</th>   
                            </tr>
                        </thead>
                        <tbody class="fade-element-in" ng-if="projectData">
                            <tr ng-repeat="project in projectData | orderBy:'minutes' | orderBy:'billable': 1 | limitTo: limit">
                                <td>
                                    <span>{{project.name}}</span>
                                    <small-alert-message ng-if="messages.updatedProjectId && project.id == messages.updatedProjectId" project="messages.updatedProjectId"></small-alert-message>
                                </td>
                                <td>{{project.hour_floored}}<span ng-if="project.minute_remainder != 0">:{{project.minute_remainder}}</span></td>
                                <td>{{billableVal[project.billable]}}</td>
                                <td>{{project.rate_salary | number}}<span ng-if="project.rate_salary"> €</span></td>
                                <td>{{project.rate_statistic | number}}<span ng-if="project.rate_statistic"> €</span></td>
                                <td>{{project.comment}}</td>
                            </tr>
                        </tbody>
                    </table><!-- End of table-->
                    <div class="row" ng-if="projectData && tabs.isActive('PROJECTS')" ng-hide="limit > projectData.length">
						<div class="col-12 col-md-12 text-right mt-3">
							<a href="" ng-click="upTheLimit()">Näita rohkem</a>
						</div><!--/col-12 col-md-12-->
					</div><!--/row-->
				</section>
				
				<section class="users table-holder mt-4 mb-4 fade-element-in" ng-if="tabs.isActive('EMPLOYEES')" ng-hide="activeRole == 'USER'">
					<div class="row" ng-if="userData" >
	                    <div class="col-lg-6 col-md-12 col-sm-12">
	                        <table class="table table-striped table-bordered table-hover">
	                            <thead>
	                                <tr>
	                                    <th width="33%">Töötaja</th>
	                                    <th width="33%">Brutokuupalk</th>
	                                    <th width="35%">Palgakokkuvõte</th>
	                                </tr>
	                            </thead>
	                            <tbody class="fade-element-in">
	                                <tr ng-repeat="user in userData | orderBy: 'first_name' | filter: { 'role' : '!' + outSourcer, 'confirmed' : 1 } as receivedUserData">
	                                    <td>{{user.first_name}} {{user.last_name}}</td>
	                                    <td>{{user.salary | number}}<span ng-if="user.salary && user.confirmed === 1"> €</span></td>
	                                    <td class="text-center">
	                                        <a ng-click="openEmailView(user.id, user.first_name, user.last_name)" href="" class="btn btn-primary">
	                                            <span>Vaata</span>
	                                        </a>	
	                                    </td>
	                                </tr>
	                                <tr ng-if="!receivedUserData">
                                        <td colspan=100>Kirjed puuduvad</td>
                                    </tr>
	                            </tbody>
	                        </table>
	                    </div> <!--col-12-->
	                </div><!--inside row-->
				</section>
				
				<section class="table-holder mt-4 mb-4 fade-element-in" ng-if="tabs.isActive('EMAILS')">
	            	<table class="table table-bordered table-striped table-hover"> <!--Table-->
                        <thead>
                            <tr><!--Headers-->
                                <th>Saatja</th> 
                                <th>Teema</th>
                                <th>Saadetud</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody class="fade-element-in">
                            <tr ng-repeat="email in emailData | limitTo: emailLimit">
                                <td>{{email.from}}</td>
                                <td>{{email.subject}}</td>
                                <td>{{email.created_at}}</td>
                            	<td class="text-center">
                                    <a ng-click="openEmail(email)" href="" class="btn btn-primary">
                                        <span>Vaata</span>
                                    </a>	
                                </td>
                            </tr>
                        </tbody>
                    </table><!-- End of table-->
                    <div class="row" ng-if="emailData && tabs.isActive('EMAILS')" ng-hide="emailLimit > emailData.length">
						<div class="col-12 col-md-12 text-right mt-3">
							<a href="" ng-click="upTheEmailLimit()">Näita rohkem</a>
						</div><!--/col-12 col-md-12-->
					</div><!--/row-->
				</section>
				
            </main><!--col9-->
        </div><!--row-->
      </div><!--/container-->
</script>