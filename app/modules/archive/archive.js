App.controller('archive', function($scope, $http, $rootScope, $routeParams, $location, ModalService){
    $scope.billableVal = [ "Unbillable", "Billable" ];
    $scope.limit = 20;
    $scope.emailLimit = 20;
    $scope.monthValues = [];
    $scope.yearValues = [];
    $scope.outSourcer = "OUTSOURCER";
    $scope.selectedYear = $rootScope.thisYearNum;
    $scope.api_host = settings.prefix;
    
    var startYear = 2017;
    var startMonth = 1;
    for(var i = startYear; i <= $rootScope.thisYearNum; i++) {
        if(i == $rootScope.thisYearNum) { var endMonth = $rootScope.prevMonthNum; startMonth = 1 } else { endMonth = 12 }
        for(var j = startMonth; j <= endMonth; j++) {
            if(j < 10) { var monthVal = j.toString(); monthVal = '0' + monthVal } else { monthVal = j };
            var monthHash = {};
            monthHash['label'] = $rootScope.monthLabels[j-1] + ' ' + i;
            monthHash['name'] = i + '-' + monthVal;    
            $scope.monthValues.push(monthHash);
        }
    }
    $scope.monthValues = $scope.monthValues.reverse();
	$scope.selectedMonth = $rootScope.finalPreviousMonth;
	
	for (var year = startYear; year <= $rootScope.thisYearNum; year++) {
	    $scope.yearValues.push(year)
	}
	
	$scope.messages = { alertMessage: false, errorMessage: false };
	
	$scope.$watch('tabs._active', function(newValue, oldValue){
	    if(newValue != oldValue) {
            $scope.getData(newValue); 
	    };
    });
	
	$scope.tabs = {
	    _active: 'PROJECTS',
	    setActive: function(tab) {
	        this._active = tab;
	    },
	    isActive: function(tab) {
	        return this._active === tab;
	    },
	    getActive: function() {
	        return this._active;
	    }
	}
	
	$scope.upTheLimit = function() {
	    $scope.limit += 20;
	}
	$scope.upTheEmailLimit = function() {
	    $scope.emailLimit += 20;
	}
	
	$scope.onChangeData = function(selected) {
	    var activeTab = $scope.tabs.getActive()
	    switch (activeTab) {  
            case 'PROJECTS':
            case 'EMPLOYEES':
                $scope.getData(activeTab, {
                    month: selected
                })
                break;
            case 'EMAILS':
                $scope.getData(activeTab, {
                    year: selected
                })
                break;
        }
	}
	
	$scope.getData = function(tab, options) {
	    options = options || {}
	    var selectedMonth = options.month || $scope.selectedMonth
	    var selectedYear = options.year || $scope.selectedYear
        switch (tab) {  
            case 'PROJECTS':
                $scope.projectData = false;
                if(!selectedMonth) {return;}
                var url = settings.prefix+ 'archive/months/' + selectedMonth + '/projects'
                break;
            case 'EMPLOYEES':
                $scope.userData = false;
                if(!selectedMonth) {return;}
                var url = settings.prefix+ 'archive/months/' + selectedMonth + '/users'
                break;
            case 'EMAILS':
                $scope.emailData = false;
                if(!selectedYear) {return;}
                var url = settings.prefix+ 'archive/emails/' + selectedYear
                break;
        }
        $http.get(url).then(function(response) {
            switch (tab) {
                case 'PROJECTS':
                    $scope.projectData = response.data;
                    var projects = response.data;
                    for(var item in projects) {
                        projects[item].minute_remainder = projects[item].minutes%60;
                        projects[item].hour_floored = Math.floor(projects[item].minutes/60);
                        if($rootScope.changedProjects.includes(projects[item].id)) { projects[item].changed = true }
                    }
                    break;
                case 'EMPLOYEES':
                    $scope.userData = response.data;
                    break;
                case 'EMAILS':
                    $scope.emailData = response.data;
                    break;
            }
        }, function(response) {
            if (options.hideErrors) return;
            var message = response.data.message;
		    if(message.length > 0) { 
		        $scope.messages.errorMessage = message; 
		    } else if (message.constructor === Object) { 
		        $scope.messages.errorMessage = response.statusText 
		    }       
        });
    };
    
    $scope.getData('PROJECTS');
    $scope.getData('EMPLOYEES', {hideErrors: true});
    $scope.getData('EMAILS', {hideErrors: true});
    
    $scope.openEmailView = function(id, first, last){
        ModalService.showModal({
            templateUrl: 'wageEmail.html',
            controller: "wageEmail"
        }).then(function(modal) {
            
            $http.get(settings.prefix+ 'months/' + $scope.selectedMonth + '/users/' + id + '/email').then(function(response) {
                modal.scope.hideActions = true;
                modal.scope.user = response.data;
                modal.scope.user.name = first + ' ' + last;
                modal.scope.userId = id;
                
                var monthNumber = parseInt($scope.selectedMonth.slice(-1));
                var monthLabel = $rootScope.monthLabels[monthNumber-1];
                var yearLabel = $rootScope.thisYear;
                modal.scope.wageEmailTitle = monthLabel + ' ' + yearLabel + ' - ' + modal.scope.user.name;
            }, function(response) {
                console.log(response);
            }).then(function(){
                modal.element.show();  
            });
            modal.close.then(function(result) {
                
            });
        });
    };
    
    $scope.openEmail = function(email){
        ModalService.showModal({
            templateUrl: 'archiveEmail.html',
            controller: "archiveEmail"
        }).then(function(modal) {
            modal.element.show();  
            modal.scope.email = email;
            modal.scope.apiUrl = settings.prefix;
            modal.close.then(function(result) {
                
            });
        });
    };
})