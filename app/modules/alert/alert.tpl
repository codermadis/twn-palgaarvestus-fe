<script type="text/ng-template" id="alert.tpl">
    <div class="container">

        <div class="row">
            <main class="col-sm-9 pt-3">
                <div class="spacing"></div>
                <ol class="breadcrumb">
				  <li class="breadcrumb-item active">Projektid</li>
				</ol>
                <div class="row">
                    <div class="col-12">
                        <div class="alert alert-danger" role="alert ">
                            "Alert message", nothing has been imported.
                        </div>
                    </div>
                </div>
            
            </main><!--col9-->
        </div><!--row-->
      </div><!--/container-->
</script>
