<script type="text/ng-template" id="breadcrumbs.tpl">
    <ol class="breadcrumb">
        <li class="padding-li" ng-repeat="breadcrumb in breadcrumbs.get() track by breadcrumb.path" ng-class="{ active: $last }">
            <a ng-if="!$last" ng-href="{{ breadcrumb.path }}" ng-bind="breadcrumb.label" class="margin-right-xs"></a>
            <i ng-if="!$last" class="fa fa-angle-right fa-1x padding-i"></i>
            <span ng-if="$last" ng-bind="breadcrumb.label"></span>
        </li>
    </ol>
</script>