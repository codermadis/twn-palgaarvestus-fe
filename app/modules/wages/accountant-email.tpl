<script type="text/ng-template" id="accountant-email.tpl">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Palgaarvestuse kokkuvõte {{previousMonthLabel.toLowerCase()}} {{thisYear}}</h4>
                <button type="button" class="close" ng-click="close(false)" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
            	<div class="row">
                    <div class="form-group col-md-12">
                        <error-message ng-if="messages.errorMessage" fadeout=true error="messages.errorMessage"></error-message>
                        <toalert-message ng-if="messages.alertMessage" toalert="messages.alertMessage"></toalert-message>
                    </div>
                </div>
				<div class="row">
			    	<div class="form-group col-md-12">
					    <span class="bold">Sisu</span>
					</div>
					<div class="form-group col-md-12">
					    <textarea ui-tinymce="tinymceOptions" ng-model="model.email_body"></textarea>
					</div>
				</div>
				<div class="row">
			    	<div class="form-group col-md-12">
					    <span class="bold">Manused</span>
					</div>
					<div class="form-group col-md-12">
					    <div class="bordered-elem">
					        <div ng-if="!model.files.length"class="p-3">Puuduvad</div>
					        <div ng-if="model.files.length" class="p-3">
                                <div class="d-inline-block text-center pr-2 pl-2 fade-element-in fade-element-out" ng-repeat="file in model.files" ng-if="file && model.files.length">
                                    <a ng-if="file.id && file.removable" class="files-remove fade-element-out" href="" ng-click="deleteDocument(file.id)"><i class="fa fa-close address-close user-alert-text"></i></a>
                                    <a ng-if="file.type === 'vacation'" ng-click="downloadVacationDocument(file.id)">
                                        <i class="fa fa-download fa-2x"></i>
                                    </a>
                                    <a ng-if="file.type !== 'vacation'" ng-click="downloadDocument(file.id)">
                                        <i class="fa fa-download fa-2x"></i>
                                    </a>
                                    <div class="files-name">{{file.originalname}}</div>
                                </div>
                            </div>
					    </div>
					</div>
				</div>
				<div class="row">
			    	<div class="form-group col-md-12">
			    		<div class="bold d-inline mr-3">Lae üles: </div>
			    		<button ngf-select="uploadFiles($files, $invalidFiles)" multiple ngf-max-size="5MB">Vali failid</button>
					</div>
				</div>
            </div>
            <div class="modal-footer" ng-if="!hideActions">
                <button type="button" ng-click="close(false)" class="btn btn-warning" data-dismiss="modal">Katkesta</button>
                <button type="button" ng-if="!emailSent" ng-disabled="disableSend" ng-click="sendEmail()" class="btn btn-primary" data-dismiss="modal">Saada</button>
            </div>
        </div>
    </div>
</script>