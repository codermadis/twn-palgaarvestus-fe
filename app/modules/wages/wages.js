App.controller('wages', function($scope, $http, $rootScope, $q,$routeParams, $location, $uibModal, $document, ModalService, $filter, $window, $route){
    
    var VACATION_TYPES = {
        'vacation': 'Puhkus',
        'unpaid': 'Palgata puhkus',
        'study': 'Õppepuhkus',
        'disability': 'Töövõimetus',
        'child': 'Lapsepuhkus',
        'other': 'Eemalviibimine'
    };
    
    $scope.loading = false;
    $scope.messages = { alertMessage: false, errorMessage: false };
    $scope.accountingEmailSent = false;
    $scope.statusReceived = false;
    $scope.outSourcer = "OUTSOURCER";
    
    $scope.getUsers = function() {
        $http.get(settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users').then(function(response) {
            $scope.users = response.data;
            $scope.wagesNotFound = false;
        }, function(response) {
            $scope.messages.errorMessage = response.data.message;
            $scope.wagesNotFound = true;
        });
    };
    $scope.getUsers();
    
    $scope.generateEmail = function() {
        $http.get(settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/accounting/email').then(function(response) {
            $scope.generatedEmail = response.data;
            
            var mailbody = '';
            var lb = '%0D%0A'; var sp = ' '; var tab = '    '; var eur = '€'; var semi = ':';
            mailbody += 'Brutopalgad'+lb;
            angular.forEach($scope.generatedEmail.users, function(value, key){
                mailbody += tab + key + semi + sp + value + sp + eur + lb;
            });
            $scope.sendMail("ksild@neti.ee", "Palgaarvestuse kokkuvõte " + $scope.previousMonthLabel + ' ' + $rootScope.thisYear, mailbody);
            $scope.accountingEmailSent = true;
            
        }, function(response) {
            $scope.messages.errorMessage = response.data.message;
        });
    }
    $scope.getAccountingStatus = function() {
        $http.get(settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/accounting/status').then(function(response) { 
            if(response.data.email_sent == 1) {
                $scope.accountingEmailSent = true;
            };
            $scope.statusReceived = true;
        }, function(response) {
            $scope.messages.errorMessage = response.data.message;
            $scope.statusReceived = true;
        });    
    };
    $scope.getAccountingStatus();
    
    $scope.openEmailView = function(id, first, last){
        ModalService.showModal({
            templateUrl: 'wageEmail.html',
            controller: "wageEmail"
        }).then(function(modal) {
            
            $http.get(settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users/' + id + '/email').then(function(response) {
                modal.scope.user = response.data;
                modal.scope.user.name = first + ' ' + last;
                modal.scope.userId = id;
                var monthLabel = $rootScope.previousMonthLabel;
                var yearLabel = $rootScope.thisYear;
                modal.scope.wageEmailTitle = monthLabel.charAt(0).toUpperCase() + monthLabel.substr(1) + ' ' + yearLabel + ' - ' + modal.scope.user.name;
            }, function(response) {
            }).then(function(){
                modal.element.show();  
            });
            modal.close.then(function(result) {
                if(result) { $route.reload() };
            });
        });
    };
    $scope.openAccountantPrompt = function() {
        ModalService.showModal({
            templateUrl: 'accountantPrompt.html',
            controller: "accountantPrompt"
        }).then(function(modal) {
            modal.scope.modalHeader = "Raamatupidaja e-mail"
            modal.scope.modalText = "E-mail juba saadetud, kas soovid uuesti saata?";
            modal.element.show();
            modal.close.then(function(result) {
                if(result) { $scope.openAccountantEmail() };
            });
        });
    }
    $scope.openAccountantEmail = function(){
        ModalService.showModal({
            templateUrl: 'accountantEmail.html',
            controller: "accountantEmail"
        }).then(function(modal) {
            $http.get(settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/accounting/email').then(function(response) {
                if(response.data.message) {
                    $scope.messages.errorMessage = response.data.message
                } else {
                    $scope.generatedEmail = response.data;
                    var userList = $filter('orderBy')($scope.generatedEmail.users, 'name');
                    var fileList = $scope.generatedEmail.files;
                    
                    var mailbody = '';
                    var lb = '%0D%0A'; var comma = ','; var sp = ' '; var tab = '    '; var eur = '€'; var semi = ':';
                    mailbody = '<p>Tere,</p>' +
                        '<p>Saadan eelmise kuu palgad.</p>' +
                        '<div>';
                    for(var user in userList) { 
                        var username = userList[user].name;
                        var vacationList = userList[user].vacations;
                        var salary_base = userList[user].salary_base.toFixed(0); 
                        var salary_bonus = userList[user].salary_bonus.toFixed(0); 
                        var salary_total = userList[user].salary_total.toFixed(0); 
                        var salary_model = userList[user].salary_model;

                        mailbody += '<span>' + username + semi + sp;
                        
                        // numbers
                        if (salary_model === 2) {
                            var rate_hour = userList[user].rate_hour.toFixed(2);
                            var hours = (userList[user].minutes / 60).toFixed(2);
                            mailbody += 'Tunnihind ' + rate_hour + sp + eur + ', töötatud tunde ' + hours; 
                        }
                        else {
                            // "Põhipalk x" on default, "Kogu kuu põhipalk x" if missing some workdays
                            var rate_month = userList[user].rate_month.toFixed(0); 
                            var salary_base_label = vacationList.length ? 'Kogu kuu põhipalk ' : 'Põhipalk ';
                            var about_bonus = (salary_model === 3) ? (comma + ' lisatasu ' + salary_bonus + sp + eur) : '';
                            mailbody += salary_base_label + rate_month + sp + eur + about_bonus; 
                        }
                        
                        var userFile = fileList.find(function(file){ return file.user_id === userList[user].id });
                        if(userList[user].vat_min_since && userFile) { mailbody += comma + sp + 'tulumaksuvaba miinimumi arvestatakse alates ' + $filter('date')(userList[user].vat_min_since, "dd.MM.yyyy") + comma + sp + "avaldus manuses"; }
                        if(userList[user].vacations.length) {
                            for(var vacation in vacationList) {
                                var vacation_type = VACATION_TYPES[vacationList[vacation].type];
                                var vacationFileExists = !!vacationList[vacation].file_path;
                                mailbody += comma + sp + vacation_type.toLowerCase() + sp + $filter('date')(vacationList[vacation].start_date, "dd.MM.yyyy") + '-' + $filter('date')(vacationList[vacation].end_date, "dd.MM.yyyy");     
                                if (vacationFileExists) mailbody += comma + sp + "puhkuseavaldus manuses"
                            }
                        }
                        
                        mailbody += '.</span><br />';
                    }
                    mailbody += '</div>' +
                        '<p>Tervitades,</p>' +
                        '<p>Ander</p>';
                    
                    modal.scope.model.files = fileList;
                    modal.scope.model.email_body = mailbody;
                    modal.element.show();
                }
            }, function(response) {
                $scope.messages.errorMessage = response.data.message;
            });
            
            modal.close.then(function(result) {
                if(result) { $route.reload() };
            });
        });
    };
    
    $scope.sendToAll = function() {
        $scope.loading = true;
        return $http({
            method: "POST",
        	url: settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users/email',
        	headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response){
            $scope.loading = false;
            $route.reload();
        }).catch(function(response){
            $scope.loading = false;
            $scope.messages.errorMessage = response.data.message;
        })
    }
    
    $scope.canSendToAll = function() {
        return !!$scope.getUsersConfirmedCount()
    }
    
    $scope.getUsersConfirmedCount = function() {
        return $scope.users && $scope.users.filter(function(user) {
            return user.confirmed && user.role != "OUTSOURCER" && user.email_sent != 1;
        }).length;
    }
    $scope.getUsersWithoutConfirmedCount = function() {
        return $scope.users && $scope.users.filter(function(user) {
            return !user.confirmed && user.role != "OUTSOURCER";
        }).length;
    }
});