<script type="text/ng-template" id="wages.tpl">
    <div class="container wages">
        <div class="row">
            <main class="col-sm-12 pt-3 mt-2">
                <ng-include src="'breadcrumbs.tpl'"></ng-include>
                <error-message class="mt-4" fadeout="true" ng-if="messages.errorMessage" error="messages.errorMessage"></error-message>
                <h4 class="mt-4 mb-0">{{previousMonthLabel}} {{thisYear}}</h4>
                <div class="row mt-4 fade-element-in" ng-if="users">
                    <div class="col-lg-12 col-md-12 col-sm-12 table-holder">
                        <h5>Töötajad</h5>
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Nimi</th>
                                    <th>Brutokuupalk</th>
                                    <th></th>
                                    <th style="text-align:center;">
                                        <button ng-disabled="loading || !getUsersConfirmedCount()"
                                            ng-class="{ 'loading' : loading, 'btn-primary-grey' : !getUsersConfirmedCount() }" 
                                            class="btn btn-primary btn-custom loader-btn" 
                                            href="" 
                                            ng-click="sendToAll()" >
                                            <i ng-if="loading" class="fa fa-circle-o-notch fa-spin"></i>
                                            <span>Saada kõigile ({{getUsersConfirmedCount()}})</span>
                                            <!--<span ng-if="!canSendToAll() && (getUsersWithoutConfirmedCount() != getUsersWithoutSentEmailCount())">Kinnita enne palgad</span>
                                            <span ng-if="!canSendToAll() && (getUsersWithoutConfirmedCount() == getUsersWithoutSentEmailCount())">Kõigile saadetud</span>-->
                                        </button>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="user in users | orderBy: 'first_name' | filter: { 'role' : '!' + outSourcer }">
                                    <td>{{user.first_name}} {{user.last_name}}</td>
                                    <td ng-if="!user.salary">arvutamata</td>
                                    <td ng-if="user.salary">{{user.salary | number: 0}} €</td>
                                    <td class="text-center">
                                        <a ng-class="{ 'btn-primary-blue' : user.confirmed == 0, 'btn-primary' : user.confirmed == 1 }" ng-href="wages/{{user.id}}" class="btn">
                                            <span ng-if="user.confirmed != 1">Vaata arvutust</span>
                                            <span ng-if="user.confirmed == 1">Palk kinnitatud</span>
                                        </a>	
                                    </td>
                                    <td class="text-center">
                                        <button ng-disabled="user.confirmed == 0" ng-class="{ 'btn-primary' : user.email_sent == 1, 'btn-primary-grey' : user.email_sent != 1 }" class="btn" href="" ng-click="openEmailView(user.id, user.first_name, user.last_name)" >
                                            <span ng-if="user.email_sent != 1">Saada e-mail</span>
                                            <span ng-if="user.email_sent == 1">E-mail saadetud</span>
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div> <!--col-12-->
                    
                    <div class="col-lg-12 col-md-12 col-sm-12 table-holder mt-4">
                        <h5>Allhankijad</h5>
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="33%">Nimi</th>
                                    <th width="33%">Kulud</th>
                                    <th width="35%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="user in users | orderBy: 'first_name' | filter: { 'role' : outSourcer } as outSourcerList">
                                    <td>{{user.first_name}} {{user.last_name}}</td>
                                    <td ng-if="user.confirmed != 1">arvutamata</td>
                                    <td ng-if="user.confirmed == 1">{{user.salary | number: 0}} €</td>
                                    <td class="text-center">
                                        <a ng-class="{ 'btn-primary-blue' : user.confirmed == 0, 'btn-primary' : user.confirmed == 1 }" ng-href="wages/{{user.id}}" class="btn">
                                            <span ng-if="user.confirmed != 1">Lisa kulud</span>
                                            <span ng-if="user.confirmed == 1">Kulud lisatud</span>
                                        </a>	
                                    </td>
                                </tr>
                                <tr ng-if="!outSourcerList.length">
                                    <td colspan=100>Puuduvad kirjed</td>
                                </tr>
                            </tbody>
                        </table>
                    </div> <!--col-12-->
                    
                    <div class="col-lg-12 col-md-12 col-sm-12 mt-4" ng-if="statusReceived">
                        <div class="bordered-elem p-3">
                            <h5 class="form-control-label mb-3">Saada e-mail raamatupidajale.</h5>
                            <div class="mt-3">
                                <a ng-if="!accountingEmailSent" href="" ng-click="openAccountantEmail()" class="btn-warning btn">
                                    <span>Saada e-mail</span>
                                </a>
                                <a ng-if="accountingEmailSent" href="" ng-click="openAccountantPrompt()" class="btn-primary btn">
                                    <span>Saadetud</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div><!--inside row-->
                <div ng-if="!users && !wagesNotFound" class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </main><!--col9-->
        </div><!--row-->
        
    </div><!--/container-->
</script>