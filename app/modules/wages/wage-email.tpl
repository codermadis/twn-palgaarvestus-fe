<script type="text/ng-template" id="wage-email.tpl">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{wageEmailTitle}}</h4>
                <button type="button" class="close" ng-click="close(false)" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
            	<div class="row">
                    <div class="form-group col-md-12">
                        <error-message fadeout=true ng-if="messages.errorMessage" error="messages.errorMessage"></error-message>
                        <toalert-message ng-if="messages.alertMessage" toalert="messages.alertMessage"></toalert-message>
                    </div>
                </div>
				<div class="row xs-none">
					<div class="col-md-3 bold">Projekt</div>
					<div class="col-md-3 bold">Töötunnid</div>
					<div class="col-md-3 bold">Tunnihind</div>
					<div class="col-md-3 bold">Sissetoodud raha</div>
				</div> 
				<hr>
				<div class="row" ng-repeat="project in user.projects">
					<div class="col-6 xs-show bold">Projekt</div>
					<div class="col-md-3 col-6">{{project.name}}</div>
					<div class="col-6 xs-show bold">Töötunnid</div>
					<div class="col-md-3 col-6">{{project.hours}}</div>
					<div class="col-6 xs-show bold">Tunnihind</div>
					<div class="col-md-3 col-6">{{project.rate_salary | number}} €</div>
					<div class="col-6 xs-show bold">Sissetoodud raha</div>
					<div class="col-md-3 col-6">{{project.revenue | number}} €</div>
					<hr ng-if="!$last">
				</div>
				<hr>
				<div class="row">
					<div class="col-md-3 col-6 pull-right bold">Töötunnid kokku</div>
					<div class="col-md-3 col-6">{{user.hours}}</div>
					<div class="col-md-3 col-6 bold">Sissetoodud raha kokku</div>
					<div class="col-md-3 col-6">{{user.revenue | number}} €</div>
				</div>
				<div class="spacing"></div>
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<div class="bold">Palgamudel</div>
						<span>Brutopalk : </span>
						<span class="bold">{{user.salary_model.name}}</span>
						<span> ({{user.salary_model.equation}})</span>
					</div>
				</div>
				<div class="spacing"></div>
				<div class="row">
					<div class="col-md-6 col-sm-6">
						<span class="bold">Tööpäevi kuus kokku: </span>
						<span>{{user.workdays | number }}</span>
					</div>
					<div class="col-md-6 col-sm-6">
						<span class="bold">Töötatud tööpäevi kuus: </span>
						<span>{{user.worked_days | number }}</span>
					</div>
				</div>
				<div class="spacing"></div>
				<div class="row" ng-if="user.salary_model.salary_model == 1">
					<div class="col-md-6 col-sm-12">
						<div class="bold">Põhipalk</div>
						<div>{{user.salary_base | number }} €</div>
					</div>
					<div class="col-md-6 col-sm-12">
						<div class="spacing xs-show"></div>
						<div class="bold modal-bold-text-color">BRUTOPALK</div>
						<div class="bold modal-bold-text-color">{{user.salary | number }} €</div>
					</div>
				</div>
				<div class="row" ng-if="user.salary_model.salary_model == 2">
					<div class="col-md-3 col-sm-12">
						<div class="bold">Tunnid</div>
						<div>{{user.hours }}</div>
					</div>
					<div class="col-md-3 col-sm-12">
						<div class="bold">Tunnihind</div>
						<div>{{user.salary_model.rate_hour | number }} €</div>
					</div>
					<div class="col-md-3 col-sm-12">
						<div class="spacing xs-show"></div>
						<div class="bold modal-bold-text-color">BRUTOPALK</div>
						<div class="bold modal-bold-text-color">{{user.salary | number }} €</div>
					</div>
				</div>
				<div class="row" ng-if="user.salary_model.salary_model == 3">
					<div class="col-md-3 col-sm-12">
						<div class="bold">Põhipalk</div>
						<div>{{user.salary_base | number }} €</div>
					</div>
					<div class="col-md-3 col-sm-12">
						<div class="bold">Lisatasu piir</div>
						<div>{{user.bonus_threshold | number }} €</div>
					</div>
					<div class="col-md-3 col-sm-12">
						<div class="bold">Koefitsient</div>
						<div>{{user.salary_model.coefficient | number }}</div>
					</div>
					<div class="col-md-3 col-sm-12">
						<div class="spacing xs-show"></div>
						<div class="bold modal-bold-text-color">BRUTOPALK</div>
						<div class="bold modal-bold-text-color">{{user.salary | number }} €</div>
					</div>
				</div>
				<div class="spacing"></div>
				<div class="row" ng-if="user.salary_model.salary_model == 3">
					<div class="col-md-3 col-sm-12">
					</div>
					<div class="col-md-3 col-sm-12">
					</div>
					<div class="col-md-3 col-sm-12">
						<div class="bold">Sh lisatasu</div>
					</div>
					<div class="col-md-3 col-sm-12">
						<div class="bold modal-bold-text-color">{{user.salary_bonus | number }} €</div>
					</div>
				</div>
				<div class="spacing"></div>
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<span class="bold">Netopalga kalkulaator</span>
					</div>
					<div class="col-md-12 col-sm-12">
						<a href="http://palk.crew.ee/" target="_blank">http://palk.crew.ee/</a>
					</div>
				</div>
				<div class="spacing"></div>
				<div class="row" ng-if="user.comment">
					<div class="col-md-12 col-sm-12">
						<span class="bold">Märkus</span>
					</div>
					<div class="col-md-12 col-sm-12">
						<span>{{user.comment}}</span>
					</div>
				</div>
            </div>
            <div class="modal-footer" ng-if="!hideActions">
                <button type="button" ng-click="goToUser()" class="btn btn-warning" data-dismiss="modal">Muuda</button>
                <button ng-disabled="disableSend" type="button" ng-click="sendEmail()" class="btn btn-primary" data-dismiss="modal">Saada</button>
            </div>
        </div>
    </div>
</script>