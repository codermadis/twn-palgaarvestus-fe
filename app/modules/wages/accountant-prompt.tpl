<script type="text/ng-template" id="accountant-prompt.tpl">
    <div class="modal-dialog modal-import">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{modalHeader}}</h4>
                <button ng-disabled="loading" type="button" class="close" ng-click="close(false)" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <div class="spacing"></div>
                <div>{{modalText}}</div>
                <div class="spacing"></div>
			</div>
            <div class="modal-footer">
                <button ng-disabled="loading" type="button" ng-click="close(false)" class="btn btn-warning" data-dismiss="modal">Katkesta</button>
                <button ng-disabled="loading" type="button" ng-click="close(true)" class="btn btn-primary" data-dismiss="modal">Saada uuesti</button>
            </div>
        </div>
    </div>
</script>