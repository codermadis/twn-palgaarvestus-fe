App.controller('login', function($scope, $http, $rootScope, $routeParams, $location, $timeout, $cookies, $crypto) {
	
	var expireDate = new Date();
	expireDate.setDate(expireDate.getDate() + 180);
	var cookieOptions = {
		expires: expireDate
	}
	
	if($cookies.get('rememberMe')) {
		$scope.remembered = true;
		$scope.username = JSON.parse($cookies.get('username'));
		$scope.password = $crypto.decrypt(JSON.parse($cookies.get('password')));
	} else {
		$scope.remembered = false;
	}
	
	$rootScope.hideNavbar = true;
	$scope.newPassword = false;
	$scope.messages = {
		errorMessage: false,
		alertMessage: false
	};
	$scope.$watch('messages.errorMessage', function(newValue, oldValue) {
	    if(newValue != oldValue) {
	        $timeout(function () {
                $scope.messages.errorMessage = false;
            }, 2500);      
	    }
	});
	$scope.$watch('messages.alertMessage', function(newValue, oldValue) {
	    if(newValue != oldValue) {
	        $timeout(function () {
                $scope.messages.alertMessage = false;
            }, 2500);      
	    }
	});
	$scope.forgotPassword = false;
	$scope.openEmailForm = function() {
		$scope.forgotPassword = !$scope.forgotPassword;
	};
	$scope.loginSubmit = function(username, password, remembered){
		$http({
			method: "POST",
			url: settings.prefix+"login",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: {
				email: username,
				password: password
			},
    		transformRequest: function(obj) {
	            var str = [];
	            for(var p in obj)
	               str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
	            return str.join("&");
    		}
		}).then(function( response ){
			if(response.data.success) { 
				if(remembered) {
					$cookies.putObject('rememberMe', true, cookieOptions);
					$cookies.putObject('username', username, cookieOptions);
					$cookies.putObject('password', $crypto.encrypt(password), cookieOptions);
				} else {
					$cookies.remove('rememberMe');
					$cookies.remove('username');
					$cookies.remove('password');
				}
		
				$rootScope.login = true; 
				sessionStorage.setItem('loggedIn', true);
				sessionStorage.setItem('user', username);
				sessionStorage.setItem('role', response.data.role );
				$location.path('/');
			} else {
				$scope.messages.errorMessage = response.data.message;
			}
			
		}, function(response){
			if(response.data.message) {
				$scope.messages.errorMessage = response.data.message;
			} else { $scope.messages.errorMessage = "Logimine ebaõnnestus" }
		});
	}
	$scope.sendRecoveryEmail = function(input){
		$http({
			method: "POST",
			url: settings.prefix+"recovery",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: {
				email: input,
			},
    		transformRequest: function(obj) {
	            var str = [];
	            for(var p in obj)
	               str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
	            return str.join("&");
    		}
		}).then(function( response ){
			if(response.data.success) { 
				$scope.messages.alertMessage = response.data.message;
			} else {
				$scope.messages.errorMessage = response.data.message;
			}
		}, function(response){
			if(response.data.message) {
				$scope.messages.errorMessage = response.data.message;
			} else { $scope.messages.errorMessage = "Logimine ebaõnnestus" }
		});
	};
})