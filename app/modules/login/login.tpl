<script id="login.tpl" type="text/ng-template">
	<div class="container" ng-if="locationValiditySet">
		<div ng-if="!locationAllowed" class="login text-center disallowed-error animate-if">
			<div class="login-logo">
				<img src="/assets/img/logo.svg" width="300" height="120" alt="logo">
			</div>
			<h2>Juurdepääs piiratud</h2>
		</div>
		<section class="container login__container text-center animate-if" ng-if="locationAllowed">
			<div class="login-logo">
				<img src="/assets/img/logo.svg" width="370" height="120" alt="logo">
			</div>
			<div class="login">
            	<h2><span id="formHeading">Palgaarvestus ja statistika</span></h2>
            	<!--Login form-->
	            <form id="login_form" name="login_form" ng-if="!forgotPassword">
					<p ng-if="messages.errorMessage" class="well text-danger" id="login_validation_errors">{{messages.errorMessage}}</p>
					<p ng-if="messages.alertMessage" class="well item-update" id="login_validation_errors">{{messages.alertMessage}}</p>
					<p ng-if="!messages.errorMessage && !messages.alertMessage" class="well text-danger" id="login_validation_errors"></p>
					<div>
						<label class="login__input">
							<span>E-mail:</span>
							<input id="login_email" class="form-control" ng-model="username" type="email" name="login_email" value="">
		            	</label>
	            	</div>
	            	<div>
						<label class="login__input">
							<span>Salasõna:</span>
							<input id="login_password" class="form-control" ng-model="password" type="password" name="login_password" value="">
						</label>
					</div>
					<div class="login__button text-right">
						<a href="" ng-click="openEmailForm()">Unustasid salasõna?</a>
					</div>
					<div class="login__button text-left">
						<label class="login__checkbox">
							<input type="checkbox" name="login_remember_checkbox" ng-model="remembered" id="login_remember_checkbox" > Jäta mind meelde
						</label>
					</div>
					<div>
						<a href="" ng-click="loginSubmit(username, password, remembered)" class="btn btn-primary">Logi sisse</a>
					</div>
	            </form>
	            <form id="enter_email_form" name="enter_email_form" ng-if="forgotPassword">
					<h5>Sisesta e-mail uue salasõna saamiseks</h5>
					<p ng-if="messages.errorMessage" class="well text-danger" id="login_validation_errors">{{messages.errorMessage}}</p>
					<p ng-if="messages.alertMessage" class="well item-update" id="login_validation_errors">{{messages.alertMessage}}</p>
					<p ng-if="!messages.errorMessage && !messages.alertMessage" class="well text-danger" id="login_validation_errors"></p>
					<div>
						<div class="login__input email__input">
							<input id="email" class="form-control" ng-model="set_email" type="email" name="email" value="">
		            	</div>
	            	</div>
					<div class="login__button">
						<a href="" ng-click="sendRecoveryEmail(set_email)" class="btn btn-primary btn-custom">Saada uus salasõna</a>
					</div>
					<div>
						<a href="" ng-click="openEmailForm()">LOGI SISSE</a>
					</div>
	            </form>
			</div> 
		</section>
	</div><!--/container-->
</script>