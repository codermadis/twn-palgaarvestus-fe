App.controller('holidays', function($scope, $http, $route, $rootScope, $location, ModalService, $filter){
    
    var VACATION_TYPES = {
        'vacation': 'Puhkus',
        'unpaid': 'Palgata puhkus',
        'study': 'Õppepuhkus',
        'disability': 'Töövõimetus',
        'child': 'Lapsepuhkus',
        'other': 'Muu'
    };
    
    $scope.messages = { alertMessage: false, myMessage: false };
    $scope.firstActive = true;
	$scope.secondActive = false;
	$scope.regularLimit = 20;
	$scope.activeLimit = 20;
    
    $scope.upTheRegularLimit = function() {
	    $scope.regularLimit += 20;
	}
	$scope.upTheActiveLimit = function() {
	    $scope.activeLimit += 20;
	}
	
	$scope.getVacations = function() {
	    $scope.vacations = false
	    $scope.active_vacations = false
        $http.get(settings.prefix + 'vacations').then(function(response) {
            $scope.vacations = response.data.map(function(elem) {
                elem.type = VACATION_TYPES[elem.type];
                return elem;
            });
            $scope.active_vacations = $scope.vacations.filter(function(elem){return elem.active});
            $scope.previousMonthBeginning = $rootScope.finalPreviousMonth+ '-01';
            $scope.now = $filter('date')(new Date($scope.previousMonthBeginning), 'yyyy-MM-dd');
        }, function(response) {
            $scope.messages.errorMessage = response.data.message;
        });
    };
    $scope.getVacations();
    
    $scope.reloadThis = function() {
        $route.reload()
    }
    
    $scope.openHolidayAdd = function(){
        ModalService.showModal({
            templateUrl: 'addHoliday.html',
            controller: "addHoliday"
        }).then(function(modal) {
            modal.scope.modalHeader = "Eemalviibimise lisamine";
            modal.element.show();  
            modal.close.then(function(result) {
                if(result) { 
                    $scope.firstActive = false;
	                $scope.secondActive = true;
	                $scope.getVacations(); 
                };
            });
        });
    };
    $scope.openHolidayView = function(id, first_name, last_name){
        ModalService.showModal({
            templateUrl: 'viewHoliday.html',
            controller: "addHoliday"
        }).then(function(modal) {
            modal.scope.modalHeader = "Eemalviibimine";
            modal.scope.userName = first_name + ' ' + last_name;
            $http.get(settings.prefix+ 'vacations/' + id).then(function(response){
                modal.scope.vacationData = response.data;
                modal.scope.vacationData.start_date = new Date(modal.scope.vacationData.start_date);
                modal.scope.vacationData.end_date = new Date(modal.scope.vacationData.end_date);
                modal.scope.type = VACATION_TYPES[modal.scope.vacationData.type];
                modal.scope.editView = true;
                modal.scope.editVacationId = id;
                modal.element.show();  
            }, function(response) {
                console.log(response);
            });
            modal.close.then(function(result) {
                if(result) {
                    $scope.firstActive = false;
	                $scope.secondActive = true;
	                $scope.getVacations(); 
                };
            });
        });
    };
})