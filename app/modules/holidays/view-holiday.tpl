<script type="text/ng-template" id="view-holiday.tpl">
    <div class="modal-dialog modal-user">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{modalHeader}}</h4>
                <button type="button" class="close" ng-click="close(false)" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form name="holidayForm" id="holidayForm">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <error-message fadeout=true ng-if="messages.errorMessage" error="messages.errorMessage"></error-message>
                            <toalert-message ng-if="messages.alertMessage" toalert="messages.alertMessage"></toalert-message>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-6">
                            <label for="date">Töötaja: {{userName}}</label>
                        </div>
    				</div>
    				<div class="row mb-2">
                        <div class="col-md-6">
                            <label for="date">Tüüp: {{type}}</label>
                        </div>
    				</div>
    			    <div class="row" ng-repeat="field in dateFields">
    				    <div class="col-md-6 col-12">
    				        <label for="date">Alguskuupäev<span class="user-alert-text">*</span></label>
                            <p class="input-group">
                                <input type="text" ng-change="setViewEndDate($index)" ng-model-options="{ debounce: 500 }" ng-class="{ 'border-error' : datepickerFirstError }" class="form-control datepicker-element-1" readonly="true" ng-model="vacationData.start_date" uib-datepicker-popup="dd-MM-yyyy" is-open="popup_start[$index]" datepicker-options="dateOptions" close-text="Sulge" current-text="Täna" clear-text="Kustuta" required />
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default" ng-click="openStart($index)"><i class="fa fa-calendar"></i></button>
                                </span>
                            </p>
                        </div>
                        <div class="col-md-6 col-12">
    				        <label for="date">Lõppkuupäev<span class="user-alert-text">*</span></label>
                            <p class="input-group">
                                <input type="text" ng-class="{ 'border-error' : datepickerSecondError }" class="form-control datepicker-element-2" readonly="true" ng-model="vacationData.end_date" uib-datepicker-popup="dd-MM-yyyy" is-open="popup_end[$index]" datepicker-options="dateOptions" close-text="Sulge" current-text="Täna" clear-text="Kustuta" required />
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default" ng-click="openEnd($index)"><i class="fa fa-calendar"></i></button>
                                </span>
                            </p>
                        </div>
    			    </div>
    			    <div class="row">
    				    <div class="col-md-12 col-12">
    				        <label for="date">Puhkuseavaldus</label>
    				        <div class="bordered-elem radius padded">
                                <div class="spacing"></div>
                                    <div class="mb-3" ng-if="vacationData.originalname">
                                        <a ng-click="downloadDocument(vacationData.id)"><i class="fa fa-download fa-2x"></i></a> <span class="file-name">{{vacationData.originalname}}</span>
                                    </div>
                                    <a class="soft-italic" ng-click="replaceFile = !replaceFile">
                                        <span ng-if="vacationData.originalname">Asenda fail</span>
                                        <span ng-if="!vacationData.originalname">Lisa fail</span>
                                    </a>
                                    <div class="replace-file animate-hide mt-3" ng-hide="!replaceFile">
                                        <input class="file-input" type="file" ngf-select ng-model="uploadedFile" name="file" ngf-max-size="5MB" ngf-model-invalid="errorFile">
                                        <a href="" ng-click="uploadedFile = null" ng-show="uploadedFile"><i class="fa fa-close address-close user-alert-text"></i></a>
                                        <div><i ng-show="holidayForm.file.$error.maxSize">Fail liiga suur {{errorFile.size / 1000000|number:1}}MB: max 5MB</i></div>
                                    </div>
                                <div class="spacing"></div>
                            </div>
                        </div>
    			    </div>
    			</div>
                <div class="modal-footer">
    				<div class="col-md-12 col-sm-12 right-align">
                        <button type="button" ng-click="close(false)" class="btn btn-warning mt-2 mb-2" data-dismiss="modal">Tagasi</button>
                        <button type="button" ng-click="deleteHoliday(vacationData.id)" class="btn btn-custom-red mt-2 mb-2" data-dismiss="modal">Kustuta</button>
                        <button type="submit" ng-click="updateHoliday(holidayForm, uploadedFile)" class="btn btn-primary mt-2 mb-2" data-dismiss="modal">Salvesta</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</script>