<script type="text/ng-template" id="add-holiday.tpl">
    <div class="modal-dialog modal-user">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{modalHeader}}</h4>
                <button type="button" class="close" ng-click="close(false)" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form name="holidayForm" id="holidayForm">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <error-message ng-if="messages.errorMessage" fadeout=true error="messages.errorMessage"></error-message>
                            <toalert-message ng-if="messages.alertMessage" toalert="messages.alertMessage"></toalert-message>
                        </div>
                    </div>
                    <div class="row auto-row">
                        <div class="col-md-6 auto-complete">
                            <label for="date">Vali töötaja<span class="user-alert-text">*</span></label>
                            <input type="text" ng-model="autoModel.user" placeholder="" uib-typeahead="item as item.first_name + ' ' + item.last_name for item in getUser($viewValue)" class="form-control" required>
                            <div ng-show="noResults">
                              <i class="glyphicon glyphicon-remove"></i> No Results Found
                            </div>
                        </div>
    				</div>
    				<div class="row auto-row">
                        <div class="col-md-6 auto-complete">
                            <label>Vali tüüp<span class="user-alert-text">*</span></label>
                            
                            <select name="type" ng-model="model.type" class="form-control" required>
                              <option value="vacation">Puhkus</option>
                              <option value="unpaid">Palgata puhkus</option>
                              <option value="study">Õppepuhkus</option>
                              <option value="disability">Töövõimetus</option>
                              <option value="child">Lapsepuhkus</option>
                              <option value="other">Muu</option>
                            </select>
                        </div>
    				</div>
    			    <div class="row" ng-repeat="field in dateFields">
    				    <div class="col-md-6 col-12">
    				        <label for="date">Alguskuupäev<span class="user-alert-text">*</span></label>
                            <p class="input-group">
                                <input ng-change="setEndDate($index)" ng-model-options="{ debounce: 500 }" type="text" ng-class="{ 'border-error' : datepickerFirstError }" class="form-control datepicker-element-1" readonly="true" ng-model="model.start[$index]" uib-datepicker-popup="dd-MM-yyyy" is-open="popup_start[$index]" datepicker-options="dateOptions" close-text="Sulge" current-text="Täna" clear-text="Kustuta" required />
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default" ng-click="openStart($index)"><i class="fa fa-calendar"></i></button>
                                </span>
                            </p>
                        </div>
                        <div class="col-md-6 col-12">
    				        <label for="date">Lõppkuupäev<span class="user-alert-text">*</span></label>
                            <p class="input-group">
                                <input type="text" ng-class="{ 'border-error' : datepickerSecondError }" class="form-control datepicker-element-2" readonly="true" ng-model="model.end[$index]" uib-datepicker-popup="dd-MM-yyyy" is-open="popup_end[$index]" datepicker-options="dateOptions" close-text="Sulge" current-text="Täna" clear-text="Kustuta" required />
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default" ng-click="openEnd($index)"><i class="fa fa-calendar"></i></button>
                                </span>
                            </p>
                        </div>
    			    </div>
    			    <div class="row mb-2" ng-if="false">
    				    <div class="col-md-12">
        					<a href="" ng-click="addDateField()">
        					    <i class="fa fa-plus"></i>
        					    <span>Lisa kuupäevad</span>
        					</a>
        				</div>
    				</div>
    			    <div class="row">
    				    <div class="col-md-12 col-12">
    				        <label for="date">Lae üles puhkuseavaldus</label>
    				        <div class="bordered-elem radius padded">
                                <div class="spacing"></div>
                                    <p class="soft-italic">Lae fail</p>
                                    <input class="file-input" type="file" ngf-select ng-model="uploadedFile" name="file" ngf-max-size="5MB" ngf-model-invalid="errorFile">
                                    <a href="" ng-click="uploadedFile = null" ng-show="uploadedFile"><i class="fa fa-close address-close user-alert-text"></i></a>
                                    <div><i ng-show="holidayForm.file.$error.maxSize">Fail liiga suur {{errorFile.size / 1000000|number:1}}MB: max 5MB</i></div>
                                <div class="spacing"></div>
                            </div>
                        </div>
    			    </div>
    			</div>
                <div class="modal-footer">
    				<div class="col-md-12 col-sm-12 right-align">
                        <button type="button" ng-click="close(false)" class="btn btn-warning mt-2 mb-2" data-dismiss="modal">Katkesta</button>
                        <button type="submit" ng-click="saveHoliday(holidayForm, uploadedFile)" class="btn btn-primary mt-2 mb-2" data-dismiss="modal">Lisa</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</script>