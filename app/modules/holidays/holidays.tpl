<script type="text/ng-template" id="holidays.tpl">
    <div class="container animate-if">
        <div class="row">
            <main class="col-sm-12 pt-3 mb-4 mt-2">
				<ng-include src="'breadcrumbs.tpl'"></ng-include>
				<error-message class="mt-4" ng-if="messages.errorMessage" error="messages.errorMessage" fadeout=true></error-message>
                <div class="row mt-4" ng-if="vacations">
                    <div class="col-12">
                        <div class="bordered-elem p-3">
                            <h5 class="form-control-label mb-3">Lisa eemalviibimine</h5>
                            <div class="mt-3">
                                <a href="" ng-click="openHolidayAdd()" class="btn btn-primary">Lisa</a>
                            </div>
                        </div>
                    </div>
                </div>
				<ul class="nav nav-tabs mt-4">
				  <li class="nav-item">
				    <a ng-class="{ 'active' : firstActive }" ng-click="firstActive = true; secondActive = false;" class="nav-link" href="">Aktiivsed eemalviibimised</a>
				  </li>
				  <li class="nav-item">
				    <a ng-class="{ 'active' : secondActive }" ng-click="firstActive = false; secondActive = true;" class="nav-link" href="">Kõik eemalviibimised</a>
				  </li>
				</ul>
                <div class="row table-holder">
                    <div class="col-md-12 col-sm-12">
						<table class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr>
                                    <th style="width:20%">Nimi</th>
                                    <th style="width:20%">Tüüp</th>
                                    <th style="width:20%">Alates</th>
                                    <th style="width:20%">Kuni</th>
                                    <th style="width:20%"></th>
                                </tr>
                            </thead>
                            <tbody class="fade-element-in" ng-if="active_vacations && firstActive">
                                <tr ng-repeat="vacation in active_vacations | orderBy: '-start_date' as activeVacations" ng-if="vacation.end_date > now">
                                    <td>{{vacation.first_name}} {{vacation.last_name}}</td>
                                    <td>{{vacation.type}}</td>
                                    <td>{{vacation.start_date | date: 'd MMM, y' }}</td>
                                    <td>{{vacation.end_date | date: 'd MMM, y' }}</td>
                                    <td><a href="" class="btn btn-primary light-green" ng-click="openHolidayView(vacation.id, vacation.first_name, vacation.last_name)">Vaata</a></td>
                                </tr>
                                <tr ng-if="!activeVacations.length">
                                    <td colspan=100>Kirjed puuduvad</td>
                                </tr>
                            </tbody>
                            <tbody class="fade-element-in" ng-if="vacations && secondActive">
                                <tr ng-repeat="vacation in vacations | orderBy: '-start_date' | orderBy: 'first_name'">
                                    <td>{{vacation.first_name}} {{vacation.last_name}}</td>
                                    <td>{{vacation.type}}</td>
                                    <td>{{vacation.start_date | date: 'd MMM, y' }}</td>
                                    <td>{{vacation.end_date | date: 'd MMM, y' }}</td>
                                    <td><a href="" class="btn btn-primary light-green" ng-click="openHolidayView(vacation.id, vacation.first_name, vacation.last_name)">Vaata</a></td>
                                </tr>
                                <tr ng-if="!vacations.length">
                                    <td colspan=100>Kirjed puuduvad</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row" ng-if="vacations && secondActive" ng-hide="regularLimit > vacations.length">
					<div class="col-12 col-md-12 text-right mt-3">
						<a href="" ng-click="upTheRegularLimit()">Näita rohkem</a>
					</div><!--/col-12 col-md-12-->
				</div><!--/row-->
				<div class="row" ng-if="active_vacations && firstActive" ng-hide="activeLimit > active_vacations.length">
					<div class="col-12 col-md-12 text-right mt-3">
						<a href="" ng-click="upTheActiveLimit()">Näita rohkem</a>
					</div><!--/col-12 col-md-12-->
				</div><!--/row-->
                <div ng-if="!vacations" class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </main><!--col9-->
        </div><!--row-->
    </div><!--/container-->
</script>