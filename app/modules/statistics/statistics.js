App.controller('statistics', function($scope, $http, $rootScope, $routeParams, $location, $filter){
    $scope.messages = { alertMessage: false, errorMessage: false };
    $scope.enableMonthRevenueTable = false;
    $scope.enableMonthHourTable = false;
    $scope.enableYearRevenueTable = false;
    $scope.enableYearHourTable = false;
    $scope.yearLoader = true;
    $scope.monthLoader = true;
    $scope.monthValues = [];
    $scope.selectedGeneralItem = 1;
    if(!$scope.statisticSelectedMonth) { $scope.statisticSelectedMonth = $rootScope.finalPreviousMonth; }   
    if(!$scope.yearStatisticSelectedMonth) { $scope.yearStatisticSelectedMonth = $rootScope.finalPreviousMonth; }   
    
    $scope.sortBySum = function(arr) {
	    var resultArr = arr.sort(function(first, second) {
	        return (parseInt(first.minutes_client + first.minutes_internal, 10) - parseInt(second.minutes_client + second.minutes_internal, 10));
	    });
	    return resultArr.reverse();
	};
    
    $scope.setTab = function(modifier) {
        $scope.enableMonthRevenueTable = false;
        $scope.enableMonthHourTable = false;
        $scope.enableYearRevenueTable = false;
        $scope.enableYearHourTable = false;
        if(modifier) { $scope.firstActive = true; $scope.secondActive = false; } else { $scope.firstActive = false; $scope.secondActive = true; }
        if(modifier) { $scope.getMonthStatistics($scope.statisticSelectedMonth); } else { $scope.getYearStatistics($scope.yearStatisticSelectedMonth); }
    }

    $scope.setMonthRevenueTableVisible = function() {
        $scope.enableMonthRevenueTable = !$scope.enableMonthRevenueTable;
    }
    $scope.setMonthHourTableVisible = function() {
        $scope.enableMonthHourTable = !$scope.enableMonthHourTable;
    }
    $scope.setYearRevenueTableVisible = function() {
        $scope.enableYearRevenueTable = !$scope.enableYearRevenueTable;
    }
    $scope.setYearHourTableVisible = function() {
        $scope.enableYearHourTable = !$scope.enableYearHourTable;
    }
    $scope.setGeneralItem = function(nr) {
        $scope.selectedGeneralItem = nr;
        $scope.yearGeneralRows = [];
        
        var monthList = $filter('orderBy')($scope.yearStatistics.months, 'month_of_year');
        if($scope.selectedGeneralItem == 1) { var activeGeneralItem = 'avg_hour_rate'; $scope.activeGeneralItemLabel = "Tegelik billitava töö tunnihind" }
        if($scope.selectedGeneralItem == 2) { activeGeneralItem = 'gross_margin'; $scope.activeGeneralItemLabel = "Brutomarginaal"  }
        if($scope.selectedGeneralItem == 3) { activeGeneralItem = 'total_billing_rate'; $scope.activeGeneralItemLabel = "Billing rate (sh siseprojektid)"  }
        if($scope.selectedGeneralItem == 4) { activeGeneralItem = 'client_billing_rate'; $scope.activeGeneralItemLabel = "Billing rate (siseprojektideta)"  }
        for(var month in monthList) {
            $scope.yearGeneralRows.push({ 'c': [
                { 'v': $rootScope.monthLabels[monthList[month].month_of_year - 1] },
                { 'v': monthList[month][activeGeneralItem] },
            ]});
        }
        $scope.drawYearGeneralChart();
    }
    
    var startYear = 2017;
    var startMonth = 1;
    for(var i = startYear; i <= $rootScope.thisYearNum; i++) {
        if(i == $rootScope.thisYearNum) { var endMonth = $rootScope.prevMonthNum; startMonth = 1 } else { endMonth = 12 }
        for(var j = startMonth; j <= endMonth; j++) {
            if(j < 10) { var monthVal = j.toString(); monthVal = '0' + monthVal } else { monthVal = j };
            var monthHash = {};
            monthHash['label'] = $rootScope.monthLabels[j-1] + ' ' + i;
            monthHash['name'] = i + '-' + monthVal;    
            $scope.monthValues.push(monthHash);
        }
    }
    $scope.monthValues = $scope.monthValues.reverse();
    
	$scope.firstActive = true;
	$scope.secondActive = false;
	
	$scope.getMonthStatistics = function(month) {
        if(month) {
            $scope.enableMonthRevenueTable = false;
            $scope.enableMonthHourTable = false;
            $scope.enableYearRevenueTable = false;
            $scope.enableYearHourTable = false;
            $scope.messages.errorMessage = false;
            $scope.monthRevenueChart = false;
            $scope.monthHourChart = false;
            $scope.monthStatistics = false;
            $scope.monthLoader = true;
            $http.get(settings.prefix+ 'months/' + month + '/statistics').then(function(response) {
                $scope.monthStatistics = response.data;
                var userListByRevenue = $filter('orderBy')($scope.monthStatistics.users, '-revenue');
                var userListByClientProjects = $scope.sortBySum($scope.monthStatistics.users);
                $scope.monthStatistics.minutes_client_project_floored = Math.floor($scope.monthStatistics.minutes_client_project/60);
                $scope.monthStatistics.minutes_client_project_remainder = $scope.monthStatistics.minutes_client_project%60;
                $scope.monthStatistics.minutes_internal_project_floored = Math.floor($scope.monthStatistics.minutes_internal_project/60);
                $scope.monthStatistics.minutes_internal_project_remainder = $scope.monthStatistics.minutes_internal_project%60;
                $scope.monthRevenueRows = [];
                $scope.monthHourRows = [];
                for(var user in userListByRevenue) {
                    $scope.monthRevenueRows.push({ 'c': [
                        { 'v': userListByRevenue[user].first_name + ' ' + userListByRevenue[user].last_name },
                        { 'v': parseInt(userListByRevenue[user].revenue) },
                        { 'v': parseInt(userListByRevenue[user].expense) },
                    ]});
                }
                for(var user in userListByClientProjects) {
                    $scope.monthHourRows.push({ 'c': [
                        { 'v': userListByClientProjects[user].first_name + ' ' + userListByClientProjects[user].last_name },
                        { 'v': userListByClientProjects[user].minutes_client/60, f: Math.floor(userListByClientProjects[user].minutes_client/60) + 'h ' + (userListByClientProjects[user].minutes_client%60) + 'min'},
                        { 'v': userListByClientProjects[user].minutes_internal/60, f: Math.floor(userListByClientProjects[user].minutes_internal/60) + 'h ' + (userListByClientProjects[user].minutes_internal%60) + 'min'},
                    ]});
                }
                $scope.drawMonthRevenueChart();
                $scope.drawMonthHourChart();
                $scope.monthLoader = false;
            }, function(response) {
                var message = response.data.message;
		        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }               
		        $scope.monthLoader = false;
            });
            for(var item in $scope.monthValues) {
                if($scope.monthValues[item].name == month) {
                    $scope.activeMonthLabel = $scope.monthValues[item].label;
                }
            }
        }
    };
    $scope.getYearStatistics = function(year) {
        if(year) {
            $scope.yearStatistics = false;
            $scope.yearLoader = true;
            $http.get(settings.prefix+ 'years/' + year + '/statistics').then(function(response){ 
                $scope.yearStatistics = response.data;
                $scope.yearStatistics.minutes_client_project_floored = Math.floor($scope.yearStatistics.minutes_client_project/60);
                $scope.yearStatistics.minutes_client_project_remainder = $scope.yearStatistics.minutes_client_project%60;
                $scope.yearStatistics.minutes_internal_project_floored = Math.floor($scope.yearStatistics.minutes_internal_project/60);
                $scope.yearStatistics.minutes_internal_project_remainder = $scope.yearStatistics.minutes_internal_project%60;
                var lastMonth = response.data.last_month;
                $scope.yearStatisticsStartMonth = $rootScope.monthLabels[0];
                $scope.yearStatisticsEndMonth = $rootScope.monthLabels[lastMonth-1];
                
                var userListByRevenue = $filter('orderBy')($scope.yearStatistics.users, '-revenue');
                var userListByClientProjects = $scope.sortBySum($scope.yearStatistics.users);
                var monthList = $filter('orderBy')($scope.yearStatistics.months, 'month_of_year');
                $scope.yearRevenueRows = [];
                $scope.yearHourRows = [];
                $scope.yearGeneralRows = [];
                
                for(var user in userListByRevenue) {
                    $scope.yearRevenueRows.push({ 'c': [
                        { 'v': userListByRevenue[user].first_name + ' ' + userListByRevenue[user].last_name },
                        { 'v': parseInt(userListByRevenue[user].revenue) },
                        { 'v': parseInt(userListByRevenue[user].expense) },
                    ]});
                }
                for(var user in userListByClientProjects) {
                    $scope.yearHourRows.push({ 'c': [
                        { 'v': userListByClientProjects[user].first_name + ' ' + userListByClientProjects[user].last_name },
                        { 'v': userListByClientProjects[user].minutes_client/60, f: Math.floor(userListByClientProjects[user].minutes_client/60) + 'h ' + (userListByClientProjects[user].minutes_client%60) + 'min' },
                        { 'v': userListByClientProjects[user].minutes_internal/60, f: Math.floor(userListByClientProjects[user].minutes_internal/60) + 'h ' + (userListByClientProjects[user].minutes_internal%60) + 'min' },
                    ]});
                }
                
                if($scope.selectedGeneralItem == 1) { var activeGeneralItem = 'avg_hour_rate'; $scope.activeGeneralItemLabel = "Tegelik billitava töö tunnihind" }
                if($scope.selectedGeneralItem == 2) { activeGeneralItem = 'gross_margin'; $scope.activeGeneralItemLabel = "Brutomarginaal"  }
                if($scope.selectedGeneralItem == 3) { activeGeneralItem = 'total_billing_rate'; $scope.activeGeneralItemLabel = "Billing rate (sh siseprojektid)"  }
                if($scope.selectedGeneralItem == 4) { activeGeneralItem = 'client_billing_rate'; $scope.activeGeneralItemLabel = "Billing rate (siseprojektideta)"  }
                for(var month in monthList) {
                    $scope.yearGeneralRows.push({ 'c': [
                        { 'v': $rootScope.monthLabels[monthList[month].month_of_year - 1] },
                        { 'v': monthList[month][activeGeneralItem] },
                    ]});
                }
                $scope.drawYearRevenueChart();
                $scope.drawYearHourChart();
                $scope.drawYearGeneralChart();
                $scope.yearLoader = false;
            }, function(response) {
                var message = response.data.message;
		        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }
		        $scope.yearLoader = false;
            });  
        }
    };
    
    $scope.getMonthStatistics($scope.statisticSelectedMonth);
    
    //month charts
    $scope.drawMonthRevenueChart = function() {
        $scope.monthRevenueChart = {};
        $scope.monthRevenueChart.type = "ColumnChart";
        $scope.monthRevenueChart.data = {
            "cols": [
                {id: 1, label: "Töötaja", type: "string"},
                {id: 2, label: "Tulu", type: "number"},
                {id: 2, label: "Kulu", type: "number"}
            ], 
            "rows": $scope.monthRevenueRows
        };
    
        $scope.monthRevenueChart.options = {
            'legend': { position: 'top', alignment: 'end' },
            'title': 'Raha',
            "colors": ['#3366cc', '#dc3912'],
            'chartArea': {'width': '85%'},
            'hAxis' : { 
                showTextEvery: 1,
                slantedText: true,
                slantedTextAngle: 45,
                textStyle : {
                    fontSize: 9
                }
            }
        };    
    }
    $scope.drawMonthHourChart = function() {
        $scope.monthHourChart = {};
        $scope.monthHourChart.type = "ColumnChart";
        $scope.monthHourChart.data = {
            "cols": [
                {id: 1, label: "Töötaja", type: "string"},
                {id: 2, label: "Tunnid kliendiprojektides", type: "number"},
                {id: 3, label: "Tunnid siseprojektides", type: "number"},
            ], 
            "rows": $scope.monthHourRows
        };
    
        $scope.monthHourChart.options = {
            'legend': { position: 'top', alignment: 'end' },
            'title': 'Töötunnid',
            "colors": ['#FF9900', '#990099'],
            'chartArea': {'width': '85%'},
            'isStacked': true,
            'hAxis' : {
                showTextEvery: 1,
                slantedText: true,
                slantedTextAngle: 45,
                textStyle : {
                    fontSize: 9
                }
            }
        };    
    }
    
    //year-charts
    $scope.drawYearRevenueChart = function() {
        $scope.yearRevenueChart = {};
        $scope.yearRevenueChart.type = "ColumnChart";
        $scope.yearRevenueChart.data = {
            "cols": [
                {id: 1, label: "Töötaja", type: "string"},
                {id: 2, label: "Tulu", type: "number"},
                {id: 2, label: "Kulu", type: "number"}
            ], 
            "rows": $scope.yearRevenueRows
        };
    
        $scope.yearRevenueChart.options = {
            'legend': { position: 'top', alignment: 'end' },
            'title': 'Raha',
            "colors": ['#3366cc', '#dc3912'],
            'chartArea': {'width': '85%'},
            'hAxis' : { 
                showTextEvery: 1,
                slantedText: true,
                slantedTextAngle: 45,
                textStyle : {
                    fontSize: 9
                }
            }
        };    
    }
    $scope.drawYearHourChart = function() {
        $scope.yearHourChart = {};
        $scope.yearHourChart.type = "ColumnChart";
        $scope.yearHourChart.data = {
            "cols": [
                {id: 1, label: "Töötaja", type: "string"},
                {id: 2, label: "Tunnid kliendiprojektides", type: "number"},
                {id: 3, label: "Tunnid siseprojektides", type: "number"},
            ], 
            "rows": $scope.yearHourRows
        };
    
        $scope.yearHourChart.options = {
            'legend': { position: 'top', alignment: 'end' },
            'title': 'Töötunnid',
            "colors": ['#FF9900', '#990099'],
            'chartArea': {'width': '85%'},
            'isStacked': true,
            'hAxis' : { 
                showTextEvery: 1,
                slantedText: true,
                slantedTextAngle: 45,
                textStyle : {
                    fontSize: 9
                }
            }
        };    
    }
    $scope.drawYearGeneralChart = function() {
        $scope.yearGeneralChart = {};
        $scope.yearGeneralChart.type = "LineChart";
        $scope.yearGeneralChart.data = {
            "cols": [
                {id: 1, label: "Kuud", type: "string"},
                {id: 2, label: $scope.activeGeneralItemLabel, type: "number"},
            ], 
            "rows": $scope.yearGeneralRows
        };
    
        $scope.yearGeneralChart.options = {
            'legend': { position: 'top', alignment: 'end' },
            'title': $scope.activeGeneralItemLabel,
            "colors": ['#990099'],
            'chartArea': {'width': '85%'}
        };    
    }
})