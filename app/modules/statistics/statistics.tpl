<script type="text/ng-template" id="statistics.tpl">
    <div class="container animate-if">
        <div class="row">
            <main class="col-sm-12 pt-3 mt-2">
				<ng-include src="'breadcrumbs.tpl'"></ng-include>
				<error-message class="mt-4" ng-if="messages.errorMessage" fadeout=true error="messages.errorMessage"></error-message>
				<ul class="nav nav-tabs mt-4">
				  <li class="nav-item">
				    <a ng-class="{ 'active' : firstActive }" ng-click="setTab(true)" class="nav-link" href="">Kuu valik</a>
				  </li>
				  <li class="nav-item">
				    <a ng-class="{ 'active' : secondActive }" ng-click="setTab(false)" class="nav-link" href="">Aasta</a>
				  </li>
				</ul>

                <section class="month animate-hide-remove" ng-hide="!firstActive">
                    <form>
    					<div class="row">
    						<div class="col-12 col-md-12">
    							<div class="bordered-elem p-3">
    								<div class="form-group mb-0">
    									<h5 class="form-control-label d-inline">Kuu</h5>
    									<select ng-model="statisticSelectedMonth" class="form-control custom-select" ng-options="item.name as item.label for item in monthValues" ng-change="getMonthStatistics(statisticSelectedMonth);" style="width:200px; margin-left: 11px;">
    									</select>
    								</div><!--/form-group-->
    	                        </div>
    						</div><!--/col-3 col-md-3-->
    					</div><!--/row-->
    				</form>
    				<form>
    					<div class="row">
    						<div class="col-md-6 col-sm-12">
    							<div class="bordered-elem mt-4 fade-element-in" ng-if="monthStatistics && monthHourChart && monthRevenueChart">
    							    <div class="row upper-statistic-row border-bottom">
                                        <div class="col-md-6 upper-statistic-col text-left">
                                            <div class="text-left"><label class="form-control-label ml-3 bold">Tegelik billitava töö tunnihind</label></div>
                                        </div>
                                        <div class="col-md-6 upper-statistic-col">
                                            <div class="text-center"><label class="form-control-label ml-3 bold">{{monthStatistics.avg_hour_rate | number: 2 }} €</label></div>
                                        </div>
                                    </div>
                                    <div class="row upper-statistic-row border-bottom">
                                        <div class="col-md-6 upper-statistic-col">
                                            <div class="text-left"><label class="form-control-label ml-3 bold">Brutomarginaal</label></div>
                                        </div>
                                        <div class="col-md-6 upper-statistic-col">
                                            <div class="text-center"><label class="form-control-label ml-3 bold">{{monthStatistics.gross_margin | number }} %</label></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
    						<div class="col-md-6 col-sm-12">
    							<div class="bordered-elem mt-4 fade-element-in" ng-if="monthStatistics && monthHourChart && monthRevenueChart">
                                    <div class="row upper-statistic-row border-bottom">
                                        <div class="col-md-6 upper-statistic-col">
                                            <div class="text-left"><label class="form-control-label ml-3 bold">Billing rate (sh siseprojektid)</label></div>
                                        </div>
                                        <div class="col-md-6 upper-statistic-col">
                                            <div class="text-center"><label class="form-control-label ml-3 bold">{{monthStatistics.total_billing_rate | number }} %</label></div>
                                        </div>
                                    </div>
                                    <div class="row upper-statistic-row">
                                        <div class="col-md-6 upper-statistic-col">
                                            <div class="text-left"><label class="form-control-label ml-3 bold">Billing rate (siseprojektideta)</label></div>
                                        </div>
                                        <div class="col-md-6 upper-statistic-col">
                                            <div class="text-center"><label class="form-control-label ml-3 bold">{{monthStatistics.client_billing_rate | number }} %</label></div>
                                        </div>
                                    </div>
    	                        </div>
    						</div><!--/col-3 col-md-3-->
    					</div><!--/row-->
    				</form>
                    <div ng-if="monthRevenueChart" class="fade-element-in chart-holder bordered-elem mt-4">
                        <div google-chart chart="monthRevenueChart" style="height:500px; width:100%;"></div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="text-center"><label class="form-control-label ml-3 mt-3 mb-3 bold">Tulud kokku: <span style="color: #3366cc">{{monthStatistics.revenue | number: 0 }} €</span></label></div>
                            </div>
                            <div class="col-md-6">
                                <div class="text-center"><label class="form-control-label ml-3 mt-3 mb-3 bold">Kulud kokku: <span style="color: #dc3912">{{monthStatistics.expense | number: 0 }} €</span></label></div>
                            </div>
                        </div>
                        <div class="row" ng-if="monthRevenueChart">
    						<div class="col-md-12 ml-3 mb-3">
    							<a href="" ng-hide="enableMonthRevenueTable" class="animate-hide-remove" ng-click="setMonthRevenueTableVisible()">Vaata täpsemalt</a>
    							<a href="" ng-hide="!enableMonthRevenueTable" class="animate-hide-remove" ng-click="setMonthRevenueTableVisible()">Peida</a>
    						</div><!--/col-12 col-md-12-->
    					</div><!--/row-->
                    </div>
                    <div class="row fade-element-in mt-4" ng-if="monthStatistics && enableMonthRevenueTable">
                        <div class="col-md-12 table-holder">
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th width="25%">Töötaja</th>
                                        <th>Tulu</th>
                                        <th>Kulu</th>
                                        <th>Vahe</th>
                                        <th>Bmrg</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="user in monthStatistics.users | orderBy: '-revenue'">
                                        <td>{{user.first_name}} {{user.last_name}}</td>
                                        <td>{{user.revenue | number: 0 }} €</td>
                                        <td>{{user.expense | number: 0 }} €</td>
                                        <td>{{user.difference | number: 0 }} €</td>
                                        <td>{{user.gross_margin | number }} %</td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">Kokku</td>
                                        <td class="font-weight-bold">{{monthStatistics.revenue | number: 0 }} €</td>
                                        <td class="font-weight-bold">{{monthStatistics.expense | number: 0 }} €</td>
                                        <td class="font-weight-bold">{{monthStatistics.difference | number: 0 }} €</td>
                                        <td class="font-weight-bold">{{monthStatistics.gross_margin | number }} %</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div ng-if="monthHourChart" class="fade-element-in chart-holder bordered-elem mt-4">
                        <div google-chart chart="monthHourChart" style="height:500px; width:100%;"></div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="text-center"><label class="form-control-label ml-3 mt-3 mb-3 bold">Tunnid kliendiprojektides: <span style="color: #FF9900">{{monthStatistics.minutes_client_project_floored}}h {{monthStatistics.minutes_client_project_remainder}}min</span></label></div>
                            </div>
                            <div class="col-md-6">
                                <div class="text-center"><label class="form-control-label ml-3 mt-3 mb-3 bold">Tunnid siseprojektides: <span style="color: #990099">{{monthStatistics.minutes_internal_project_floored}}h {{monthStatistics.minutes_internal_project_remainder}}min</span></label></div>
                            </div>
                        </div>
                        <div class="row" ng-if="monthHourChart">
    						<div class="col-md-12 ml-3 mb-3">
    							<a ng-hide="enableMonthHourTable" class="animate-hide-remove" href="" ng-click="setMonthHourTableVisible()">Vaata täpsemalt</a>
    							<a ng-hide="!enableMonthHourTable" class="animate-hide-remove" href="" ng-click="setMonthHourTableVisible()">Peida</a>
    						</div><!--/col-12 col-md-12-->
    					</div><!--/row-->
                    </div>
                    <div class="row fade-element-in mt-4" ng-if="monthStatistics && enableMonthHourTable">
                        <div class="col-md-12 table-holder">
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th width="25%">Töötaja</th>
                                        <th>Tunnid kliendiprojektides</th>
                                        <th>Tunnid siseprojektides</th>
                                        <th>Keskmised tunnid päevas</th>
                                        <th>Töötatud päevi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="user in monthStatistics.users | orderBy: '-minutes_client'">
                                        <td>{{user.first_name}} {{user.last_name}}</td>
                                        <td>{{user.hours_client}}</td>
                                        <td>{{user.hours_internal}}</td>
                                        <td>{{user.avg_hour_day}}</td>
                                        <td>{{user.worked_days | number }}</td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">Kokku</td>
                                        <td class="font-weight-bold">{{monthStatistics.minutes_client_project_floored}}h {{monthStatistics.minutes_client_project_remainder}}min</td>
                                        <td class="font-weight-bold">{{monthStatistics.minutes_internal_project_floored}}h {{monthStatistics.minutes_internal_project_remainder}}min</td>
                                        <td class="font-weight-bold"></td>
                                        <td class="font-weight-bold">{{monthStatistics.collective_worked_days | number }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
                <section class="year animate-hide-remove" ng-hide="!secondActive">
    				<form>
    					<div class="row">
    						<div class="col-12 col-md-12">
    							<div class="bordered-elem p-3">
    								<div class="form-group mb-0">
    									<h5 class="form-control-label d-inline">Kuu</h5>
    									<select ng-model="yearStatisticSelectedMonth" class="form-control custom-select" ng-options="item.name as item.label for item in monthValues" ng-change="getYearStatistics(yearStatisticSelectedMonth);" style="width:200px; margin-left: 11px;">
    									</select>
    								</div><!--/form-group-->
    								<div class="form-group float-right" ng-if="false">
                                        <h5 class="form-control-label ml-3 d-inline">{{yearStatisticsStartMonth}}-{{yearStatisticsEndMonth}} {{yearStatistics.year}}</h5>
    								</div><!--/form-group-->
    	                        </div>
    						</div><!--/col-3 col-md-3-->
    					</div><!--/row-->
    				</form>
    				<form>
    					<div class="row">
    						<div class="col-md-6 col-sm-12 mt-4 fade-element-in" ng-if="yearStatistics && yearHourChart && yearRevenueChart">
    							<div class="bordered-elem">
                                    <div class="row upper-statistic-row border-bottom" ng-class="{ 'selected-border' : selectedGeneralItem == 1 }" ng-click="setGeneralItem(1)">
                                        <div class="col-md-6 upper-statistic-col text-left">
                                            <div><label class="form-control-label ml-3 bold">Tegelik billitava töö tunnihind</label></div>
                                        </div>
                                        <div class="col-md-6 upper-statistic-col">
                                            <div class="text-center"><label class="form-control-label ml-3 bold">{{yearStatistics.avg_hour_rate | number: 2 }} €</label></div>
                                        </div>
                                    </div>
                                    <div class="row upper-statistic-row border-bottom" ng-class="{ 'selected-border' : selectedGeneralItem == 2 }" ng-click="setGeneralItem(2)">
                                        <div class="col-md-6 upper-statistic-col text-left">
                                            <div><label class="form-control-label ml-3 bold">Brutomarginaal</label></div>
                                        </div>
                                        <div class="col-md-6 upper-statistic-col">
                                            <div class="text-center"><label class="form-control-label ml-3 bold">{{yearStatistics.gross_margin | number }} %</label></div>
                                        </div>
                                    </div>
                                    <div class="row upper-statistic-row border-bottom" ng-class="{ 'selected-border' : selectedGeneralItem == 3 }" ng-click="setGeneralItem(3)">
                                        <div class="col-md-6 upper-statistic-col text-left">
                                            <div><label class="form-control-label ml-3 bold">Billing rate (sh siseprojektid)</label></div>
                                        </div>
                                        <div class="col-md-6 upper-statistic-col">
                                            <div class="text-center"><label class="form-control-label ml-3 bold">{{yearStatistics.total_billing_rate | number }} %</label></div>
                                        </div>
                                    </div>
                                    <div class="row upper-statistic-row" ng-class="{ 'selected-border' : selectedGeneralItem == 4 }" ng-click="setGeneralItem(4)">
                                        <div class="col-md-6 upper-statistic-col text-left">
                                            <div><label class="form-control-label ml-3 bold">Billing rate (siseprojektideta)</label></div>
                                        </div>
                                        <div class="col-md-6 upper-statistic-col">
                                            <div class="text-center"><label class="form-control-label ml-3 bold">{{yearStatistics.client_billing_rate | number }} %</label></div>
                                        </div>
                                    </div>
    	                        </div>
    						</div><!--/col-3 col-md-3-->
    						<div class="col-md-6 col-sm-12 mt-4 fade-element-in" ng-if="yearStatistics">
    						    <div ng-if="yearGeneralChart" class="chart-holder bordered-elem">
                                    <div google-chart chart="yearGeneralChart" style="height:500px; width:100%;"></div>
                                </div>
    						</div>
    					</div><!--/row-->
    				</form>
                    <div ng-if="yearRevenueChart && yearStatistics" class="fade-element-in chart-holder bordered-elem mt-4">
                        <div google-chart chart="yearRevenueChart" style="height:500px; width:100%;"></div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="text-center"><label class="form-control-label ml-3 mt-3 mb-3 bold">Tulud kokku: <span style="color: #3366cc">{{yearStatistics.revenue | number: 0 }} €</span></label></div>
                            </div>
                            <div class="col-md-6">
                                <div class="text-center"><label class="form-control-label ml-3 mt-3 mb-3 bold">Kulud kokku: <span style="color: #dc3912">{{yearStatistics.expense | number: 0 }} €</span></label></div>
                            </div>
                        </div>
                        <div class="row" ng-if="yearRevenueChart">
    						<div class="col-md-12 ml-3 mb-3">
    							<a ng-hide="enableYearRevenueTable" class="animate-hide-remove" href="" ng-click="setYearRevenueTableVisible()">Vaata täpsemalt</a>
    							<a ng-hide="!enableYearRevenueTable" class="animate-hide-remove" href="" ng-click="setYearRevenueTableVisible()">Peida</a>
    						</div><!--/col-12 col-md-12-->
    					</div><!--/row-->
                    </div>
                    <div class="row fade-element-in mt-4" ng-if="yearStatistics && enableYearRevenueTable">
                        <div class="col-md-12 table-holder">
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                    <th width="25%">Töötaja</th>
                                    <th>Tulu</th>
                                    <th>Kulu</th>
                                    <th>Vahe</th>
                                    <th>Bmrg</th>
                                    </tr>
                                </thead>
                                <tbody ng-if="yearStatistics">
                                    <tr ng-repeat="user in yearStatistics.users | orderBy: '-revenue'">
                                        <td>{{user.first_name}} {{user.last_name}}</td>
                                        <td>{{user.revenue | number: 0 }} €</td>
                                        <td>{{user.expense | number: 0 }} €</td>
                                        <td>{{user.difference | number: 0 }} €</td>
                                        <td>{{user.gross_margin | number }} %</td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">Kokku</td>
                                        <td class="font-weight-bold">{{yearStatistics.revenue | number: 0 }} €</td>
                                        <td class="font-weight-bold">{{yearStatistics.expense | number: 0 }} €</td>
                                        <td class="font-weight-bold">{{yearStatistics.difference | number: 0 }} €</td>
                                        <td class="font-weight-bold">{{yearStatistics.gross_margin | number }} %</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div ng-if="yearHourChart && yearStatistics" class="fade-element-in chart-holder bordered-elem mt-4">
                        <div google-chart chart="yearHourChart" style="height:500px; width:100%;"></div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="text-center"><label class="form-control-label ml-3 mt-3 mb-3 bold">Tunnid kliendiprojektides: <span style="color: #FF9900">{{yearStatistics.minutes_client_project_floored}}h {{yearStatistics.minutes_client_project_remainder}}min</span></label></div>
                            </div>
                            <div class="col-md-6">
                                <div class="text-center"><label class="form-control-label ml-3 mt-3 mb-3 bold">Tunnid siseprojektides: <span style="color: #990099">{{yearStatistics.minutes_internal_project_floored}}h {{yearStatistics.minutes_internal_project_remainder}}min</span></label></div>
                            </div>
                        </div>
                        <div class="row" ng-if="yearHourChart">
    						<div class="col-md-12 ml-3 mb-3">
    							<a href="" ng-hide="enableYearHourTable" class="animate-hide-remove" ng-click="setYearHourTableVisible()">Vaata täpsemalt</a>
    							<a href="" ng-hide="!enableYearHourTable" class="animate-hide-remove" ng-click="setYearHourTableVisible()">Peida</a>
    						</div><!--/col-12 col-md-12-->
    					</div><!--/row-->
                    </div>
                    <div class="row fade-element-in mt-4" ng-if="yearStatistics && enableYearHourTable">
                        <div class="col-md-12 table-holder">
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th width="25%">Töötaja</th>
                                        <th>Tunnid kliendiprojektides</th>
                                        <th>Tunnid siseprojektides</th>
                                        <th>Keskmised tunnid päevas</th>
                                        <th>Töötatud päevi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="user in yearStatistics.users | orderBy: '-minutes_client'">
                                        <td>{{user.first_name}} {{user.last_name}}</td>
                                        <td>{{user.hours_client}}</td>
                                        <td>{{user.hours_internal}}</td>
                                        <td>{{user.avg_hour_day}}</td>
                                        <td>{{user.worked_days | number }}</td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">Kokku</td>
                                        <td class="font-weight-bold">{{yearStatistics.minutes_client_project_floored}}h {{yearStatistics.minutes_client_project_remainder}}min</td>
                                        <td class="font-weight-bold">{{yearStatistics.minutes_internal_project_floored}}h {{yearStatistics.minutes_internal_project_remainder}}min</td>
                                        <td class="font-weight-bold"></td>
                                        <td class="font-weight-bold">{{yearStatistics.collective_worked_days | number }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
                <div ng-if="secondActive && yearLoader" class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
                <div ng-if="firstActive && monthLoader" class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </main><!--col9-->
        </div><!--row-->
      </div><!--/container-->
</script>
