App.controller('users', function($scope, $http, $route, $rootScope, $routeParams, $location, ModalService){
    
    $scope.roles = { "ADMIN": 'Administraator', "OUTSOURCER": "Allhankija", "USER": "Projektijuht", "EMPLOYEE": "Töötaja" };
    
    $scope.messages = { alertMessage: false, myMessage: false };
    $scope.activatedVal = [ "Deaktiveeritud", "Aktiveeritud" ];
    $scope.emailRecipients = [];
    
    $scope.outSourcerLast = function(item){
        return item.role !== 'OUTSOURCER';
    }
    
    $scope.removeRecipient = function(index) {
        $scope.emailRecipients.splice(index, 1);
        $scope.saveEmailRecipients();
    };
    
    $scope.addRecipient = function() {
        $scope.emailRecipients.push({ email: ""});
    };
    
	$scope.getUsers = function() {
        $http.get(settings.prefix + 'users').then(function(response) {
            $scope.users = response.data;
        }, function(response) {
            $scope.messages.errorMessage = response.data.message;
        });
    };
    $scope.getUsers();
    
    $scope.reloadThis = function() {
        $route.reload();
    }
    
    $scope.openUserEdit = function(id){
        ModalService.showModal({
            templateUrl: 'userModal.html',
            controller: "userModal"
        }).then(function(modal) {
            
            $http.get(settings.prefix+ 'users/' + id).then(function(response) {
                modal.scope.modalHeader = "Töötaja andmed";
                modal.scope.editForm = true;
                modal.scope.user = response.data;
                if(modal.scope.user && modal.scope.user.active_since) {
                    modal.scope.user.active_since = new Date(modal.scope.user.active_since);
                };
                if(modal.scope.user && modal.scope.user.vat_min_since) {
                    modal.scope.user.vat_min_since = new Date(modal.scope.user.vat_min_since);
                };
            }, function(response) {
                console.log(response);
            }).then(function(){
                modal.element.show();  
            });
            modal.close.then(function(result) {
                if(result) { $scope.reloadThis(); };
            });
        });
    };
    $scope.openUserAdd = function(){
        ModalService.showModal({
            templateUrl: 'userModal.html',
            controller: "userModal"
        }).then(function(modal) {
            modal.scope.modalHeader = "Uue töötaja lisamine"
            modal.element.show();  
            modal.close.then(function(result) {
                if(result) { $scope.reloadThis(); };
            });
        });
    };
    $scope.getEmailRecipients = function() {
        $http.get(settings.prefix + 'config/carbon').then(function(response) {
            if(response.data.value) {
                var recipientArray = (response.data.value).split(',');
                for(var elem in recipientArray) {
                    $scope.emailRecipients.push({ email: recipientArray[elem] });
                }
            } else {
                $scope.emailRecipients.push({ email: "" });
            }
        }, function(response) {
            $scope.messages.errorMessage = response.data.message;
        });
    }
    $scope.getEmailRecipients();
    
    $scope.saveEmailRecipients = function() {
        
        var submitData = {};
        var recipientArray = [];
        for(var elem in $scope.emailRecipients) {
            if($scope.emailRecipients[elem].email != undefined) {
                recipientArray.push($scope.emailRecipients[elem].email);
            };
        }
        submitData['cc'] = recipientArray.join(',');
        if(submitData.cc == "") { submitData.cc = " "; }
        $http({
			method: "PUT",
			url: settings.prefix+ 'config/carbon',
			data: submitData,
		}).then(function( response ){
		    $scope.messages.myMessage = true;
		}, function(response){
		    var message = response.data.message;
	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
		});
		
    }
})