<script type="text/ng-template" id="users.tpl">
    <div class="container animate-if">
        <div class="row">
            <main class="col-sm-12 pt-3 mt-2 mb-4">
				<ng-include src="'breadcrumbs.tpl'"></ng-include>
				<error-message class="mt-4" ng-if="messages.errorMessage" error="messages.errorMessage" fadeout=true></error-message>
				<div class="row mt-4 fade-element-in" ng-if="users">
                    <div class="col-12">
                        <div class="bordered-elem p-3">
                            <h5 class="form-control-label mb-3">Lisa uue töötaja andmed ja palgamudel</h5>
                            <div class="mt-3">
                                <a href="" ng-click="openUserAdd()" class="btn btn-primary">Lisa töötaja</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row table-holder mt-4 fade-element-in" ng-if="users">
                    <div class="col-md-12 col-sm-12">
						<table class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr>
                                    <th style="width:30%">Nimi</th>
                                    <th style="width:30%">Roll</th>
                                    <th style="width:20%">Staatus</th>
                                    <th style="width:20%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="user in users | orderBy:'-first_name' | orderBy:outSourcerLast | orderBy:'active': 1">
                                    <td>{{user.first_name}} {{user.last_name}}</td>
                                    <td>{{roles[user.role]}}</td>
                                    <td ng-class="{ 'user-approve-text' : user.active === 1, 'user-alert-text' : user.active === 0 }">{{activatedVal[user.active]}}</td>
                                    <td><a href="" class="btn btn-primary light-green" ng-click="openUserEdit(user.id)">Muuda</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div ng-if="!users" class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </main><!--col9-->
        </div><!--row-->
    </div><!--/container-->
</script>