<script type="text/ng-template" id="user-confirm-modal.tpl">
    <div class="modal-dialog modal-import">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{modalHeader}}</h4>
                <button ng-disabled="loading" type="button" class="close" ng-click="close(false)" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <error-message ng-if="messages.errorMessage" fadeout=true error="messages.errorMessage"></error-message>
                <toalert-message ng-if="messages.alertMessage" toalert="messages.alertMessage"></toalert-message>
                <div class="spacing"></div>
                <div>Sisestatud eesnime ja perekonnanimega kasutaja on süsteemis juba olemas, kas soovid ikkagi jätkata?</div>
			</div>
            <div class="modal-footer">
                <button ng-disabled="loading" type="button" ng-click="close(false)" class="btn btn-warning" data-dismiss="modal">Katkesta</button>
                <button ng-disabled="loading" type="button" ng-click="add()" class="btn btn-primary" data-dismiss="modal">Edasi</button>
            </div>
        </div>
    </div>
</script>