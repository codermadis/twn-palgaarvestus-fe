<script type="text/ng-template" id="user-modal.tpl">
    <div class="modal-dialog modal-user">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{modalHeader}}</h4>
                <button type="button" class="close" ng-click="close(false)" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form name="userForm" id="userForm">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <error-message fadeout=true ng-if="messages.errorMessage" error="messages.errorMessage"></error-message>
                            <toalert-message ng-if="messages.alertMessage" toalert="messages.alertMessage"></toalert-message>
                        </div>
                    </div>
    				<div class="row">
    				    <div class="form-group col-md-6">
    				        <label for="first_name">Eesnimi</label>
    					    <input class="form-control" type="text" name="first_name" id="first_name" ng-model="user.first_name" required >
    					</div>
    				    <div class="form-group col-md-6">
    				        <label for="last_name">Perekonnanimi</label>
    					    <input class="form-control" type="text" name="last_name" id="last_name" ng-model="user.last_name" pattern="[a-zA-ZõäöüÕÄÖÜ-]+" ng-pattern-restrict required >
    					</div>
    				</div>
    				<div class="row">
    					<div class="form-group col-md-6">
    				        <label for="last_name">E-mail</label>
    					    <input class="form-control" type="email" name="email" id="email" ng-model="user.email" required >
    					</div>
    					<div class="form-group col-md-6">
    				        <label for="first_name">Roll</label>
    					    <select ng-class="{ 'border-error' : roleError }" class="form-control" type="text" name="role" id="role" ng-model="user.role" required >
    					        <option value="EMPLOYEE">Töötaja</option>
    					        <option value="USER">Projektijuht</option>
    					        <option value="ADMIN">Administraator</option>
    					        <option value="OUTSOURCER">Allhankija</option>
    					    </select>
    					</div>
    				</div>
    			    <div class="row">
    				    <div class="col-md-6">
    				        <label for="date">Töötab alates</label>
                            <p class="input-group">
                                <input type="text" ng-class="{ 'border-error' : datepickerFirstError }" class="form-control datepicker-element-1" readonly="true" ng-model="user.active_since" uib-datepicker-popup="dd-MM-yyyy" is-open="popup.first" datepicker-options="dateOptions" close-text="Sulge" current-text="Täna" clear-text="Kustuta"/>
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default" ng-click="open_first()"><i class="fa fa-calendar"></i></button>
                                </span>
                            </p>
                        </div>
    			    </div>
    			    <div class="row" ng-if="user.role != 'OUTSOURCER'">
    				    <div class="form-group col-md-4">
    				        <label class="primary-label" for="last_name">Palgamudel</label>
    				        <div>
        					    <input class="form-control user-inline-radio" type="radio" name="main" value=1 ng-model="user.salary_model" ng-required="user.salary_model == 1 || !user.salary_model">
        					    <label for="main" class="user-inline-label">põhipalk</label>
    					    </div>
    					    <div>
        					    <input class="form-control user-inline-radio" type="radio" name="hour" value=2 ng-model="user.salary_model" ng-required="user.salary_model == 2">
                                <label for="hour" class="user-inline-label">tunnipõhine</label>    	
                            </div>
                            <div>
        					    <input class="form-control user-inline-radio" type="radio" name="calc" value=3 ng-model="user.salary_model" ng-required="user.salary_model == 3">
        					    <label for="calc" class="user-inline-label">valemipõhine</label>
    					    </div>
    					</div>
    					<div class="form-group col-md-8">
    					    <div class="heavy-spacing"></div>
    					    <div class="row" ng-if="user.salary_model == 1 || user.salary_model == 3">
    					        <div class="col-md-3">
            				        <label for="last_name regular-weight">Põhipalk</label>
            				    </div>
            				    <div class="col-md-4">
            					    <input valid-number smart-float class="form-control form-control-sm" type="text" name="rate_month" id="rate_month" ng-model="user.rate_month" required>
            					</div>
            				</div>
            				<div class="row" ng-if="user.salary_model == 2">
    					        <div class="col-md-3">
    					            <label for="last_name regular-weight">Tunnihind</label>
            				    </div>
            				    <div class="col-md-4">
            				        <input valid-number smart-float class="form-control form-control-sm" type="text" name="rate_hour" id="rate_hour" ng-model="user.rate_hour" required>
            					</div>
            				</div>
            				<div class="row" ng-if="user.salary_model == 3">
    					        <div class="col-md-3">
    					            <label for="last_name regular-weight">Lisatasupiir</label>
            				    </div>
            				    <div class="col-md-4">
            				        <input valid-number smart-float class="form-control form-control-sm" type="text" name="bonus_threshold" id="bonus_threshold" ng-model="user.bonus_threshold" required>
            					</div>
            				</div>
            				<div class="row" ng-if="user.salary_model == 3">
    					        <div class="col-md-3">
    					            <label for="last_name regular-weight">Koefitsient</label>
            				    </div>
            				    <div class="col-md-4">
            				        <input valid-number smart-float class="form-control form-control-sm" type="text" name="coefficient" id="coefficient" ng-model="user.coefficient" required>
            					</div>
            				</div>
    					</div>
    			    </div>
    			    <div class="row" ng-if="user.role != 'OUTSOURCER'">
    				    <div class="col-md-6 col-sm-12 form-group fade-element-in">
    				        <label for="date">Tulumaksuvaba miinimumi avaldus</label>
    				        <div class="padded bordered-elem radius">
                                <div class="spacing"></div>
                                <div ng-if="user.files.length" class="mb-1 fade-element-out">
                                    <div class="d-inline-block text-center pr-2 fade-element-out" ng-if="file && user.files.length" ng-class="{ 'pl-2': !$first, 'pl-0' : $first }" ng-repeat="file in user.files" >
                                        <a class="files-remove" href="" ng-click="deleteDocument(file.id)"><i class="fa fa-close address-close user-alert-text"></i></a>
                                        <a ng-click="downloadDocument(file.id, file.file_path)">
                                            <i class="fa fa-download fa-2x"></i>
                                        </a>
                                        <div class="files-name">{{file.originalname}}</div>
                                    </div>
                                </div>
                                <div ng-if="!user.files.length" class="fade-element-in">
                                    <p class="soft-italic">Lisa fail</p>
                                    <input type="file" ngf-select ng-model="addedFile.uploadedFile" name="file" ngf-max-size="5MB" ngf-model-invalid="errorFile" class="file-input">
                                    <a href="" ng-click="addedFile.uploadedFile = null" ng-show="addedFile.uploadedFile"><i class="fa fa-close address-close user-alert-text"></i></a>
                                    <div><i ng-show="file.$error.maxSize">Fail liiga suur {{errorFile.size / 1000000|number:1}}MB: max 5MB</i></div>
                                    <div class="spacing"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 fade-element-in">
    				        <label for="date">Arvesta tulumaksuvaba miinimumi alates</label>
                            <p class="input-group">
                                <input type="text" ng-class="{ 'border-error' : datepickerSecondError }" class="form-control datepicker-element-2" readonly="true" ng-model="user.vat_min_since" uib-datepicker-popup="dd-MM-yyyy" is-open="popup.second" datepicker-options="dateOptions" close-text="Sulge" current-text="Täna" clear-text="Kustuta" required/>
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default" ng-click="open_second()"><i class="fa fa-calendar"></i></button>
                                </span>
                            </p>
                        </div>
    			    </div>
    			    <div class="row">
			            <div class="col-md-6 col-sm-6" ng-if="editForm">
            		        <label class="primary-label user-inline-radio" for="active">Deaktiveeritud</label>
            			    <input class="form-control user-inline-radio" type="checkbox" name="active" ng-true-value="0" ng-false-value="1" ng-model="user.active">
            			</div>
    			    </div>
			    </div>
                <div class="modal-footer">
    				<div class="col-md-12 col-sm-12 right-align" ng-if="editForm">
                        <button type="button" ng-click="close(false)" class="btn btn-warning mt-2 mb-2" data-dismiss="modal">Katkesta</button>
                        <button type="submit" ng-click="edit(user, userForm, addedFile.uploadedFile)" class="btn btn-primary mt-2 mb-2" data-dismiss="modal">Salvesta</button>
                    </div>
                    <div class="col-md-12 col-sm-12 right-align" ng-if="!editForm">
                        <button type="button" ng-click="close(false)" class="btn btn-warning mt-2 mb-2" data-dismiss="modal">Katkesta</button>
                        <button type="submit" ng-click="openUserConfirm(userForm, addedFile.uploadedFile)" class="btn btn-primary mt-2 mb-2" data-dismiss="modal">Salvesta</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</script>