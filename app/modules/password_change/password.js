App.controller('password', function($scope, $http, $rootScope, $routeParams, $location, $timeout){
	
	$scope.getLinkStatus = function() {
		$http.get(settings.prefix + 'password_reset?email=' + $location.search().email + '&code=' + $location.search().code).then(function(response) {
			$scope.acceptedLink = true;
			$scope.currentEmail = $location.search().email;
			$scope.linkStatusReceived = true;
		}, function(response){
			$scope.currentEmail = $location.search().email;
			$scope.messages.errorMessage = response.data.message;
			$scope.acceptedLink = false;
			$scope.linkStatusReceived = true;
			// $timeout(function () {
			// 	$location.search({});
   //             $location.path('/login');
   //         }, 2000); 
		});
		
	};
	$scope.getLinkStatus();
	
	$rootScope.hideNavbar = true;
	$scope.messages = { errorMessage : false, alertMessage: false }; 
	
	$scope.$watch('messages.errorMessage', function(newValue, oldValue) {
	    if(newValue != oldValue) {
	        $timeout(function () {
                $scope.messages.errorMessage = false;
            }, 2500);      
	    }
	});
	
	$scope.$watch('messages.alertMessage', function(newValue, oldValue) {
	    if(newValue != oldValue) {
	        $timeout(function () {
                $scope.messages.alertMessage = false;
            }, 2500);      
	    }
	});
	
	$scope.openPasswordForm = function() {
	    $location.path('/login');
	};
	$scope.changePassword = function(password, repeated, form){
	    
	    var parseQueryString = function() {
            var str = window.location.search;
            var objURL = {};
        
            str.replace(
                new RegExp( "([^?=&]+)(=([^&]*))?", "g" ),
                function( $0, $1, $2, $3 ){
                    objURL[ $1 ] = $3;
                }
            );
            return objURL;
        };
        
        var params = parseQueryString();
		
		if(form.$valid) {
		    if(angular.equals(password, repeated)) {
				$http({
					method: "PUT",
					url: settings.prefix + 'password_reset?email=' + $location.search().email + '&code=' + $location.search().code,
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					data: {
						password: password,
					},
		    		transformRequest: function(obj) {
			            var str = [];
			            for(var p in obj)
			               str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
			            return str.join("&");
		    		}
				}).then(function( response ){
					$scope.messages.alertMessage = response.data.message;
				}, function(response){
					$scope.messages.errorMessage = response.data.message;
				});
			} else {
				$scope.messages.errorMessage = "Sisestatud paroolid ei ühti, proovi uuesti!";
			}
		} 
	}
})