<script id="password.tpl" type="text/ng-template">
	<div class="container" ng-if="linkStatusReceived">
		<div ng-if="!acceptedLink" class="login text-center disallowed-error animate-if">
			<div class="login-logo">
				<img src="/assets/img/logo.svg" width="300" height="120" alt="logo">
			</div>
			<h2>Kehtetu link</h2>
		</div>
		<section ng-if="acceptedLink" class="container login__container text-center">
			<div class="login-logo">
				<img src="/assets/img/logo.svg" width="370" height="120" alt="logo">
			</div>
			<div class="login">
            	<h2><span id="formHeading">Palgaarvestus ja statistika</span></h2>
            	<!--Login form-->
	            <form id="new_password_form" name="new_password_form">
					
					<p ng-if="messages.errorMessage" class="well text-danger" id="login_validation_errors">{{messages.errorMessage}}</p>
					<p ng-if="messages.alertMessage" class="well item-update" id="login_validation_errors">{{messages.alertMessage}}</p>
					<p ng-if="!messages.errorMessage && !messages.alertMessage" class="well text-danger" id="login_validation_errors"></p>
					<div>
						<div>
							<h4 class="item-update">{{currentEmail}}</h4>
		            	</div>
	            	</div>
	            	<div>
						<div class="login__input">
							<label>Loo uus salasõna</label>
							<input id="new_password" class="form-control recovery" ng-model="new_password" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$" type="password" name="new_password" value="" required>
		            	</div>
	            	</div>
	            	<div>
						<div class="login__input second">
							<label>Kinnita uus salasõna</label>
							<input id="repeat_new_password" class="form-control recovery" ng-model="repeat_new_password" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$" type="password" name="repeat_new_password" value="" required>
						</div>
					</div>
					<div>
						<div class="login__input">
							<div class="password-helper">Parool peab olema vähemalt 6 tähemärki pikk ning sisaldama ühte suurt tähte ja ühte numbrit.</div>
		            	</div>
	            	</div>
					<div class="login__button">
						<button type="submit" ng-click="changePassword(new_password, repeat_new_password, new_password_form)" class="btn btn-primary">Kinnita</button>
					</div>
					<div>
						<a href="" ng-click="openPasswordForm()">LOGI SISSE</a>
					</div>
	            </form>
			</div> 
		</section>
	</div><!--/container-->
</script>