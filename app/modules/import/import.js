App.controller('import', function($scope, $route, $http, $rootScope, $routeParams, $location, $filter, ModalService, $timeout, $interval, ngProgressFactory){
    
    $scope.messages = { alertMessage: false, errorMessage: false, workdaysAlert: false, updatedProjectId: false, monthDaysInvalid: false };
    
    $scope.$watch('selectedMonth', function (newValue, oldValue, scope) {
        $scope.importedProjects = false;
        $scope.importValues($scope.selectedMonth, '/projects');
    });
    // $scope.$watch('messages.monthDaysInvalid', function(){
    //     if($scope.messages.monthDaysInvalid) {
    //         $timeout(function () {
    //             $scope.messages.monthDaysInvalid = false;
    //         }, 2500);  
    //     }
    // });
    $scope.billableVal = [ "Unbillable", "Billable" ]
    $scope.selectedMonth = $rootScope.finalPreviousMonth;
    
    var prog;
    $scope.getProgress = function(selection, urlEnding){ 
        prog = $interval(function() {
            if(urlEnding.indexOf('import') != -1 && !$scope.importedProjects) {
                $http.get(settings.prefix+ 'months/' + selection + urlEnding + '/progress').then(function(response) { 
                    if(response.data.progress || response.data.progress == null) {
                        if(response.data.progress == 100) {
                            $scope.stopRequests();
                            $scope.progressbar.complete();
                        }
                        if(!$scope.progressbar) {
                            $scope.progressbar = ngProgressFactory.createInstance();
                            $scope.progressbar.setHeight('4px');
                            $scope.progressbar.setParent(document.getElementById('progressBar'));
                        }
                        if(response.data.progress != null) {
                            $scope.progressbar.set(response.data.progress);  
                        }
                    } else {
                        $scope.stopRequests();
                        $scope.progressbar.complete();
                    }
                });
            } else {
                $scope.stopRequests();
                $scope.progressbar.complete();
            }
        }, 1250);
    };
    $scope.stopRequests = function() {
        $interval.cancel(prog);
    };
    
    $scope.importValues = function(selection, urlEnding) {
        if(selection) {
            $scope.messages.workdaysAlert = false;
            $scope.disableLoader = false;
            $scope.importedProjects = false;
            if(urlEnding.indexOf('import') != -1) { 
                $scope.getProgress(selection, urlEnding);
            };
            
            $http.get(settings.prefix+ 'months/' + selection + urlEnding).then(function(response) {
                if(urlEnding.indexOf('import') != -1) { 
                    $scope.importWorkdays(selection);
                }
                $scope.messages.workdaysAlert = false;
                $scope.importedProjects = response.data;
                var projects = response.data;
                for(var item in projects) {
                    projects[item].minute_remainder = projects[item].minutes%60;
                    projects[item].hour_floored = Math.floor(projects[item].minutes/60);
                    if($rootScope.changedProjects) {
                        if($rootScope.changedProjects.includes(projects[item].id)) { projects[item].changed = true }
                    };
                }
                $rootScope.changedProjects = [];
            }, function(response) {
                $scope.disableLoader = true;
                var message = response.data.message;
		        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
            });
        };
    }
    
    $scope.updateProject = function(selection, project, type) {
        angular.forEach(project, function(value, key){
            if(!value && value != 0) { 
                project[key] = null;
            };
            if(value == "" && key == "comment") { 
                project[key] = null;
            };
        });
        if((type == 1 && (project.rate_salary < 0 || project.rate_salary === null)) || (type == 2 && (project.rate_statistic < 0 || project.rate_statistic === null))) { } else {
            $http({
    			method: "PUT",
    			url: settings.prefix+ 'months/' + selection + '/projects/' + project.id,
    			data: project,
    		}).then(function( response ){
    		    $scope.messages.updatedProjectId = project.id;
    		}, function(response){
    		    var message = response.data.message;
    	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
    		});
        };
    };
    
    $scope.setRateStatistic = function(project) {
        if(project.rate_statistic == null) { project.rate_statistic = project.rate_salary }
    };
    
    $scope.exportValues = function(selection) {
        $http({
			method: "PUT",
			url: settings.prefix+ 'months/' + selection + '/projects',
			data: $scope.importedProjects,
		}).then(function( response ){
			$location.path('/wages');
		}, function(response){
		    var message = response.data.message;
	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
		});
    };
    
    
    $scope.saveWorkdays = function(selection, monthDays, form) {
        if(form.monthDays.$valid) {
            $http({
    			method: "PUT",
    			url: settings.prefix+ 'months/' + selection + '/workdays',
    			data: {
    			    'workdays': monthDays,
    			}
    		}).then(function( response ){
    		  //  $scope.messages.alertMessage = response.data.message;
    		    $scope.messages.workdaysAlert = true;
    		}, function(response){
    		    var message = response.data.message;
    	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
    		});   
        } else {
            $scope.messages.monthDaysInvalid = true;
        }
    }
    
    $scope.importWorkdays = function(selection) {
        // $http.get(settings.prefix+ 'months/' + selection + '/workdays').then(function(response) {
        //     $scope.monthDays = response.data.workdays;
        // }, function(response) {
        //     var message = response.data.message;
	       // if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
        // });
    }
    
    $scope.promptImport = function(){
        ModalService.showModal({
            templateUrl: 'importPrompt.html',
            controller: "importPrompt"
        }).then(function(modal) {
            modal.scope.modalHeader = "Oled sa kindel?"
            modal.scope.selectedMonth = $scope.selectedMonth;
            modal.element.show();  
            modal.close.then(function(result) {
                if(result == 1) { $location.path('/wages'); };
                if(result == 0) { $scope.importValues($scope.selectedMonth, '/projects') };
                if(!result) {};
            });
        });
    };
});
