<script type="text/ng-template" id="import.tpl">
    <div class="container">
        <div class="row">  
            <main class="col-sm-12 pt-3 mt-2">
                <ng-include src="'breadcrumbs.tpl'"></ng-include>
                <error-message class="mt-4" ng-if="messages.errorMessage" fadeout=true error="messages.errorMessage"></error-message>
                <toalert-message class="mt-4" ng-if="messages.alertMessage" toalert="messages.alertMessage"></toalert-message>
                <div class="row mt-4">
                    <div class="col-12">
                        <div class="bordered-elem p-3">
                            <h5 class="form-control-label mb-3">Vali kuu projektide importimiseks</h5>
                            <div>
                                <input type="radio" ng-model="selectedMonth" ng-value="finalPreviousMonth" name="Month"> 
                                <span>{{previousMonthLabel}} (Eelmine kuu)</span><br>
                            </div>
                            <div>
                                <input type="radio" ng-model="selectedMonth" ng-value="currentMonth" name="Month"> 
                                <span>{{currentMonthLabel}} (Käesolev kuu)</span>
                            </div>
                            <div class="mt-3">
                                <a href="" class="btn btn-primary" ng-disabled="!selectedMonth" ng-click="importValues(selectedMonth, '/projects/import')">Impordi</a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-12 item-update" id='progressBar'></div>
                    
                    <div class="col-12 fade-element-in table-holder mt-4" ng-if="importedProjects">
                        <table class="table table-bordered table-striped table-hover"> <!--Table-->
                            <thead>
                                <tr><!--Headers-->
                                    <th width="25%">Projekt</th> 
                                    <th width="10%">Tunnid</th>
                                    <th width="10%">Staatus</th>
                                    <th width="12.5%">Grupp</th>
                                    <th width="10%" style="overflow:hidden;">Palgaarvestuse tunnihind</th>
                                    <th width="10%" style="overflow:hidden;">Statistika tunnihind</th>
                                    <th class="null-th" width="20%">Kommentaar</th>   
                                    <th class="null-th" width="0%"></th>   
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-class="{ 'bg-warning' : project.changed }" ng-repeat="project in importedProjects | orderBy:'group_name==null'">
                                    <td>
                                        <span>{{project.name}}</span>
                                        <span class="md-up-none"><small-alert-message ng-if="messages.updatedProjectId && project.id == messages.updatedProjectId" project="messages.updatedProjectId"></small-alert-message></span>
                                    </td>
                                    <td><span>{{project.hour_floored}}</span><span ng-if="project.minute_remainder != 0">:{{project.minute_remainder}}</span><span ng-if="project.minute_remainder == 0">:00</span>
                                    </td>
                                    <td>{{billableVal[project.billable]}}</td>
                                    <td>{{project.group_name}}</td>
                                    <td>
                                        <input valid-number type="text" class="form-control" ng-blur="updateProject(selectedMonth, project, 1);  setRateStatistic(project)" ng-model="project.rate_salary" name="Palgaarvestuse tunnihind" smart-float >
                                    </td>
                                    <td>
                                        <input valid-number type="text" class="form-control" ng-change="updateProject(selectedMonth, project, 2)" ng-model-options="{ debounce: 500 }" ng-model="project.rate_statistic" name="Statistika tunnihind" smart-float >
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" ng-change="updateProject(selectedMonth, project, 3)" ng-model-options="{ debounce: 500 }" ng-model="project.comment" name="kommentaar" >
                                    </td>
                                    <td class="md-none" style="position: absolute; right: -3rem; border: 0px solid white;">
                                        <small-alert-message ng-if="messages.updatedProjectId && project.id == messages.updatedProjectId" project="messages.updatedProjectId"></small-alert-message>
                                    </td>
                                </tr>
                            </tbody>
                        </table><!-- End of table-->
                    </div>
                    <div class="col-md-12 mt-4 fade-element-in" ng-if="importedProjects && (selectedMonth == finalPreviousMonth)" ng-hide="activeRole == 'USER'">
                         <a href="" class="btn btn-primary btn-custom ml-0 pull-right" ng-click="promptImport()">Edasi palgaarvestusse</a>
                    </div>
                    <div ng-if="!importedProjects && !disableLoader" class="spinner">
                        <div class="bounce1"></div>
                        <div class="bounce2"></div>
                        <div class="bounce3"></div>
                    </div>
                </div>
            </main><!--col9-->
                        
        </div><!--row-->
      </div><!--/container-->
</script>
