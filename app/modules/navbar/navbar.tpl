<script type="text/ng-template" id="navbar.tpl">
	<nav class="navbar navbar-toggleable-md navbar-inverse fixed-top bg-inverse">
		<button style="margin-top: 8px" class="navbar-toggler navbar-toggler-right hidden-lg-up" type="button" data-toggle="collapse" data-target="#navNav" ng-click="setCollapsed = !setCollapsed" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
        <a class="navbar-brand" href="/">
            <img src="/assets/img/logo.svg" width="160" height="50" alt="">
        </a>
		<a class="navbar-brand navbar-title md-none" href="/">
		   <span class="lg-none">Palgaarvestus ja statistika</span>
		   <span class="lg-up-none">Palgaarvestus</span>
		</a>
      
      <div ng-class="{ 'not-collapsed' : setCollapsed, 'is-collapsed' : !setCollapsed }" class="navbar-collapse" id="navNav">
         <ul class="navbar-nav mr-auto">
            <li class="nav-item">
               <a class="nav-link" ng-class="{'active' : currentLocation == '/'}" ng-click="setCollapsed = true" ng-href="/">Projektid</a>
            </li>
            <li class="nav-item" ng-hide="activeRole == 'USER'">
               <a class="nav-link" ng-class="{'active' : currentLocation == '/wages'}" ng-click="setCollapsed = true" ng-href="/wages">Palgaarvestus</a>
            </li>
            <li class="nav-item" ng-hide="activeRole == 'USER'">
               <a class="nav-link" ng-class="{'active' : currentLocation == '/statistics'}" ng-click="setCollapsed = true" ng-href="/statistics">Statistika</a>
            </li>
            <li class="nav-item">
               <a class="nav-link" ng-class="{'active' : currentLocation == '/archive'}" ng-click="setCollapsed = true" ng-href="/archive">Arhiiv</a>
            </li>
            <li class="nav-item" ng-hide="activeRole == 'USER'">
               <a class="nav-link" ng-class="{'active' : currentLocation == '/users' }" ng-click="setCollapsed = true" ng-href="/users">
                  <span class="lg-none">Töötajad</span>
                  <span class="lg-up-none">Töötajad</span>
               </a>
            </li>
            <li class="nav-item">
               <a class="nav-link" ng-class="{'active' : currentLocation == '/holidays' }" ng-click="setCollapsed = true" ng-href="/holidays">
                  <span class="lg-none">Eemalviibimised</span>
                  <span class="lg-up-none">Eemalviibimised</span>
               </a>
            </li>
            <li class="nav-item">
               <a class="nav-link" ng-class="{'active' : currentLocation == '/workdays' }" ng-click="setCollapsed = true" ng-href="/workdays">
                  <span>Riigipühad</span>
               </a>
            </li>
            <li class="nav-item md-up-none">
               <a class="nav-link" ng-click="promptImport(); setCollapsed = true">Abi</a>
            </li>
            <li class="nav-item md-up-none">
               <a class="nav-link md" ng-class="{'active' : currentLocation == '/profile'}" ng-click="setCollapsed = true" ng-href="/profile"><span class="fa fa-user"></span> Seaded</a>
            </li>
            <li class="nav-item md-up-none">
               <a class="nav-link" ng-href="" ng-click="logOut(); setCollapsed = true"><span class="fa fa-sign-out"></span> Logi välja</a>
            </li>
         </ul>    
      </div>
      <div class="dropdown md-none" uib-dropdown dropdown-append-to-body>
         <button id="btn-append-to-body" type="button" class="btn btn-secondary dropdown-toggle" uib-dropdown-toggle>
            <span class="lg-none">{{activeUser}}</span>
            <span class="caret"></span>
         </button>
         <div class="dropdown-menu dropdown-menu-right" uib-dropdown-menu role="menu" aria-labelledby="btn-append-to-body">
             <a class="dropdown-item" ng-click="promptImport(); setCollapsed = true">Abi</a>
             <a class="dropdown-item" ng-href="/profile">Seaded</a>
             <a class="dropdown-item" href="#" ng-click="logOut()">Logi välja</a>
         </div>
      </div>
	</nav>
</script>