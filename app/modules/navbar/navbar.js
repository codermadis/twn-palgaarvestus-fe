App.controller('navbar', function($scope, ModalService) {
    $scope.activeUser = sessionStorage.user;
    $scope.setCollapsed = true;
    $scope.promptImport = function(){
        ModalService.showModal({
            templateUrl: 'helpPrompt.html',
            controller: "helpPrompt"
        }).then(function(modal) {
            modal.element.show();  
            modal.close.then(function(result) { });
        });
    };
})