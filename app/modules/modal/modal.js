App.controller('wageEmail', function($scope, close, $http, $rootScope, $location){
    $scope.user = {};
    $scope.disableSend = false;
    $scope.userId = false;
    $scope.messages = { errorMessage: false, alertMessage: false };
    $scope.close = function(result) {
        close(result, 1000);
    };
    $scope.goToUser = function() {
        $location.path('wages/' + $scope.userId);
    };
    $scope.sendEmail = function() {
        $scope.disableSend = true;
        $http({
			method: "POST",
			url: settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users/' + $scope.userId + '/email',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).then(function( response ){
		    $scope.messages.alertMessage = response.data.message;
		    $scope.disableSend = false;
		    $scope.close(true);
		}, function(response){
		    $scope.disableSend = false;
		    if(response.data.message) {
		        $scope.messages.errorMessage = response.data.message;    
		    } else {
		        $scope.messages.errorMessage = "Esines viga"
		    }
		});
    };
});
App.controller('archiveEmail', function($scope, close, $http, $rootScope, $location){
    $scope.close = function(result) {
        close(result, 500);
    };
    $scope.downloadDocument = function(id) {
        console.log(id)
        window.open(settings.prefix + 'archive/files/' + id, '_blank');
    }
    $scope.downloadVacationDocument = function(id) {
        console.log(id)
        window.open(settings.prefix + 'vacations/' + id + '/download' , '_blank');
    }
});
App.controller('accountantPrompt', function($scope, close, $http, $rootScope, $location){
    $scope.close = function(result) {
        close(result, 500);
    };
});
App.controller('accountantEmail', function($scope, close, $http, $rootScope, $location, Upload, $timeout, $filter){
    $scope.disableSend = false;
    $scope.messages = { errorMessage: false, alertMessage: false };
    $scope.model = {};
    $scope.tinymceOptions = {
        theme: "modern",
        plugins: [
            "advlist autolink lists charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code",
            "insertdatetime nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor"
        ],
        toolbar1: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | print preview media | forecolor backcolor emoticons",
        height: "300px",
    };	
    $scope.close = function(result) {
        close(result, 1000);
    };
    $scope.downloadDocument = function(id) {
        console.log(id)
        window.open(settings.prefix + 'archive/files/' + id, '_blank');
    }
    $scope.downloadVacationDocument = function(id) {
        console.log(id)
        window.open(settings.prefix + 'vacations/' + id + '/download' , '_blank');
    }
    $scope.deleteDocument = function(id) {
        $http.delete(settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/accounting/files/' + id).then(function(response){
            $scope.messages.alertMessage = response.data.message;
            for(var item in $scope.model.files) {
                if($scope.model.files[item].id == id) {
                    $scope.model.files.splice(item, 1);    
                }
            }
        })
    }
    $scope.uploadFiles = function(files, errFiles) {
        $scope.files = files;
        $scope.errFiles = errFiles;
        angular.forEach(files, function(file) {
            file.upload = Upload.upload({
                url: settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/accounting/files' ,
                data: {file: file}
            });

            file.upload.then(function (response) {
                $timeout(function () {
                    file.result = response.data;
                });
                
                var newfile = {};
                newfile['originalname'] = file.name;
                newfile['id'] = response.data.file_id;
                newfile['removable'] = true;
                $scope.model.files.push(newfile);
            
            }, function (response) {
                if(response.data.message) {
                    $scope.messages.errorMessage = response.data.message;    
                } else {
                    $scope.messages.errorMessage = "Esines viga"
                }
            });
        });
    }
    $scope.sendEmail = function() {
        $scope.disableSend = true;
        $http({
        	method: "POST",
        	url: settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/accounting/email',
        	headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        	data: $scope.model,
        }).then(function( response ){
            $scope.disableSend = false;
            $scope.messages.alertMessage = response.data.message;
            $scope.close(true);
        }, function(response){
            $scope.disableSend = false;
            if(response.data.message) {
                $scope.messages.errorMessage = response.data.message;    
            } else {
                $scope.messages.errorMessage = "Esines viga"
            }
        });
    };
});
App.controller('addHoliday', function($scope, close, $http, $rootScope, $location, $filter, $timeout, $routeParams){

    $scope.replaceFile = false;
    $scope.vacationData = {};
    $scope.userName = {};
    $scope.editView = false;
    $scope.model = {
        type: 'vacation' // form pre-filled
    };
    $scope.dateFields = [{}];
    $scope.autoModel = {};
    $scope.userId = false;
    $scope.messages = { errorMessage: false, alertMessage: false };
    $scope.popup_start = [false];
    $scope.popup_end = [false];
    $scope.openStart = function(index) {
        $scope.popup_start[index] = true;
    };
    $scope.openEnd = function(index) { 
        $scope.popup_end[index] = true;
    };
    
    $scope.addDateField = function() { 
        $scope.dateFields.push({});
    };
    
    $scope.close = function(result) {
        close(result, 1000);
    };
    $scope.setEndDate = function(index) {
        $scope.model.end = [];
        var endDate = new Date($scope.model.start[index].getTime() + 6 * 86400000 );
        $scope.model.end[index] = endDate;
        $scope.openEnd(index);
    }
    $scope.setViewEndDate = function(index) {
        $scope.vacationData.end_date = new Date( $scope.vacationData.start_date.getTime() + 6 * 86400000 );
        $scope.openEnd(index);
    }
    $scope.getUser = function(val) {
        return $http.get(settings.prefix+ 'users/autocomplete', {
            params: {
                search: val,
            }
        }).then(function(response){
            return response.data.map(function(item){
                return item;
            });
        });
    };
    
    $scope.deleteHoliday = function(id) {
        $http.delete(settings.prefix+ 'vacations/' + id).then(function(response){
            $scope.messages.alertMessage = "Puhkus kustutatud!";
            $scope.close(true); 
        }, function(response) {
            $scope.messages.errorMessage = response.data.message;
        });
    };
    
    $scope.saveHoliday = function(form, uploadedFile) {
        if(form.$valid) {
            var submitData = {};
            var startValues = [];
            var endValues = [];
            submitData = angular.copy($scope.model);
            submitData['user_id'] = $scope.autoModel.user.id;
            angular.forEach(submitData.start, function(value, key) {
                var val = $filter('date')(value, 'yyyy-MM-dd');
                startValues.push(val);
            });
            angular.forEach(submitData.end, function(value, key) {
                var val = $filter('date')(value, 'yyyy-MM-dd');
                endValues.push(val);
            });
            submitData['start_date'] = startValues.join(',');
            submitData['end_date'] = endValues.join(',');
            delete submitData['end'];
            delete submitData['start'];
            
            var fd = new FormData();
            fd.append('file', uploadedFile);
            var data ={};
            
            angular.forEach(submitData, function(value, key){
                if(submitData[key] && submitData[key] != null) {
                    data[key] = value;
                    fd.append(key, data[key]);  
                }
            });
            $http.post(settings.prefix+ 'vacations', fd, {
                headers : {
                    'Content-Type' : undefined
                },
                transformRequest : angular.identity
            }).success(function(response) {
                $scope.datepickerFirstError = false;
    		    $scope.datepickerSecondError = false;
    		    if(response.data && response.data.message) {
    		        $scope.messages.alertMessage = response.data.message;    
    		    } else { $scope.messages.alertMessage = response };
    			$scope.close(true); 
            }).error(function(response) {
                console.log(response)
                if(response && response.message) {
    		        $scope.messages.errorMessage = response.message;    
    		    } else { $scope.messages.errorMessage = response };
            });  
            
        } else {
            if(angular.element(".datepicker-element-1").hasClass('ng-invalid-required')) { $scope.datepickerFirstError = true; } else { $scope.datepickerFirstError = false; }
            if(angular.element(".datepicker-element-2").hasClass('ng-invalid-required')) { $scope.datepickerSecondError = true; } else { $scope.datepickerSecondError = false; }
        }
    }
    $scope.updateHoliday = function(form, uploadedFile) {
        if(form.$valid) {
            var fd = new FormData();
            if(uploadedFile) {
                fd.append('file', uploadedFile);
            };
            var data ={};
            
            angular.forEach($scope.vacationData, function(value, key){
                if($scope.vacationData[key] && $scope.vacationData[key] != null) {
                    if(value instanceof Date) {
                        value = $filter('date')(value, 'yyyy-MM-dd');
                    }
                    data[key] = value;
                    fd.append(key, data[key]);  
                }
            });
            $http.put(settings.prefix+ 'vacations/' + $scope.vacationData.id, fd, {
                headers : {
                    'Content-Type' : undefined
                },
                transformRequest : angular.identity
            }).success(function(response) {
                $scope.datepickerFirstError = false;
    		    $scope.datepickerSecondError = false;
    		    if(response.data && response.data.message) {
    		        $scope.messages.alertMessage = response.data.message;    
    		    } else { $scope.messages.alertMessage = response };
    			$scope.close(true); 
            }).error(function(response) {
                if(response.data && response.data.message) {
    		        $scope.messages.errorMessage = response.data.message;    
    		    } else { $scope.messages.errorMessage = response };
            });  
            
        } else {
            if(angular.element(".datepicker-element-1").hasClass('ng-invalid-required')) { $scope.datepickerFirstError = true; } else { $scope.datepickerFirstError = false; }
            if(angular.element(".datepicker-element-2").hasClass('ng-invalid-required')) { $scope.datepickerSecondError = true; } else { $scope.datepickerSecondError = false; }
        }
    }
    $scope.downloadDocument = function(id) {
        window.open(settings.prefix + 'vacations/' + id + '/download', '_blank');
    }
});
App.controller('importPrompt', function($scope, close, $http, $rootScope, $location){
    $scope.messages = { errorMessage: false, alertMessage: false };
    $scope.exportValues = function(selection) {
        $scope.setLoader = true;
        $scope.loading = true;
        $http({
			method: "PUT",
			url: settings.prefix+ 'months/' + selection + '/projects',
			data: $scope.importedProjects,
		}).then(function( response ){
		    $scope.loading = false;
		    var message = response.data.message;
		    $scope.messages.alertMessage = message;
		    $scope.close(1);
		}, function(response){
		    $scope.loading = false;
		    var message = response.data.message;
		    $rootScope.changedProjects = response.data.project_ids;
	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText } 
	        $scope.close(0);
		});
    };
    
    $scope.close = function(result) {
        close(result, 2500);
    };
});
App.controller('helpPrompt', function($scope, close){
    $scope.close = function(result) {
        close(result, 500);
    };
});
App.controller('userConfirmModal', function($scope, close, $http, $rootScope, $location, $filter){
    $scope.messages = { errorMessage: false, alertMessage: false };
    $scope.user = {};
    $scope.add = function() {
        var submitData = {};
        submitData = $scope.user;
        submitData['active_since'] = $filter('date')($scope.user.active_since, 'yyyy-MM-dd');
        $http({
			method: "POST",
			url: settings.prefix+ 'users',
			data: submitData,
		}).then(function( response ){
			$scope.close(true);
		}, function(response){
		    var message = response.data.message;
	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
		});
    }
    
    $scope.close = function(result) {
        close(result, 500);
    };
});
App.controller('wagePrompt', function($scope, close, $http, $rootScope, $location){
    $scope.messages = { errorMessage: false, alertMessage: false };
    $scope.resetUserWage = function() {
        $scope.setLoader = true;
        $scope.loading = true;
        $http({
			method: "PUT",
			url: settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users/' + $scope.userId + '/reset'
		}).then(function( response ){
		    $scope.loading = false;
		    var message = response.data.message;
		    $scope.messages.alertMessage = message;
		    $scope.close(true);
		}, function(response){
		    $scope.loading = false;
		    var message = response.data.message;
	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
		});
    }
    $scope.close = function(result) {
        close(result, 500);
    };
});
App.controller('addProject', function($scope, close, $http, $rootScope, $routeParams){
    $scope.model = {};
    $scope.autoModel = {};
    $scope.projectForm = {};
    $scope.model.billable = 1;
    $scope.billableValues = [
        { name: 'Billable', value: 1 },
        { name: 'Unbillable', value: 0 }
    ]
    $scope.getProject = function(val) {
        return $http.get(settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/projects/search/auto', {
            params: {
                name: val,
            }
        }).then(function(response){
            return response.data.map(function(item){
                return item;
            });
        });
    };
    $scope.setProjectProperties = function(item) {
        angular.forEach(item, function(value, key) {
            $scope.model[key] = value;
        })
    }
    $scope.close = function(result) {
        close(result, 500);
    };
    $scope.add = function(form) {
        if(form.$valid) {
            var submitModel = angular.copy($scope.model);
            if(!submitModel.name && typeof $scope.autoModel.selectedProject != 'object') { submitModel.name = $scope.autoModel.selectedProject; }
            var submitData = [];
            submitModel['minutes'] = submitModel['hours']*60;
            delete submitModel['hours'];
            submitData.push(submitModel);
            $http({
    			method: "POST",
    			url: settings.prefix+ 'months/' + $rootScope.finalPreviousMonth + '/users/' + $routeParams.id + '/projects',
    			data: submitData,
    		}).then(function( response ){
    			$scope.close(true);
    		}, function(response){
    		    var message = response.data.message;
    	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
    		});
        };
    }
});
App.controller('userModal', function($scope, close, $route, $http, $rootScope, $filter, $location, ModalService){
    
    $scope.messages = { alertMessage: false, errorMessage: false };
    
    $scope.addedFile = {};
    $scope.setVatMin = false;
    $scope.userForm = {};
    $scope.user = {};
    $scope.editForm = false;
    $scope.salaryVal = ['Põhipalk', 'Tunnipõhine', 'Valemipõhine']
    $scope.activatedVal = [false, true];
    $scope.popup = { first: false, second: false };
    $scope.open_first = function() { $scope.popup.first = true; };
    $scope.open_second = function() { $scope.popup.second = true; };
    $scope.dateOptions = { startingDay: 1 };
    
    $scope.close = function(result) {
        close(result, 1000);
    };
    
    $scope.reloadThis = function() {
        $route.reload();
    }
    $scope.downloadDocument = function(id, path) {
        window.open(settings.prefix + 'users/files/' + id + '/download', '_blank');
    }
    $scope.deleteDocument = function(id) {
        $http.delete(settings.prefix+ 'users/files/' + id).then(function(response){
            $scope.messages.alertMessage = response.data.message;
            for(var item in $scope.user.files) {
                if($scope.user.files[item].id == id) {
                    $scope.user.files.splice(item, 1);    
                }
            }
        })
    }
    $scope.setAdmin = function(value) {
        var submitData = {
            "admin": value
        };
        $http({
			method: "PUT",
			url: settings.prefix+ 'users/' + $scope.user.id + '/admin',
			data: submitData,
		}).then(function( response ){
            $scope.messages.alertMessage = response.data.message;
		}, function(response){
		    var message = response.data.message;
	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
		});
    };
    $scope.edit = function(user, form, uploadedFile) {
        if(form.$valid) {
            var submitData = {};
            submitData = angular.copy(user);
            submitData['active_since'] = $filter('date')(user.active_since, 'yyyy-MM-dd');
            submitData['vat_min_since'] = $filter('date')(user.vat_min_since, 'yyyy-MM-dd');
            
            var fd = new FormData();
            fd.append('file', uploadedFile); 
            var data ={};    
            
            angular.forEach(submitData, function(value, key){
                if(submitData[key] || submitData[key] == 0) {
                    data[key] = value;
                    fd.append(key, data[key]);  
                }
            });
            $http.put(settings.prefix+ 'users/' + submitData.id, fd, {
                headers : {
                    'Content-Type' : undefined
                },
                transformRequest : angular.identity
            }).success(function(response) {
                $scope.datepickerFirstError = false;
    		    $scope.datepickerSecondError = false;
    		    $scope.roleError = false;
    		    if(response.data && response.data.message) {
    		        $scope.messages.alertMessage = response.data.message;    
    		    } else { $scope.messages.alertMessage = response };
    			$scope.close(true); 
            }).error(function(response) {
                $scope.messages.errorMessage = response.message;
            });  
        } else {
            if(angular.element(".datepicker-element-1").hasClass('ng-invalid-required')) { $scope.datepickerFirstError = true; } else { $scope.datepickerFirstError = false; }
            if(angular.element(".datepicker-element-2").hasClass('ng-invalid-required')) { $scope.datepickerSecondError = true; } else { $scope.datepickerSecondError = false; }
            if(angular.element("#role").hasClass('ng-invalid-required')) { $scope.roleError = true; } else { $scope.roleError = false; }
        }
    }
    
    $scope.add = function(form) {
        var submitData = {};
        submitData = angular.copy($scope.user);
        submitData['active_since'] = $filter('date')($scope.user.active_since, 'yyyy-MM-dd');
        $http({
			method: "POST",
			url: settings.prefix+ 'users',
			data: submitData,
		}).then(function( response ){
		    $scope.messages.alertMessage = response.data.message;
			$scope.close(true);
		}, function(response){
		    var message = response.data.message;
	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
		});
    }
    
    $scope.openUserConfirm = function(form, uploadedFile){
        if(form.$valid) {
            $http.get(settings.prefix + 'users?' + 'first_name=' + $scope.user['first_name'] + '&last_name=' + $scope.user['last_name']).then(function(response) {
                var submitData = {};
                submitData = angular.copy($scope.user);
                submitData['active_since'] = $filter('date')($scope.user.active_since, 'yyyy-MM-dd');
                submitData['vat_min_since'] = $filter('date')($scope.user.vat_min_since, 'yyyy-MM-dd');
                
                var fd = new FormData();
                if(uploadedFile && uploadedFile != null) {
                    fd.append('file', uploadedFile);     
                }
                var data ={};    
                
                angular.forEach(submitData, function(value, key){
                    if(submitData[key] && submitData[key] != null) {
                        data[key] = value;
                        fd.append(key, data[key]);  
                    }
                });
                $http.post(settings.prefix+ 'users', fd, {
                    headers : {
                        'Content-Type' : undefined
                    },
                    transformRequest : angular.identity
                }).success(function(response) {
                    $scope.datepickerFirstError = false;
        		    $scope.datepickerSecondError = false;
        		    $scope.roleError = false;
        		    if(response.data && response.data.message) {
        		        $scope.messages.alertMessage = response.data.message;    
        		    } else { $scope.messages.alertMessage = response };
        			$scope.close(true); 
                }).error(function(response) {
                    $scope.datepickerFirstError = false;
        		    $scope.datepickerSecondError = false;
        		    $scope.roleError = false;
                    if(response.data && response.data.message) {
        		        $scope.messages.errorMessage = response.data.message;    
        		    } else { $scope.messages.errorMessage = response };
                });  
            }, function(response) {
                ModalService.showModal({
                    templateUrl: 'userConfirmModal.html',
                    controller: "userConfirmModal"
                }).then(function(modal) {
                    modal.scope.modalHeader = "Kasutaja juba olemas";
                    modal.scope.user = $scope.user;
                    modal.element.show();  
                    modal.close.then(function(result) {
                        if(result) { $scope.close(true); };
                    });
                });
            });
        } else {
            if(angular.element(".datepicker-element-1").hasClass('ng-invalid-required')) { $scope.datepickerFirstError = true; } else { $scope.datepickerFirstError = false; }
            if(angular.element(".datepicker-element-2").hasClass('ng-invalid-required')) { $scope.datepickerSecondError = true; } else { $scope.datepickerSecondError = false; }
            if(angular.element("#role").hasClass('ng-invalid-required')) { $scope.roleError = true; } else { $scope.roleError = false; }
        }
    };
});