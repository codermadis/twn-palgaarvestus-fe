<script id="profile.tpl" type="text/ng-template">
    <div class="container profile">
        <div class="row">
            <main class="col-sm-12 pt-3 mt-2">
                <ng-include src="'breadcrumbs.tpl'"></ng-include>
                <error-message class="mt-4" ng-if="messages.errorMessage" fadeout=true error="messages.errorMessage"></error-message>
                <toalert-message class="mt-4" ng-if="messages.alertMessage" toalert="messages.alertMessage"></toalert-message>
                <div class="row animate-if mt-4">
                    <div class="col-md-12 header main-header mb-3">
                        <h4>Seaded</h4>
                    </div>
                </div>
                <div class="row animate-if">
                    <div class="col-md-12 header main-header">
                        <ul class="nav nav-tabs">
        				  <li class="nav-item">
        				    <a ng-class="{ 'active' : actives.firstActive }" ng-click="actives.firstActive = true; actives.thirdActive = false; actives.fourthActive = false; actives.secondActive = false; actives.fifthActive = false;" class="nav-link" href="">Konto seaded</a>
        				  </li>
        				  <li class="nav-item" ng-hide="activeRole == 'USER'">
        				    <a ng-class="{ 'active' : actives.secondActive }" ng-click="actives.firstActive = false; actives.thirdActive = false; actives.fourthActive = false; actives.secondActive = true; actives.fifthActive = false;" class="nav-link" href="">Meiliteenus</a>
        				  </li>
        				  <li class="nav-item" ng-hide="activeRole == 'USER'">
        				    <a ng-class="{ 'active' : actives.thirdActive }" ng-click="actives.firstActive = false; actives.secondActive = false; actives.fourthActive = false; actives.thirdActive = true; actives.fifthActive = false;" class="nav-link" href="">IP haldus</a>
        				  </li>
        				  <li class="nav-item" ng-hide="activeRole == 'USER'">
        				    <a ng-class="{ 'active' : actives.fourthActive }" ng-click="actives.firstActive = false; actives.secondActive = false; actives.thirdActive = false; actives.fourthActive = true; actives.fifthActive = false;" class="nav-link" href="">Koopia saajad</a>
        				  </li>
        				  <li class="nav-item" ng-hide="activeRole == 'USER'">
        				    <a ng-class="{ 'active' : actives.fifthActive }" ng-click="actives.firstActive = false; actives.secondActive = false; actives.thirdActive = false; actives.fourthActive = false; actives.fifthActive = true;" class="nav-link" href="">Raamatupidaja e-mail</a>
        				  </li>
        				</ul>
                    </div>
                    <div ng-if="actives.firstActive" class="col-lg-6 col-md-12 col-sm-12 fade-element-in">
                        <h5 class="header">Konto seaded</h5>
                        <div class="row field">
                            <div class="col-md-6">
                                <div class="bold">E-mail</div>
                            </div>
                            <div class="col-md-6">{{activeUser}}</div>
                        </div>
                        <h5 class="header">Muuda salasõna</h5>
                        <form id="password_form" name="password_form">
                            <!--<div class="row field">
                                <div class="col-md-6">
                                    <div class="config-label bold">Kehtiv salasõna</div>
                                </div>
                                <div class="col-md-6">
                                    <input class="form-control" type="password" id="password" pattern=".{6,}" ng-model="password" >
                                </div>
                            </div>-->
                            <div class="row field">
                                <div class="col-md-12">
                                    <span class="password-helper">Parool peab olema vähemalt 6 tähemärki pikk ning sisaldama ühte suurt tähte ja ühte numbrit.</span>
                                </div>
                            </div>
                            <div class="row field">
                                <div class="col-md-6">
                                    <div class="config-label bold">Uus salasõna</div>
                                </div>
                                <div class="col-md-6">
                                    <input class="form-control" type="password" id="new_password" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$" ng-model="pw_model.new_password" required>
                                </div>
                            </div>
                            <div class="row field">
                                <div class="col-md-6">
                                    <div class="config-label bold">Kinnita uus salasõna</div>
                                </div>
                                <div class="col-md-6">
                                    <input class="form-control" type="password" id="confirm_new_password" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$" ng-model="pw_model.confirm_new_password" required>
                                </div>
                            </div>
                            <div class="row field">
                                <div class="col-md-12">
                                    <div class="form-controls right-align">
                                        <button type="submit" ng-click="changePassword(pw_model.new_password, pw_model.confirm_new_password, password_form)" class="btn btn-primary">Salvesta</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div> <!--col-12-->
                    
                    <div ng-if="actives.secondActive && statusReceived" ng-hide="activeRole == 'USER'" class="col-lg-6 col-md-12 col-sm-12 fade-element-in">
                        <h5 class="header">Meiliteenus</h5>
                        <div class="row field">
                            <div class="col-md-6">
                                <div class="bold">Teenus</div>
                            </div>
                            <div class="col-md-6">
                                <span>MS Outlook 2016</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <h5 class="header mb-0">Määra Outlook365 salasõna</h5>
                            </div>
                            <div class="col-md-6">
                                <span class="item-update" ng-if="outlookPasswordStatus"> (salasõna määratud)</span>
                                <span class="text-warning" ng-if="!outlookPasswordStatus"> (salasõna määramata)</span>
                            </div>
                        </div>
                        <form id="outlook_form" name="outlook_form">
                            <div class="row field">
                                <div class="col-md-6">
                                    <div class="config-label bold">Meilikonto salasõna</div>
                                </div>
                                <div class="col-md-6">
                                    <input placeholder="{{outlookPlaceholder}}" class="form-control" type="password" id="outlook" ng-model="outlook" >
                                </div>
                            </div>
                            <div class="row field">
                                <div class="col-md-12">
                                    <div class="form-controls right-align">
                                        <button type="button" ng-click="setOutlookPassword(outlook, outlook_set)" class="btn btn-primary">Salvesta</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div> <!--col-12-->
                    
                    <div ng-if="actives.thirdActive && addressList" ng-hide="activeRole == 'USER'" class="col-lg-6 col-md-12 col-sm-12 fade-element-in">
                        <h5 class="header mb-1">Halda IP aadresse</h5>
                        <div class="row field">
                            <div class="col-12">
                                <div class="ip_big_label bold mt-2">Lülita IP piirang sisse</div>
                                <div class="ip_switch">
                                    <label class="switch">
                                        <input type="checkbox" ng-model="ipCheckStatus" ng-checked="ipCheckStatus" ng-change="changeStatus(ipCheckStatus)">
                                        <div class="slider round"></div>
                                    </label>
                                </div> 
                            </div>
                        </div>
                        <div class="row field">
                            <div class="col-md-12">
                                <div class="bold">Süsteemi salvestatud IP aadressid</div>
                            </div>
                            <div class="col-md-12">
                                <form id="ip_list" name="ip_list">
                                    <div class="row field" ng-repeat="item in addressList">
                                        <div class="col-md-12 col-sm-12 list-item">
                                            <span class="form-control ip-input pointer-none">{{item}}</span>
                                            <span class="ip-action"><a href="" ng-click="removeAddressListItem($index)"><i class="fa fa-close address-close user-alert-text"></i></a></span>
                                        </div>
                					</div>
                					<div class="row field" ng-if="!addressList.length">
                                        <div class="col-md-12 col-sm-12">
                                            <span class="ip-input">Puuduvad</span>
                                        </div>
                					</div>
                                </form>
                            </div>
                        </div>
                        <div class="row field">
                            <div class="col-md-12">
                                <div class="bold">Lisa uus IP aadress</div>
                            </div>
                            <div class="col-md-12">
                                <form id="ip_submit" name="ip_submit">
                                    <div class="row field" ng-repeat="item in additionList">
                                        <div class="col-md-12 col-sm-12">
                                            <input valid-number class="form-control ip-input" type="text" id="address" ng-model="additionList[$index].address" >
                						</div>
                					</div>
                					
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-controls right-align">
                                                <button type="button" ng-click="saveAddresses()" class="btn btn-primary">Salvesta</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div> <!--col-12-->
                    
                    <div ng-if="actives.fourthActive" ng-hide="activeRole == 'USER'" class="col-lg-6 col-md-12 col-sm-12 fade-element-in">
                        <h5 class="header">Palgakokkuvõtte koopiate saajad</h5>
                        <div class="row field">
                            <div class="col-md-12">
                                <div class="bold">Meilikontod</div>
                            </div>
                            <div class="col-md-12">
                                <form id="cc_list" name="cc_list">
                                    <div class="row field" ng-repeat="recipient in emailRecipients">
                                        <div class="col-md-12 col-sm-12 list-item">
                                            <input name="recipient_{{$index}}" type="email" class="form-control ip-input" ng-model="recipient.email" ng-blur="saveEmailRecipients()">
                                            <span ng-if="$index != 0" class="ip-action"><a href="" ng-click="removeRecipient($index)"><i class="fa fa-close address-close user-alert-text"></i></a></span>
                                        </div>
                					</div>
                					<div class="row field">
                						<div class="col-md-12 col-sm-12">
                						    <span><a href="" ng-click="addRecipient()"><i class="fa fa-plus address-close item-update"></i> Lisa väli</a></span>
                						</div>
                                    </div>
                					
                                </form>
                            </div>
                        </div>
                    </div> <!--col-12-->
                    
                    <div ng-if="actives.fifthActive" ng-hide="activeRole == 'USER'" class="col-lg-6 col-md-12 col-sm-12 fade-element-in">
                        <h5 class="header mb-3">Raamatupidaja e-mail</h5>
                        <form id="outlook_form" name="outlook_form">
                            <div class="row field">
                                <div class="col-md-6">
                                    <div class="config-label bold">E-mail</div>
                                </div>
                                <div class="col-md-6">
                                    <input class="form-control" type="email" id="accountant" ng-model="accountant_model.email" >
                                </div>
                            </div>
                            <div class="row field">
                                <div class="col-md-12">
                                    <div class="form-controls right-align">
                                        <button type="button" ng-click="setAccountant(accountant_model.email)" class="btn btn-primary">Salvesta</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div> <!--col-12-->
                    
                </div><!--inside row-->
            </main><!--col9-->
        </div><!--row-->
    </div><!--/container-->
</script>