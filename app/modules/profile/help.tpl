<script type="text/ng-template" id="help.tpl">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">TW palgaarvestus ja statistika kasutusjuhend</h4>
                <button ng-disabled="loading" type="button" class="close" ng-click="close(false)" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <h5 class="mb-3">
                    SISSE- JA VÄLJALOGIMINE
                </h5>
                <p>
                    <strong>1. </strong>
                    <strong>Süsteemi sisenemine</strong>
                </p>
                <p>
                    Süsteemi on lubatud siseneda kasutajal, kes on määratud administraatoriks
                    ja kes siseneb süsteemi lubatud IP aadressilt. Mittelubatud IP aadressi
                    korral keelatakse ligipääs süsteemile.
                </p>
                <p><img src="/assets/img/pajuhend/Picture1.png"></p>
                <p>
                    <em>
                        Vaade 1: IP aadress puudub süsteemis ja seetõttu on ligipääs süsteemile
                        keelatud.
                    </em>
                </p>
                <p>
                    Sisselogimine toimub töö (eesnimi.perekonnanimi@twn.ee) e-maililt ja
                    kasutajale süsteemi administraatori poolt määratud või kasutaja enda poolt
                    loodud salasõnaga.
                </p>
                <p>
                    <strong>2. </strong>
                    <strong>Sisselogimise andmete salvestamine</strong>
                </p>
                <p>
                Sisselogimise andmete talletamiseks märkida brauserisse    <strong>linnuke „Jäta mind meelde“ lahtrisse</strong>.
                </p>
                <p><img src="/assets/img/pajuhend/Picture2.png"></p>
                <p>
                    <em>Vaade 2: brauserisse sisselogimise andmete salvestamine.</em>
                </p>
                <p>
                    <strong>3. </strong>
                    <strong>Uue salasõna saamine</strong>
                </p>
                <p>
                    <strong>Salasõna unustamise</strong>
                    korral vajutada <strong>lingile „Unustasid salasõna?“</strong>, sisestada
                    uuesti oma töö e-mail ning vajutada „Saada“ nupule. Süsteemi lisatud ja
                    ligipääsu omava kasutaja e-mailile saadetakse automaatne e-mail uue
                    salasõna määramiseks.
                </p>
                <p><img src="/assets/img/pajuhend/Picture3.png"></p>
                <p>
                    <em>
                        Vaade 3: salasõna unustamise korral vajutada lingile „Unustasid
                        salasõna?“.
                    </em>
                </p>
                <p><img src="/assets/img/pajuhend/Picture4.png"></p>
                <p>
                    <em>
                        Vaade 4: salasõna unustamise korral saadetakse süsteemi kasutajale
                        automaatne e-mail uue salasõna määramiseks.
                    </em>
                </p>
                <p>
                    <strong>4. </strong>
                    <strong>Salasõna muutmine</strong>
                </p>
                <p>
                    Salasõna saab muuta <strong>konto seadete</strong> alt, mis avaneb menüült
                    kasutaja e-maili nupu alt. Salasõna peab vastama nõutud vormingule.
                </p>
                <p><img src="/assets/img/pajuhend/Picture5.png"></p>
                <p>
                    <em>Vaade 5: seadete lehekülg avaneb kasutaja e-maili nupu alt.</em>
                </p>
                <p>
                    <strong>5. </strong>
                    <strong>Väljalogimine</strong>
                </p>
                <p>
                    Süsteemist väljumiseks avada kasutaja e-maili nupu alt avanev alammenüü
                    ning vajutada „Logi välja“ nupule. Brauseri sulgemisel logitakse kasutaja
                    süsteemist välja, kuid vahelehe sulgemisel jääb kasutussessioon avatuks.
                </p>
                <p><img src="/assets/img/pajuhend/Picture6.png"></p>
                <p>
                    <em>Vaade 6: väljalogimise nupp avaneb kasutaja e-maili nupu alt.</em>
                </p>
                <p>
                    <em> </em>
                </p>
                <h5 class="mb-3">
                    <a name="_Toc484170078">SEADISTUSED</a>
                </h5>
                <p>
                    <strong>1. </strong>
                    <strong>Meilikonto seadistamine </strong>
                </p>
                <p>
                Süsteemist e-maili väljasaatmiseks peab kasutaja süsteemis    <strong>määrama oma Outlook’i salasõna</strong>. Meilikonto salasõna
                    määramine toimub seadete leheküljelt meiliteenuse vormi kaudu. Meilikonto
                    salasõna määramata jätmise korral kuvatakse palgaarvestuse leheküljel
                    töötaja e-maili väljasaatmisel veateade.
                </p>
                <p><img src="/assets/img/pajuhend/Picture7.png"></p>
                <p>
                    <em>
                        Vaade 7: meiliteenuse vorm seadete leheküljel, mis avaneb kasutaja
                        e-maili nupu alt.
                    </em>
                </p>
                <p><img src="/assets/img/pajuhend/Picture8.png"></p>
                <p>
                    <em>
                        Vaade 8: veateade palgakokkuvõtte väljasaatmise eelselt, mis kuvatakse
                        juhul, kui kasutaja pole seadete leheküljel määranud oma Outlook’i
                        konto salasõna.
                    </em>
                </p>
                <p>
                    <strong>2. </strong>
                    <strong>Süsteemi konto seadistamine </strong>
                </p>
                <p>
                    Konto seadete all kuvatakse kasutajakonto e-mail ning seal saab muuta oma
                    salasõna. Salasõna peab vastama nõutud vormingule.
                </p>
                <p><img src="/assets/img/pajuhend/Picture9.png"></p>
                <p>
                    <em>
                        Vaade 9: seadete leheküljel saab vaadata oma kasutajakonto e-maili ning
                        saab muuta salasõna.
                    </em>
                </p>
                <p>
                    <strong>3. </strong>
                    <strong>IP haldus</strong>
                </p>
                <p>
                    Seadete leheküljel on võimalik hallata IP aadresse. IP halduse vormi kaudu
                    saab süsteemi lisada IP aadresse, mille kaudu on lubatud süsteemi siseneda.
                    Muudelt IP aadressidelt on ligipääs süsteemi keelatud. Lisatud IP aadresse
                    saab kustutada, vajutades aadressi järel olevale „x“ ikoonile.
                </p>
                <p><img src="/assets/img/pajuhend/Picture10.png"></p>
                <p>
                    <em>Vaade 10: IP halduse vorm seadete leheküljel.</em>
                </p>
                <p>
                    <strong>4. </strong>
                    <strong>E-maili koopia saajate haldus</strong>
                </p>
                <p>
                Töötaja ja raamatupidaja e-mailile lisatakse juurde ka    <strong>koopia saajate e-mailid</strong>, kui need on süsteemis sisestatud.
                    Kasutaja saab lisada, muuta ja kustutada e-maili koopia saajate
                    elektronaadresse, mille haldamine toimub seadete leheküljel oleva koopia
                    saajate vormi kaudu.
                </p>
                <p><img src="/assets/img/pajuhend/Picture11.png"></p>
                <p>
                    <em>
                        Vaade 11: süsteemist väljasaadetavale ja genereeritavale e-mailile saab
                        juurde lisada koopia saajaid seadetel oleva vormi kaudu.
                    </em>
                </p>
                <h5 class="mb-3">
                    <a name="_Toc484170079">TÖÖTAJATE HALDUS</a>
                </h5>
                <p>
                    Töötajate halduse leheküljel saab süsteemi lisada uusi töötajaid, saab
                    muuta olemasolevate töötajate andmeid ning saab aktiveerida ja
                    deaktiveerida töötajaid. Deaktiveeritud töötajate puhul palgaarvestust teha
                    ei saa.
                </p>
                <p>
                    <strong>1. </strong>
                    <strong>Töötajate lisamine </strong>
                </p>
                <p>
                    Töötajate lisamine süsteemi toimub töötajate halduse leheküljelt, mis
                    avaneb menüült „Halda töötajaid“ nupu kaudu. Töötaja lisamiseks tuleb
                    halduse leheküljel vajutada nupule „Lisa töötaja“, misjärel avaneb uue
                    töötaja lisamise vorm. Kohustuslike andmeväljadena tuleb ära täita töötaja
                    eesnime, perekonnanime, e-maili ja töötab alates andmeväli. Palga
                    arvutamiseks on vaja töötajale määrata ka palgamudel ja väärtustada selle
                    parameetrid. Vastasel juhul puudub töötajal kehtiv palgamudel ja palga
                    väljaarvutamiseks tuleb määrata ühekordne palgamudel, mis on järgmiseks
                    palgaarvestuseks kustunud.
                </p>
                <p><img src="/assets/img/pajuhend/Picture12.png"></p>
                <p>
                    <em>
                        Vaade 12: uue töötaja lisamine töötajate haldamise leheküljelt.
                        Süsteemi on juba lisatud need töötajad, kes on määratud
                        administraatoriteks.
                    </em>
                </p>
                <p>
                    Töötajate halduse vaatel on kohe olemas need töötajad, kes on määratud
                    süsteemi administraatoriteks.
                </p>
                <p>
                    Nende töötajate puhul, kes on palgaarvestuse kuul Freckle-sse tunde
                    loginud, päritakse importimise käigus Freckle-s olevat nime ja e-maili.
                    Nende andmete muutumisel Freckle-s kanduvad muudatused üle ka süsteemi.
                </p>
                <p>
                    Sama nimega töötajaid saab süsteemi lisada, kuid e-mailid peavad kõigil
                    olema erinevad, sh peab töötaja e-mail olema sama mis on Freckle-s olev, et
                    importimisel oleks võimalik pärida projektides tehtud töötunde.
                </p>
                <p><img src="/assets/img/pajuhend/Picture13.png"></p>
                <p>
                    <em>Vaade 13: sama nimega töötaja lisamisel antakse vastav teavitus.</em>
                </p>
                <p>
                    Kui töötajale märgitakse „Töötab alates“ käesoleva kuu kuupäev, siis see
                    töötaja aktiivses palgaarvestuse kuus ei kuvata. Aktiivseks palgaarvestuse
                    kuuks on eelmine kuu. Kui töötaja lisati käesoleval kuul, kuid ta tegi
                    töötunde eelmisel kuul, siis lisatakse see töötaja kehtivasse
                    palgaarvestusse.
                </p>
                <p>
                    Töötaja osalemised projektides päritakse projektide andmete importimise
                    käigus. Samuti päritakse osalemised projektides juhul, kui töötaja
                    lisatakse aktiivse palgaarvestuse käigus ning kasutaja liigub
                    palgaarvestusse menüü lingi kaudu. Viimasel juhul ei lähtestata juba välja
                    arvutatud ja kinnitatud palkasid.
                </p>
                <p>
                    <strong>2. </strong>
                    <strong>Töötaja aktiveerimine ja deaktiveerimine</strong>
                </p>
                <p>
                    Vaikimisi on kõik süsteemi lisatud töötajad aktiveeritud. Töötaja
                    deaktiveerimiseks tuleb avada töötaja andmete vorm, mis tuuakse nähtavale
                    „Muuda“ nupule vajutamisega. Antud vormil tuleb märkida valituks
                    „Deaktiveeritud“ kastike. Pärast deaktiveerimist kuvatakse töötaja
                    staatusena „Deaktiveeritud“ ning antud töötaja eemaldatakse palgaarvestuse
                    leheküljelt juhul, kui tal puudub kinnitatud palgaarvutus. Deaktiveeritud
                    töötajat ei saa määrata administraatoriks. Töötaja uuesti aktiveerimisel
                    kuvatakse ta jälle palgaarvestuse leheküljel.
                </p>
                <p><img src="/assets/img/pajuhend/Picture14.png"></p>
                <p>
                    <em>
                        Vaade 14: deaktiveerimine toimub läbi töötaja andmete vormi.
                        „Deaktiveeritud“ kastike on valituks märgitud.
                    </em>
                </p>
                <h5 class="mb-3">
                    <a name="_Toc484170080">TOIMINGUD PROJEKTIDEGA</a>
                </h5>
                <p>
                    Projektide leheküljel saab käivitada eelmise või käesoleva kuu projektide
                    andmete impordi Freckle API-st, saab sisestada kuu tööpäevade arvu ning
                    saab projektidele lisada palgaarvestuse ja statistika tunnihindasid ning
                    kommentaare. Projektide leheküljel imporditud ja sisestatud andmed on
                    sisendiks töötajate palgaarvestusele, arhiivile ning statistikale.
                </p>
                <p>
                    <strong>1. </strong>
                    <strong>Projektide andmete importimine</strong>
                </p>
                <p>
                    Projektide andmete (nimetus, töötunnid, staatus) importimiseks avada menüül
                    projektide lehekülg, teha kuu valik ning vajutada „Impordi“ nupule.
                    Vaikimisi on valituks märgitud eelmine kuu. Kui valitud kuu kohta pole veel
                    importi käivitatud, kuvatakse teavitus importimata andmetest.
                </p>
                <p><img src="/assets/img/pajuhend/Picture15.png"></p>
                <p>
                    <em>Vaade 15: projektide andmete importi pole veel käivitatud.</em>
                </p>
                <p>
                    Korraga saab käivitada ühe impordi. Kahe kasutaja samaaegsel importimisel
                    kuvatakse teavitus käimasolevast impordist ning uut importi enne teha ei
                    saa, kui käimasolev lõpuni viiakse.
                </p>
                <p><img src="/assets/img/pajuhend/Picture16.png"></p>
                <p>
                    <em>
                        Vaade 16: käesoleva kuu projektide andmete importimine Freckle API-st.
                        Andmed on selle kuu kohta veel importimata.
                    </em>
                </p>
                <p>
                    Pärast importimist kuvatakse tabel imporditud projektide andmetega
                    <strong>
                        . Tabelis kuvatakse välja ainult nende töötajate projektid, kes on
                        süsteemi lisatud.
                    </strong>
                </p>
                <p>
                    Igal impordil uuendatakse projektide andmeid vastavalt sellele, kuidas need
                    on muutunud Freckle-s. Lisatakse juurde uusi Freckle-sse loodud projekte
                    ning eemaldatakse kustutatud või arhiveeritud projektid. Impordi käigus
                    kontrollitakse, kas projekti nimetus, töötunnid või staatus on muutunud ja
                    muudetakse neid siis vastavalt.
                </p>
                <p>
                    Importimise käigus eeltäidetakse nende projektide tunnihindade väljad,
                    millele on sisestatud tunnihinnad sellest eelneval kuul. Eeltäidetud
                    tunnihinda muutes ja uuesti importi käivitades jääb tunnihinnaks kehtima
                    see, mis sai viimati väljale sisestatud.
                </p>
                <p><img src="/assets/img/pajuhend/Picture17.png"></p>
                <p>
                    <em>Vaade 17: eeltäidetud väljadega imporditud projektid.</em>
                </p>
                <p>
                    Eelmise ja käesoleva kuu andmeid saab võrrelda, kui vahetada kuud
                    raadionuppude abil.
                </p>
                <p><img src="/assets/img/pajuhend/Picture18.png"></p>
                <p>
                    <em>Vaade 18: kuude vahetamine raadionuppude abil.</em>
                </p>
                <p>
                    <strong>2. </strong>
                    <strong>Kuu tööpäevade sisestamine</strong>
                </p>
                <p>
                    Pärast impordi käivitamist kuvatakse projektide leheküljel kuu tööpäevade
                    arvu sisestusväli. Antud väli on kohustuslik, mis ilma täitmiseta ei lase
                    edasi liikuda palgaarvestuse toimingu juurde. Kuu tööpäevade arv kandub
                    edasi töötajate palgaarvestuse vormile ja statistikasse. Tööpäevade arvu
                    muutes kandub see muudatus automaatselt statistikasse. Töötaja
                    palgaarvestuse vormil muutub tööpäevade arv juhul, kui lähtestatakse
                    palgaarvestus.
                </p>
                <p><img src="/assets/img/pajuhend/Picture19.png"></p>
                <p>
                    <em>
                        Vaade 19: pärast importimist kuvatakse kuu tööpäevade arvu
                        sisestusväli.
                    </em>
                </p>
                <p>
                    <strong>3. </strong>
                    <strong>
                        Projektidele palgaarvestuse ja statistika tunnihinna ning kommentaari
                        sisestamine
                    </strong>
                </p>
                <p>
                    Pärast importimist kuvatakse tabel projektide andmetega. Projektid on
                    järjestatud suurima töötundide arvu ja „Billable“ staatuse alusel.
                    Eeltäidetud on nende projektide tunnihinnad, millele eelneval kuul lisati
                    tunnihindu. Kui palgaarvestuse ja statistika tunnihinna väljad on tühjad,
                    siis palgaarvestuse tunnihinna sisestamisel kopeeritakse see statistika
                    tunnihinna väljale.
                </p>
                <p>
                    Palgaarvestuse juurde edasiliikumiseks on vajalik ära täita kõik
                    palgaarvestuse ja statistika tunnihindade väljad. Kommentaari väli on
                    mittekohustuslik andmeväli. Väljadele sisestatu salvestub automaatselt.
                </p>
                <p>
                    Statistika tunnihinda muutes kandub see muudatus edasi statistikasse.
                </p>
                <p><img src="/assets/img/pajuhend/Picture20.png"></p>
                <p>
                    <em>
                        Vaade 20: eelmise kuu põhjal eeltäidetud väljad. Väljade automaatsel
                        salvestamisel kuvatakse salvestumise ikoon. Palgaarvestuse tunnihind
                        kopeeritakse statistika tunnihinna väljale.
                    </em>
                </p>
                <p>
                    <strong>4. </strong>
                    <strong>Kordusimport</strong>
                </p>
                <p>
                Projektide leheküljelt saab palgaarvestuse toimingu juurde edasi liikuda    <strong>„Edasi palku arvutama“ nupule</strong> vajutamisega. Enne edasi
                    suunamist teostatakse kordusimport, mis kontrollib, kas võrreldes viimase
                    impordiga on toimunud muudatusi projektide andmetes (nimetus, staatus,
                    tundide hulk) ja kas on lisandunud uusi projekte või on mõni olemasolev
                    arhiveeritud. Muudatustest teavitatakse kasutajat ning muutunud andmetega
                    projektid kuvatakse teise taustavärviga. Samuti kontrollitakse
                    kordusimpordil, kas kõik kohustuslikud väljad on täidetud. Tühjad väljad
                    kuvatakse teise taustavärviga. Pärast kasutajapoolset andmete muutmist
                    tuleb uuesti vajutada „Edasi palku arvutama“ nupule, misjärel suunatakse
                    kasutaja palgaarvestuse leheküljele.
                </p>
                <p><img src="/assets/img/pajuhend/Picture21.png"></p>
                <p>
                    <em>
                        Vaade 21: muutunud andmetega või täitmata tunnihindadega projektid
                        kuvatakse teise taustavärviga.
                    </em>
                </p>
                <h5 class="mb-3">
                    <a name="_Toc484170081">PALGAARVESTUS</a>
                </h5>
                <p>
                    Palgaarvestust saab teha eelmise kuu kohta ning see toiming hõlmab
                    töötajatele palkade väljaarvutamist, kinnitamist ning palgakokkuvõtte
                    e-mailide väljasaatmist nii töötajatele kui ka raamatupidajale.
                </p>
                <p>
                    <strong>1. </strong>
                    <strong>Palgaarvestuse toimingu algatamine</strong>
                </p>
                <p>
                    Kuu palgaarvestuse toimingut saab alustada pärast seda, kui projektide
                    leheküljel on käivitatud import, on sisestatud kuu tööpäevade arv ning on
                    lisatud tunnihinnad kõikidele projektidele.
                </p>
                <p><img src="/assets/img/pajuhend/Picture22.png"></p>
                <p>
                    <em>Vaade 22: teavitus tegemata toimingutest projektide leheküljel.</em>
                </p>
                <p>
                    <strong>
                        Palgaarvestuse toimingu käivitab „Edasi palku arvutama“ nupule vajutus
                    </strong>
                    , mis teeb andmete kordusimpordi Freckle-st, kontrollib kohustuslike
                    väljade täidetust ning tekitab palgaarvestuse leheküljele nimekirja
                    töötajatest, kes on aktiveeritud ning kelle tööaja algus ei jää
                    käesolevasse kuusse.
                </p>
                <p>
                    Lisaks kordusimpordile lähtestab „Edasi palku arvutama“ nupp kogu
                    palgaarvestuse, st kustuvad kõik eelnevalt välja arvutatud ja kinnitatud
                    palgad. Lähtestatakse ka nuppude staatused.
                </p>
                <p><img src="/assets/img/pajuhend/Picture23.png"></p>
                <p>
                    <em>
                        Vaade 23: „Edasi palku arvutama“ nupp teostab kontrollimpordi ja
                        lähtestab palgaarvestuse.
                    </em>
                </p>
                <p><img src="/assets/img/pajuhend/Picture24.png"></p>
                <p>
                    <em>
                        Vaade 24: „Edasi palku arvutama“ loob palgaarvestuse vaatele nimekirja
                        töötajatest, kellele saab selle kuu eest palka arvutada.
                    </em>
                </p>
                <p>
                    <strong>2. </strong>
                    <strong>Töötajale palgaarvestuse vormi avamine</strong>
                </p>
                <p>
                    Palgaarvestuse leheküljel olevast töötajate nimekirjast tuleb valida
                töötaja, kellele soovitakse palka arvutada, ning tuleb vajutada „    <strong>Palka arvutama</strong>“ lingile, mis avab selle töötaja
                    palgaarvestuse vormi.
                </p>
                <p><img src="/assets/img/pajuhend/Picture25.png"></p>
                <p>
                    <em>
                        Vaade 25: töötaja palgaarvestuse vormi avab „Palka arvutama“ nupule
                        vajutus.
                    </em>
                </p>
                <p><img src="/assets/img/pajuhend/Picture26.png"></p>
                <p>
                    <em>Vaade 26: töötaja palgaarvestuse vorm.</em>
                </p>
                <p>
                    Töötaja palgaarvestuse vormi all on lingid, mille kaudu saab
                    <strong>
                        liikuda järgmise töötaja vormile või siis pöörduda tagasi eelmise
                        töötaja vormi juurde.
                    </strong>
                </p>
                <p><img src="/assets/img/pajuhend/Picture27.png"></p>
                <p>
                    <em>Vaade 27: liikumine töötajate palgaarvestuse vormide vahel.</em>
                </p>
                <p>
                    <strong>3. </strong>
                    <strong>Projektid ja nende muutmine töötaja palgaarvestuse vormil</strong>
                </p>
                <p>
                    Palgaarvestuse vorm koosneb kolmest andmeplokist
                    <strong>
                        : osalemised projektides, tööpäevade arv ja kehtiv palgamudel.
                    </strong>
                </p>
                <p>
                    <strong>Osalemised projektides</strong>
                    plokk sisaldab andmeid projektide, eelmise kuu töötundide, palgaarvestuse
                    tunnihinna ning summeeritud töötundide, palgaarvestuse sissetuleku ja
                    statistika sissetuleku kohta. Antud andmed tekivad vormile impordi ja
                    korduimpordi hetkel või siis palgaarvestuse vormil käsitsi lisades või uue
                    töötaja lisamisel. Muuta saab projekti töötunde ja palgaarvestuse
                    tunnihinda. Muudatused salvestatakse automaatselt ning need kajastuvad
                    koheselt ka töötundide, palgaarvestuse ja statistika sissetuleku
                    arvutustes.
                </p>
                <p><img src="/assets/img/pajuhend/Picture28.png"></p>
                <p>
                    <em>
                        Vaade 28: projekti töötundide või palgaarvestuse tunnihinna muutmine.
                    </em>
                </p>
                <p>
                    <strong>4. </strong>
                    <strong>Projekti lisamine</strong>
                </p>
                <p>
                    Töötajale saab käsitsi lisada projekti. Projekti lisamiseks vajutada
                    projektide nimekirja lõpus olevale „Lisa projekt“ lingile, misjärel avaneb
                    modaalaken projekti andmete sisestamiseks. Käsitsi lisatud projekti saab
                    kustutada.
                </p>
                <p><img src="/assets/img/pajuhend/Picture29.png"></p>
                <p>
                    <em>
                        Vaade 29: projekti töötundide käsitsi lisamine töötaja palgaarvestuse
                        vormilt.
                    </em>
                </p>
                <p><img src="/assets/img/pajuhend/Picture30.png"></p>
                <p>
                    <em>
                        Vaade 30: käsitsi lisatud projekti saab kustutada „x“ ikoonile
                        klikkides.
                    </em>
                </p>
                <p>
                    <strong>5. </strong>
                    <strong>Tööpäevade arv</strong>
                </p>
                <p>
                    Tööpäevade arvu andmeplokk sisaldab andmeid kuu tööpäevade arvu ning selle
                    töötaja töötatud päevade kohta. „Tööpäevi kuus kokku“ väli täidetakse
                    projektide leheküljel sisestatud tööpäevade arvuga ning selle sama välja
                    väärtus kopeeritakse ka „Töötatud tööpäevi kuus“ väljale. Mõlemad
                    andmeväljad on muudetavad.
                </p>
                <p><img src="/assets/img/pajuhend/Picture31.png"></p>
                <p>
                    <em>Vaade 31: tööpäevade arvu andmeplokk.</em>
                </p>
                <p>
                    <strong>6. </strong>
                    <strong>Palgaarvutus kehtiva ja ühekordse palgamudeli alusel</strong>
                </p>
                <p>
                    Kehtiva palgamudeli plokis sisalduvad andmed töötaja palgamudeli formaadi
                    ja selle parameetri(te) kohta, kui need on määratud. Palgamudelit saab
                    määrata ja muuta töötajate halduse vaatelt. Kui vaikimisi teostatakse
                    arvutus kehtiva palgamudeli alusel, siis võimalik on määrata ka ühekordset
                    palgamudelit, mille järgi palka arvutada. Ühekordse palgamudeli muutus
                    hõlmab palgamudeli valikut ja selle palgamudeli parameetrite väärtustamist.
                </p>
                <p><img src="/assets/img/pajuhend/Picture32.png"></p>
                <p>
                    <em>
                        Vaade 32: kehtiv palgamudel on töötajate halduse vaatelt määratud.
                        Palgaarvutus toimub kehtiva palgamudeli alusel (vaikimisi).
                    </em>
                </p>
                <p><img src="/assets/img/pajuhend/Picture33.png"></p>
                <p>
                    <em>
                        Vaade 33: palgaarvutus toimub ühekordse palgamudeli alusel. Kasutaja
                        peab valima palgamudeli ja väärtustama selle parameetrid.
                    </em>
                </p>
                <p>
                    <strong>7. </strong>
                    <strong>Arvuta palk ja kinnita</strong>
                </p>
                <p>
                    Palgaarvutuse käivitamiseks vajutada „Arvuta palk“ nupule, misjärel
                    kuvatakse selle kuu brutopalga arvutus. Pärast arvutust tuleb palk ka
                    kinnitada, misjärel kuvatakse töötaja palk ka palgaarvestuse leheküljel
                    ning muudetakse aktiivseks palgakokkuvõtte e-maili saatmise nupp.
                    Kinnitamise järgselt kuvatakse „Kinnita“ nupu staatusena „Palk kinnitatud“.
                </p>
                <p><img src="/assets/img/pajuhend/Picture34.png"></p>
                <p>
                    <em>
                        Vaade 34: palga arvutamine „Arvuta palk“ nupule vajutamisega ning
                        väljaarvutatud palga kinnitamine „Kinnita“ nupule vajutamisega. Ilma
                        „Kinnita“ nupule vajutuseta on töötaja palgaarvutuse kokkuvõtte juures
                        vastav teavitus.
                    </em>
                </p>
                <p><img src="/assets/img/pajuhend/Picture35.png"></p>
                <p>
                    <em>
                        Vaade 35: palk on kinnitatud, muutunud on „Kinnita“ nupu tekst ja
                        värvus ning vasakpoolne teavituse tekst.
                    </em>
                </p>
                <p><img src="/assets/img/pajuhend/Picture36.png"></p>
                <p>
                    <em>
                        Vaade 36: pärast palga väljaarvutamist ja kinnitamist kuvatakse
                        töötaja palk palgaarvestuse leheküljel. Aktiivseks on muutunud „Saada
                        e-mail“ nupp. Muutunud on „Palka arvutama“ nupu staatus – nupp
                        kuvatakse rohelisena ja tekst on „Palk arvutatud“.
                    </em>
                </p>
                <p>
                    <strong>8. </strong>
                    <strong>Palgaarvutuse muutmine</strong>
                </p>
                <p>
                    Väljaarvutatud ja kinnitatud palgaarvutuse muutmiseks tuleb avada töötaja
                    palgaarvestuse vorm. Seda saab teha kas palgaarvestuse leheküljel vastava
                    töötaja juures oleva „Palk arvutatud“ nupu kaudu või siis e-maili eelvaate
                    kaudu, mis avaneb „Saada e-mail“ nupu alt (või kui e-mail on saadetud, siis
                    „E-mail saadetud“ nupu alt).
                </p>
                <p><img src="/assets/img/pajuhend/Picture37.png"></p>
                <p>
                    <em>
                        Vaade 37: töötaja palgaarvestuse vorm avaneb palgaarvestuse leheküljelt
                        „Palk arvutatud“ või „Saada e-mail“/“E-mail saadetud“ nupu alt.
                    </em>
                </p>
                <p>
                    <strong>9. </strong>
                    <strong>Töötajale palgakokkuvõtte e-maili saatmine</strong>
                </p>
                <p>
                    <strong>
                        Pärast seda kui töötaja palk on välja arvutatud ja kinnitatud saab
                        töötajale saata palgakokkuvõtte e-maili.
                    </strong>
                    Selleks tuleb palgaarvestuse leheküljelt valida töötaja, vajutada „Saada
                    e-mail“ nupule, misjärel avaneb palgakokkuvõtte e-maili eelvaade. Eelvaade
                    annab võimaluse enne e-maili väljasaatmist üle kontrollida palgaarvutuse.
                    Juhul, kui arvutuses esineb vigu, saab liikuda tagasi töötaja
                    palgaarvestuse vormile ning teha seal vajalikud muudatused, vajutades
                    eelvaatel <strong>„Muuda“</strong> nupule. Kui palgaarvutus on korrektne,
                    siis e-maili väljasaatmiseks vajutada „Saada“ nupule. Pärast e-maili
                    saatmist kuvatakse nupu „Saada e-mail“ staatusena „E-mail saadetud“, mis on
                    kinnitus edukast e-maili saatmisest.
                </p>
                <p><img src="/assets/img/pajuhend/Picture39.png"></p>
                <p>
                    <em>
                        Vaade 38: töötaja palgakokkuvõtte e-maili eelvaade. Andmed kanduvad
                        eelvaatele töötaja palgaarvestuse vormilt. „Muuda“ nupu kaudu saab
                        tagasi liikuda töötaja palgaarvestuse vormile. „Saada“ nupu kaudu saab
                        töötajale e-maili süsteemist välja saata.
                    </em>
                </p>
                <p><img src="/assets/img/pajuhend/Picture40.png"></p>
                <p>
                    <em>
                        Vaade 39: „E-mail saadetud“ kuvatakse, kui e-mail on töötajale välja
                        saadetud.
                    </em>
                </p>
                <p>
                    <strong>10. </strong>
                    <strong>Raamatupidajale palgakokkuvõtte e-maili genereerimine</strong>
                </p>
                <p>
                    <strong>
                        Raamatupidajale saab e-maili genereerida pärast seda, kui on välja
                        arvutatud ja kinnitatud kõikide töötajate palgad.
                    </strong>
                    E-maili genereerimiseks vajutada palgaarvestuse leheküljel töötajate tabeli
                    all olevale „Genereeri“ nupule, pärast mida luuakse ühendus kasutaja
                    Outlook’i kontoga ning koostatakse uus e-mail koos süsteemi poolt täietud
                    kehandiga. Pärast e-maili genereerimist kuvatakse „Genereeri“ nupu
                    staatusena „Genereeritud“.
                </p>
                <p><img src="/assets/img/pajuhend/Picture41.png"></p>
                <p>
                    <em>
                        Vaade 40: „Genereeri“ nupule vajutus genereerib palgaarvestuse
                        kokkuvõtte raamatupidajale.
                    </em>
                </p>
                <p><img src="/assets/img/pajuhend/Picture42.png"></p>
                <p>
                    <em>
                        Vaade 41: „Genereeri“ nupule vajutus palgaarvestuse leheküljelt loob
                        ühenduse MS Outlook’iga ning koostab eeltäidetud meili kehandi.
                    </em>
                </p>
                <p><img src="/assets/img/pajuhend/Picture43.png"></p>
                <p>
                    <em>
                        Vaade 42: „Genereeritud“ nupu staatus viitab e-maili genereerimise
                        toimingu läbiviimisest.
                    </em>
                </p>
                <h5 class="mb-3">
                    <a name="_Toc484170082">ARHIIV</a>
                </h5>
                <p>
                    Arhiivi leheküljel kuvatakse eelmise ja varasemate kuude projektide ja
                    töötajate palgaarvestuse andmed. Vaikimisi kuvatakse arhiivi leheküljel
                    üleelmise kuu projektide andmed. Kasutaja saab valida, millise kuu andmeid
                    ta vaadata soovib.
                </p>
                <p><img src="/assets/img/pajuhend/Picture44.png"></p>
                <p>
                    <em>
                        Vaade 43: teade kuvatakse juhul, kui kuu kohta pole projektide andmeid
                        imporditud.
                    </em>
                </p>
                <p><img src="/assets/img/pajuhend/Picture45.png"></p>
                <p>
                    <em>
                        Vaade 44: kasutaja saab valida, millise kuu projektide andmeid ta
                        vaadata soovib.
                    </em>
                </p>
                <p>
                    Projektide vaatel on korraga kuvatud 20 projekti. Rohkemate projektide
                    vaatamiseks vajutada <strong>„Näita rohkem“ lingile</strong>.
                </p>
                <p>
                Töötajate palgaarvestuse andmete vaatamiseks valida    <strong>„Töötajad“</strong> sakk, misjärel kuvatakse töötajate nimekiri
                    koos väljaarvutatud brutopalkadega. Iga töötaja puhul saab vaadata talle
                    eelnevatel kuudel välja arvutatud palkasid.
                </p>
                <p><img src="/assets/img/pajuhend/Picture46.png"></p>
                <p>
                    <em>
                        Vaade 45: töötajate varasemate kuude palkade andmed on nähtavad arhiivi
                        leheküljelt töötajate saki alt. „Vaata“ nupu alt saab avada varasema
                        kuu palgaarvestuse e-mail eelvaadet.
                    </em>
                </p>
                <p><img src="/assets/img/pajuhend/Picture47.png"></p>
                <p>
                    <em>
                        Vaade 46: kui projekti andmed on importimata, kuvatakse vaatel vastav
                        teavitus.
                    </em>
                </p>
                <p><img src="/assets/img/pajuhend/Picture48.png"></p>
                <p>
                    <em>
                        Vaade 47: töötaja eelneva kuu palgaarvestuse kokkuvõtte eelvaade
                        arhiivi vaatel. Sama vaade avaneb palgaarvestuse „Saada e-mail“ nupu
                        alt.
                    </em>
                </p>
                <h5 class="mb-3">
                    <a name="_Toc484170083">STATISTIKA</a>
                </h5>
                <p>
                    Statistika koondab endas projektide ja palgaarvestuse andmete arvutuslikke
                    ülevaateid. Leheküljel saab vaadata nii kuu kui ka aastapõhiselt erinevaid
                    arvutuslikke näitajaid, sh töötajapõhist kulu-tulu suhteid ning tööaja
                    kokkuvõtet. Andmed kuvatakse vaatel nii graafiliselt kui ka tabelkujul.
                    Viimane esitab andmeid täpsemal kujul koos rohkemate arvutuslike
                    näitajatega.
                </p>
                <p><img src="/assets/img/pajuhend/Picture49.png"></p>
                <p>
                    <em>
                        Vaade 48: kasutaja saab valida kuu, mille statistilist andmestikku ta
                        vaadata soovib.
                    </em>
                </p>
                <p><img src="/assets/img/pajuhend/Picture50.png"></p>
                <p>
                    <em>
                        Vaade 49: graafiline ülevaade kuupõhistest tulu-kulu andmetest töötaja
                        põhiselt.
                    </em>
                </p>
                <p><img src="/assets/img/pajuhend/Picture51.png"></p>
                <p>
                    <em>
                        Vaade 50: graafiline ülevaade kuupõhistest tulu-kulu andmetest töötaja
                        põhiselt. Tabel avaneb graafiku all oleva „Vaata täpsemalt“ lingi
                        kaudu.
                    </em>
                </p>
                <p><img src="/assets/img/pajuhend/Picture52.png"></p>
                <p>
                    <em>
                        Vaade 51: graafiline ülevaade kuupõhistest tööaja andmetest töötaja
                        põhiselt.
                    </em>
                </p>
                <p><img src="/assets/img/pajuhend/Picture53.png"></p>
                <p>
                    <em>
                        Vaade 52: tabelkujul ülevaade kuupõhistest tööaja andmetest töötaja
                        põhiselt. Tabel avaneb graafiku all oleva „Vaata täpsemalt“ lingi
                        kaudu.
                    </em>
                </p>
			</div>
            <div class="modal-footer">
                <button type="button" ng-click="close()" class="btn btn-warning" data-dismiss="modal">Sulge</button>
            </div>
        </div>
    </div>
</script>