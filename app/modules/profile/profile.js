App.controller('profile', function($scope, $http, $rootScope, $routeParams, $location, $route){
    
    $scope.emailRecipients = [];
    
    $scope.removeRecipient = function(index) {
        $scope.emailRecipients.splice(index, 1);
        $scope.saveEmailRecipients();
    };
    
    $scope.addRecipient = function() {
        $scope.emailRecipients.push({ email: ""});
    };
    
    $scope.getStatus = function() {
    	$http.get(settings.prefix + 'ip_check').then(function( response ){
			if(response.data.status) { $scope.ipCheckStatus = true; } else { $scope.ipCheckStatus = false };
		}, function(response){
		});
    }
    $scope.changeStatus = function(currentStatus) {
    	$http({
    		method: "PUT",
			url: settings.prefix+ 'ip_check_toggle',
		}).then(function( response ){
			$scope.messages.alertMessage = response.data;
		}, function(response){
		});
    } 
    $scope.$watch('actives.thirdActive', function(newValue, oldValue){
    	if(newValue) { 
    		$scope.getAddressList(); 
    		$scope.getStatus();
    	};	
    })
    $scope.$watch('actives.secondActive', function(newValue, oldValue){
    	if(newValue) { 
    		$scope.getOutlookStatus();
    	};	
    })
    $scope.$watch('actives.fourthActive', function(newValue, oldValue){
    	if(newValue) { 
    		$scope.getEmailRecipients();
    	};	
    })
    $scope.$watch('actives.fifthActive', function(newValue, oldValue){
    	if(newValue) { 
    		$scope.getAccountant();
    	};	
    })
    
    $scope.getEmailRecipients = function() {
    	$scope.emailRecipients = [];
        $http.get(settings.prefix + 'config/carbon').then(function(response) {
            if(response.data.value) {
                var recipientArray = (response.data.value).split(',');
                for(var elem in recipientArray) {
                    $scope.emailRecipients.push({ email: recipientArray[elem] });
                }
            } else {
                $scope.emailRecipients.push({ email: "" });
            }
        }, function(response) {
            $scope.messages.errorMessage = response.data.message;
        });
    }
    
    $scope.saveEmailRecipients = function() {
        
        var submitData = {};
        var recipientArray = [];
        for(var elem in $scope.emailRecipients) {
            if($scope.emailRecipients[elem].email != undefined) {
                recipientArray.push($scope.emailRecipients[elem].email);
            };
        }
        submitData['cc'] = recipientArray.join(',');
        if(submitData.cc == "") { submitData.cc = " "; }
        $http({
			method: "PUT",
			url: settings.prefix+ 'config/carbon',
			data: submitData,
		}).then(function( response ){
		    $scope.messages.myMessage = true;
		}, function(response){
		    var message = response.data.message;
	        if(message.length > 0) { $scope.messages.errorMessage = message; } else if (message.constructor === Object) { $scope.messages.errorMessage = response.statusText }       
		});
		
    }
    
    
    $scope.messages = { errorMessage: false, alertMessage: false };
	$scope.outlookPlaceholder = "";

	$scope.accountant_model = { };
	$scope.pw_model = {};
	$scope.actives = {};
    $scope.actives.firstActive = true;
    $scope.actives.secondActive = false;
    $scope.actives.thirdActive = false;
    $scope.actives.fourthActive = false;
    $scope.actives.fifthActive = false;
	$scope.activeUser = sessionStorage.user;    
    
    $scope.getOutlookStatus = function() {
        $http.get(settings.prefix+"config/outlook").then(function(response) {
            $scope.outlookPasswordStatus = response.data.email_password;
            if($scope.outlookPasswordStatus) {
            	$scope.outlookPlaceholder = "********";
            };
            $scope.statusReceived = true;
        }, function(response){
			$scope.messages.errorMessage = response.data.message;
			$scope.statusReceived = true;
		});
    };
    
    $scope.getAddressList = function() {
    	$scope.additionList = [{ address: ""}];
    	$scope.addressList = false;
    	$http.get(settings.prefix+"config/ip_whitelist").then(function(response) {
            $scope.addressList = response.data.ip_whitelist.split(',');
            $scope.addressesReceived = true;
        }, function(response){
			$scope.messages.errorMessage = response.data.message;
			$scope.addressesReceived = true;
		});
    }
    
    $scope.addAddressBar = function() {
    	$scope.additionList.push({ address: "" });
    }
    $scope.removeAddressListItem = function(index) {
    	$scope.addressList.splice(index, 1);
    }
    
    $scope.saveAddresses = function() {
    	
    	var submitData = {};
        var addressArray = [];
        for(var elem in $scope.additionList) {
        	if($scope.additionList[elem].address && $scope.additionList[elem].address != "") {
            	addressArray.push($scope.additionList[elem].address);
        	}
        }
        for(var item in $scope.addressList) {
        	if($scope.addressList[item] && $scope.addressList[item] != "") {
        		addressArray.push($scope.addressList[item]);	
        	}
        }
        var val = null;
        if(!addressArray.length) { submitValue = val } else { var submitValue = addressArray.join(','); }
    	
        $http({
			method: "PUT",
			url: settings.prefix+"config/ip_whitelist",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: {
				ip_whitelist: submitValue
			},
    		transformRequest: function(obj) {
	            var str = [];
	            for(var p in obj)
	            	if(obj[p] != "null" && obj[p] != null) {
	            		str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p])); 
	            	} else { 
	            		console.log(obj[p]); 
	            		str.push(encodeURIComponent(p) + "=" + obj[p]) 
	            	}
	            return str.join("&");
    		}
		}).then(function( response ){
			$scope.messages.alertMessage = response.data.message;
			$scope.getAddressList();
		}, function(response){
			$scope.messages.errorMessage = response.data.message;
		});
    }
    $scope.setOutlookPassword = function(password) {
        $http({
			method: "PUT",
			url: settings.prefix+"config/outlook",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: {
				email_password: password
			},
    		transformRequest: function(obj) {
	            var str = [];
	            for(var p in obj)
	               str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
	            return str.join("&");
    		}
		}).then(function( response ){
			$scope.messages.alertMessage = response.data.message;
			$scope.getOutlookStatus();
		}, function(response){
			$scope.messages.errorMessage = response.data.message;
		});
    }
    $scope.setAccountant = function(accountant) {
    	$http({
			method: "POST",
			url: settings.prefix+"config/accountant",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: {
				value: accountant
			},
    		transformRequest: function(obj) {
	            var str = [];
	            for(var p in obj)
	               str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
	            return str.join("&");
    		}
		}).then(function( response ){
			$scope.messages.alertMessage = response.data.message;
		}, function(response){
			$scope.messages.errorMessage = response.data.message;
		});
    }
    $scope.getAccountant = function() {
    	$http.get(settings.prefix + 'config/accountant').then(function( response ){
    		$scope.accountant_model['email'] = response.data.value;
		}, function(response){
		});
    }
    
    $scope.changePassword = function(password, repeated, form){
    	if(form.$valid) {
			if( password && repeated && angular.equals(password, repeated) ) {
				$http({
					method: "PUT",
					url: settings.prefix+"config/password",
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					data: {
						password: password
					},
		    		transformRequest: function(obj) {
			            var str = [];
			            for(var p in obj)
			               str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
			            return str.join("&");
		    		}
				}).then(function( response ){
					$scope.messages.alertMessage = response.data.message;
				}, function(response){
					$scope.messages.errorMessage = response.data.message;
				});
			} else {
				$scope.messages.errorMessage = "Sisestatud paroolid ei ühti, proovi uuesti!"
			}
    	}
	}
})